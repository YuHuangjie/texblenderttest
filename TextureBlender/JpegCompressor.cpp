#include <stdexcept>	
#include "JpegCompressor.h"

using namespace std;

JpegCompressor::JpegCompressor(int width, int height)
	: _width(width),
	_height(height)
{
	unsigned long sz = 0;

	_compressor = tjInitCompress();
	sz = tjBufSize(width, height, TJSAMP_444);
	_buf = vector<uint8_t>(sz);
}

JpegCompressor::~JpegCompressor()
{
	tjDestroy(_compressor);
}

void JpegCompressor::Compress(const uint8_t * src, uint8_t ** jpegBuf, size_t *jpegSz)
{
	const int quality = 90;
	unsigned long sz = 0;
	uint8_t *buf = _buf.data();
	int result = -1;

	result = tjCompress2(_compressor, src, _width, _width * 3, _height, TJPF_RGB,
		&buf, &sz, TJSAMP_444, quality, TJFLAG_NOREALLOC | TJFLAG_BOTTOMUP);

	if (result != 0) {
		throw runtime_error(string("[ERROR] JpegCompressor: Compress failed with ") +
			tjGetErrorStr());
	}

	*jpegBuf = buf;
	*jpegSz = sz;
}
		