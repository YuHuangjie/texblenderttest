#ifndef MESH_QUEUE_H
#define MESH_QUEUE_H

#include <map>
#include <vector>

namespace CuMesh {
	class TriMesh;
}

class MeshQueue
{
public:
	int gpu_id;

	int fix_size;

	int frame_process;
	int frame_texture;

	std::vector<CuMesh::TriMesh*> mesh_queue;
	std::vector<std::map< int, void* >> teximage_queue;

	bool HasMeshToTexture();
	bool HasSpaceToProcess();

	CuMesh::TriMesh* MeshToProcess();
	CuMesh::TriMesh* MeshToTexture();

	std::map< int, void* >& TexImagesToProcess();
	std::map< int, void* >& TexImagesToTexture();
	void  AllocateTexImageToProcess(int camid, int size);

	void FinishProcess();
	void FinishTexture();

	MeshQueue(int dev_id, int size, const size_t nCams, float voxelsize);
	~MeshQueue();
};

#endif /* MESH_QUEUE_H */
