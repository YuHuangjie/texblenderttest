#include "../Common/SimpleCudaMath.h"

#include "../Common/MeshDef.h"
#include <cuda.h>  

#include <thrust/adjacent_difference.h>
#include <thrust/functional.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>
#include <thrust/scan.h>
#include <thrust/execution_policy.h>
#include <thrust/device_ptr.h>
#include <thrust/device_malloc.h>
#include <thrust/device_free.h>
#include <thrust/unique.h>
#include <thrust/fill.h>
#include <thrust/sequence.h>
#include <thrust/copy.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>

#define PT_DIFF_EPS 1E-5

using thrust::get;

__device__ float dev_sift_x;
__device__ float dev_sift_y;
__device__ float dev_sift_z;

struct ComparePoint3 {
	__host__ __device__
		bool operator()(const float3 &p1, const float3 &p2) {

		if (p1.x < p2.x) return true;
		if (p2.x < p1.x) return false;

		// x1 == x2: continue with element 2
		if (p1.y < p2.y) return true;
		if (p2.y < p1.y) return false;

		// y1 == y2: continue with element 3
		if (p1.z < p2.z) return true;
		
		return false;
	}
};

struct VertTexcoordZipComparator
{
	__host__ __device__
		inline bool operator() (const thrust::tuple<unsigned int, unsigned int> &t1, const thrust::tuple<unsigned int, unsigned int> &t2)
	{
		if ( get <0>(t1) <  get <0>(t2)) return true;
		if ( get <0>(t1) == get <0>(t2)) return  get <1>(t1) < get <1>(t2);
		return false;

	}
};

struct IsEqualPoint3 {
	__host__ __device__
		bool operator()(const float3 &p1, const float3 &p2) {

		if (p1.x == p2.x && p1.y == p2.y && p1.z == p2.z)
			return true;
		else
			return false;
	}
};

__device__
void Unique(int* arr, int& num)
{
	int i, j, k;
	
	for (i = 0; i < num; i++)
	{
		for (j = i + 1; j < num;) 
		{
			if (arr[j] == arr[i]) 
			{
				for (k = j; k < num; k++) 
				{
					arr[k] = arr[k + 1];
				}
				
				num--;
			}
			else
				j++;
		}
	}
}

__global__
void ComputeMeshCenterKernel(int face_num, const float* verts)
{
	const int face_id = blockIdx.x * blockDim.x + threadIdx.x;

	if (face_id >= face_num)
		return;

	atomicAdd(&dev_sift_x, verts[9 * face_id + 0]);
	atomicAdd(&dev_sift_x, verts[9 * face_id + 3]);
	atomicAdd(&dev_sift_x, verts[9 * face_id + 6]);

	atomicAdd(&dev_sift_y, verts[9 * face_id + 1]);
	atomicAdd(&dev_sift_y, verts[9 * face_id + 4]);
	atomicAdd(&dev_sift_y, verts[9 * face_id + 7]);

	atomicAdd(&dev_sift_z, verts[9 * face_id + 2]);
	atomicAdd(&dev_sift_z, verts[9 * face_id + 5]);
	atomicAdd(&dev_sift_z, verts[9 * face_id + 8]);
}

__global__
void VertexCodingKernel(int face_num, const float* org_verts, TYPE_CODED_VERT*  coded_verts, float shift_x, float shift_y, float shift_z, float voxel_scale)
{
	const int face_id = blockIdx.x * blockDim.x + threadIdx.x;

	if (face_id >= face_num)
		return;

	for (int v = 0; v < 3; ++v)
	{
		float px = org_verts[3 * (3 * face_id + v) + 0];
		float py = org_verts[3 * (3 * face_id + v) + 1];
		float pz = org_verts[3 * (3 * face_id + v) + 2];

		int ipx = (int)(px * voxel_scale + shift_x);
		int ipy = (int)(py * voxel_scale + shift_y);
		int ipz = (int)(pz * voxel_scale + shift_z);

		int pt = (((ipx << 10) + ipy) << 10) + ipz;

		coded_verts[3 * face_id + v] = pt;
	}
}

__global__ 
void generate_mesh_index(int* d_index_ori, int* d_flag, int* d_index_output, int threadSize, int vertex_num)
{
	int idx_id_base = blockIdx.x * threadSize + threadIdx.x;

	if (idx_id_base >= vertex_num)
	{
		return;
	}

	int T = d_index_ori[idx_id_base] - 1;
	d_index_output[T] = d_flag[idx_id_base];

}

__global__
void TexCoordCodingKernel(int face_num, const float* org_uvs, TYPE_CODED_TEXC*  coded_uvs)
{
	const int face_id = blockIdx.x * blockDim.x + threadIdx.x;

	if (face_id >= face_num)
		return;

	for (int v = 0; v < 3; ++v)
	{
		coded_uvs[2 * (3 * face_id + v) + 0] = TYPE_CODED_TEXC(org_uvs[2 * (3 * face_id + v) + 0] * 65535.0);
		coded_uvs[2 * (3 * face_id + v) + 1] = TYPE_CODED_TEXC(org_uvs[2 * (3 * face_id + v) + 1] * 65535.0);
	}
}

__global__
void CalculateNormalKernel(int face_num, const float* verts, float* normal)
{
	const int face_id = blockIdx.x * blockDim.x + threadIdx.x;

	if (face_id >= face_num)
		return;

	float3 d1 = make_float3(verts[3 * (3 * face_id + 1) + 0] - verts[3 * (3 * face_id + 0) + 0],
		verts[3 * (3 * face_id + 1) + 1] - verts[3 * (3 * face_id + 0) + 1],
		verts[3 * (3 * face_id + 1) + 2] - verts[3 * (3 * face_id + 0) + 2]);
		
	float3 d2 = make_float3(verts[3 * (3 * face_id + 2) + 0] - verts[3 * (3 * face_id + 0) + 0],
		verts[3 * (3 * face_id + 2) + 1] - verts[3 * (3 * face_id + 0) + 1],
		verts[3 * (3 * face_id + 2) + 2] - verts[3 * (3 * face_id + 0) + 2]);

	float3 nl = getCrossProduct(d1, d2);

	float  ns = sqrtf(nl.x * nl.x + nl.y * nl.y + nl.z * nl.z);

	for (int v = 0; v < 3; ++v)
	{
		if (ns > 0.0000001)
		{
			normal[3 * (3 * face_id + v) + 0] = nl.x / ns;
			normal[3 * (3 * face_id + v) + 1] = nl.y / ns;
			normal[3 * (3 * face_id + v) + 2] = nl.z / ns;
		}
		else {
			normal[3 * (3 * face_id + v) + 0] = 0;
			normal[3 * (3 * face_id + v) + 1] = 0;
			normal[3 * (3 * face_id + v) + 2] = 1;
		}
	}
}

__global__
void SortedVertAdjacentDifference(int vert_num, const float3* verts, int* adj_diff)
{
	const int vid = blockIdx.x * blockDim.x + threadIdx.x;

	if (vid >= vert_num)
		return;

	if (vid == 0)
	{
		adj_diff[vid] = 0;
		return;
	}

	float3 vt = verts[vid];
	float3 vp = verts[vid - 1];

	if(fabsf(vt.x - vp.x) + fabsf(vt.y - vp.y) + fabsf(vt.z - vp.z) < PT_DIFF_EPS)
		adj_diff[vid] = 0;
	else
		adj_diff[vid] = 1;
}

__global__
void SortedVertTexcoordAdjacentDifference(int vert_num, const unsigned int* coded_verts, const unsigned int* coded_texcoord, int* adj_diff)
{
	const int vid = blockIdx.x * blockDim.x + threadIdx.x;

	if (vid >= vert_num)
		return;

	if (vid == 0)
	{
		adj_diff[vid] = 0;
		return;
	}

	if ((coded_verts[vid] != coded_verts[vid - 1]) || (coded_texcoord[vid] != coded_texcoord[vid - 1]))
		adj_diff[vid] = 1;
	else
		adj_diff[vid] = 0;
}

__global__
void RemoveDuplicateVertTexcoord(int vert_num, const int* unique_vert_id, unsigned int* coded_verts, unsigned int* coded_texcs)
{
	const int vid = blockIdx.x * blockDim.x + threadIdx.x;

	if (vid >= vert_num)
		return;

	int uid = unique_vert_id[vid];

	if (coded_verts[uid] != coded_verts[vid] || coded_texcs[uid] != coded_texcs[vid])
	{
		coded_verts[uid] = coded_verts[vid];
		coded_texcs[uid] = coded_texcs[vid];
	}
}

__global__
void FaceToUniqueIndex(int face_num, const unsigned int* faces, const int* unique_vert_index, int* out_index_array)
{
	const int fid = blockIdx.x * blockDim.x + threadIdx.x;

	if (fid >= face_num)
		return;

	for (int i = 0; i < 3; ++i)
	{
		int vid = faces[3 * fid + i];
		int uid = unique_vert_index[vid];

		out_index_array[3 * fid + i] = uid;
	}
}

__global__
void FaceToUniqueVertMapping(int face_num, const unsigned int* face, const int* vid_unique, int* vert_to_face_map)
{
	const int fid = blockIdx.x * blockDim.x + threadIdx.x;

	if (fid >= face_num)
		return;

	for (int i = 0; i < 3; ++i) 
	{
		int vid = face[3 * fid + i];
		int uid = vid_unique[vid];

		for (int j = 0; j < TOPOLOGY_LIST_LEN; ++j)
		{
			if (atomicCAS(&vert_to_face_map[uid * TOPOLOGY_LIST_LEN + j], -1, fid) == -1)
				break;
		}
	}
}

__global__
void VertMappingToFaceMapping(int face_num, const unsigned int* face, const int* vid_unique, int* vert_to_face_map, int* face_to_face_map)
{
	const int fid = blockIdx.x * blockDim.x + threadIdx.x;

	if (fid >= face_num)
		return;

	int tfcount = 0;
	int tflist[3 * TOPOLOGY_LIST_LEN];

	for (int i = 0; i < 3; ++i)
	{
		int vid = face[3 * fid + i];
		int uid = vid_unique[vid];

		for (int j = 0; j < TOPOLOGY_LIST_LEN; ++j)
		{
			int v2face = vert_to_face_map[uid * TOPOLOGY_LIST_LEN + j];
			
			if (v2face >= 0 && v2face != fid)
			{
				tflist[tfcount++] = v2face;
			}
		}
	}

	Unique(tflist, tfcount);

	tfcount = tfcount < TOPOLOGY_LIST_LEN ? tfcount : TOPOLOGY_LIST_LEN;

	for (int k = 0; k < tfcount; ++k)
	{
		face_to_face_map[fid * TOPOLOGY_LIST_LEN + k] = tflist[k];
	}
}

void ComputeMeshCenter(int face_num, const float* verts, float scale, float& cpu_x, float& cpu_y, float& cpu_z)
{
	float zero = 0.0;
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(dev_sift_x, &zero, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(dev_sift_y, &zero, sizeof(float)));
	CUDA_SAFE_CALL(cudaMemcpyToSymbol(dev_sift_z, &zero, sizeof(float)));

	dim3 block(128, 1);
	dim3 grid((face_num + block.x - 1) / block.x, 1);

	ComputeMeshCenterKernel << <grid, block >> > (face_num, verts);

	CUDA_CHECK_ERROR();

	CUDA_SAFE_CALL(cudaMemcpyFromSymbol(&cpu_x, dev_sift_x, sizeof(float), 0, cudaMemcpyDeviceToHost));
	CUDA_SAFE_CALL(cudaMemcpyFromSymbol(&cpu_y, dev_sift_y, sizeof(float), 0, cudaMemcpyDeviceToHost));
	CUDA_SAFE_CALL(cudaMemcpyFromSymbol(&cpu_z, dev_sift_z, sizeof(float), 0, cudaMemcpyDeviceToHost));

	cpu_x = float(int(512 - cpu_x * scale / (face_num * 3)));
	cpu_y = float(int(512 - cpu_y * scale / (face_num * 3)));
	cpu_z = float(int(512 - cpu_z * scale / (face_num * 3)));
}
//because we add the color into the forth channel, we don't have to compare the .w elem
__device__ __host__ __forceinline__ bool
AlmostEqualRelative(float A, float B, float maxRelDiff = FLT_EPSILON)
{
	// Calculate the difference.
	float diff = fabs(A - B);
	A = fabs(A);
	B = fabs(B);
	// Find the largest
	float largest = (B > A) ? B : A;

	if (diff <= largest * maxRelDiff)
		return true;
	return false;
}
__device__ __host__ __forceinline__ bool
operator==(const float3& v1, const float3& v2)
{
	return (AlmostEqualRelative(v1.x, v2.x) && AlmostEqualRelative(v1.y, v2.y) && AlmostEqualRelative(v1.z, v2.z));
}
__device__ __host__ __forceinline__ bool
operator>(const float3& v1, const float3& v2)
{
	return thrust::tie(v1.x, v1.y, v1.z) > thrust::tie(v2.x, v2.y, v2.z);
}
__device__ __host__ __forceinline__ bool
operator<(const float3& v1, const float3& v2)
{
	return thrust::tie(v1.x, v1.y, v1.z) < thrust::tie(v2.x, v2.y, v2.z);
}

void TopologyInterpretation(int face_num, TYPE_VERT* verts_copy, TYPE_FACE* faces, int* org_vert_index, int* unique_vert_index, int* vert_to_face_mapping, int* face_to_face_mapping)
{
	int vert_num = face_num * 3;

	thrust::fill(thrust::device, vert_to_face_mapping, vert_to_face_mapping + vert_num * TOPOLOGY_LIST_LEN, -1);
	thrust::fill(thrust::device, face_to_face_mapping, face_to_face_mapping + face_num * TOPOLOGY_LIST_LEN, -1);
	thrust::sequence(thrust::device, org_vert_index, org_vert_index + vert_num);

	thrust::device_ptr<float3> dev_verts((float3*)verts_copy);
	thrust::sort_by_key(thrust::device, dev_verts, dev_verts + vert_num, org_vert_index);

	dim3 blockvert(128, 1);
	dim3 gridvert((vert_num + blockvert.x - 1) / blockvert.x, 1);

	SortedVertAdjacentDifference << <gridvert, blockvert >> > (vert_num, (float3*)verts_copy, unique_vert_index);

	CUDA_CHECK_ERROR();

	thrust::inclusive_scan(thrust::device, unique_vert_index, unique_vert_index + vert_num, unique_vert_index);

	thrust::device_ptr<int> org_vertsidx_ptr((int*)org_vert_index);
	thrust::device_ptr<int> uni_vertsidx_ptr((int*)unique_vert_index);
	thrust::sort_by_key(thrust::device, org_vertsidx_ptr, org_vertsidx_ptr + vert_num, uni_vertsidx_ptr);

	dim3 blockface(128, 1);
	dim3 gridface((face_num + blockface.x - 1) / blockface.x, 1);
	
	FaceToUniqueVertMapping << <gridface, blockface >> > (face_num, faces, unique_vert_index, vert_to_face_mapping);
	VertMappingToFaceMapping << <gridface, blockface >> >(face_num, faces, unique_vert_index, vert_to_face_mapping, face_to_face_mapping);

	CUDA_CHECK_ERROR();
}

void VertexReducing(int face_num, TYPE_CODED_VERT*  coded_verts, TYPE_CODED_INDX* index_array, int* fn_vert_num)
{
	// reduce duplicate vertices here -- Yu
	int ori_vertices_number = face_num * 3;
	thrust::device_vector<int> d_index_ori(ori_vertices_number, 1);
	thrust::inclusive_scan(d_index_ori.begin(), d_index_ori.end(), d_index_ori.begin()); //original index array

	thrust::device_ptr<int> dev_ptr_keys(coded_verts);
	thrust::sort_by_key(dev_ptr_keys, dev_ptr_keys + ori_vertices_number, d_index_ori.begin());

	thrust::device_vector<int> d_result(ori_vertices_number, 1);
	thrust::adjacent_difference(dev_ptr_keys, dev_ptr_keys + ori_vertices_number, d_result.begin(), thrust::not_equal_to<int>()); // set neighbor difference to 1
	thrust::fill(d_result.begin(), d_result.begin() + 1, 1);
	thrust::inclusive_scan(d_result.begin(), d_result.end(), d_result.begin()); // add all flag to increase array

	thrust::device_ptr<int> new_last = thrust::unique(dev_ptr_keys, dev_ptr_keys + ori_vertices_number);
	int fn_vertex_number = new_last - dev_ptr_keys;
	std::cout << "orignal vertex number " << ori_vertices_number << std::endl;
	std::cout << "final vertex number " << fn_vertex_number << std::endl;

	int threadSize = 1024;
	int blockNum = ori_vertices_number / threadSize + 1;
	int* index_ori = thrust::raw_pointer_cast(&d_index_ori[0]);
	int* index_flag = thrust::raw_pointer_cast(&d_result[0]);
	generate_mesh_index << <blockNum, threadSize >> > (index_ori, index_flag, index_array, threadSize, ori_vertices_number);

	*fn_vert_num = fn_vertex_number;

	d_result.clear();
	d_result.shrink_to_fit();
}

void VertexTexCoordReducing(int face_num, TYPE_FACE* face, TYPE_CODED_VERT* coded_verts, TYPE_CODED_TEXC* coded_texcs, int* org_vert_index, int* unique_vert_index, TYPE_CODED_INDX* out_index_array, int* vert_num_reduced)
{
	int org_vert_num = face_num * 3;
	thrust::sequence(thrust::device, org_vert_index, org_vert_index + org_vert_num);
	
	thrust::device_ptr<unsigned int> dptr_vert = thrust::device_pointer_cast((unsigned int*)coded_verts);
	thrust::device_ptr<unsigned int> dptr_texc = thrust::device_pointer_cast((unsigned int*)coded_texcs);

	auto vtzip_begin = thrust::make_zip_iterator(thrust::make_tuple(dptr_vert, dptr_texc));
	auto vtzip_end = thrust::make_zip_iterator(thrust::make_tuple(dptr_vert + org_vert_num, dptr_texc + org_vert_num));

	thrust::sort_by_key(thrust::device, vtzip_begin, vtzip_end, org_vert_index, VertTexcoordZipComparator());

	dim3 blockvert(128, 1);
	dim3 gridvert((org_vert_num + blockvert.x - 1) / blockvert.x, 1);

	SortedVertTexcoordAdjacentDifference << <gridvert, blockvert >> > (org_vert_num, (unsigned int*)coded_verts, (unsigned int*)coded_texcs, unique_vert_index);

	CUDA_CHECK_ERROR();

	thrust::inclusive_scan(thrust::device, unique_vert_index, unique_vert_index + org_vert_num, unique_vert_index);

	CUDA_SAFE_CALL(cudaMemcpy(vert_num_reduced, (unique_vert_index + org_vert_num - 1), sizeof(int), cudaMemcpyDeviceToHost));
	*vert_num_reduced += 1;

	RemoveDuplicateVertTexcoord << <gridvert, blockvert >> > (org_vert_num, unique_vert_index, (unsigned int*)coded_verts, (unsigned int*)coded_texcs);
	
	CUDA_CHECK_ERROR();

	thrust::sort_by_key(thrust::device, org_vert_index, org_vert_index + org_vert_num, unique_vert_index);

	dim3 blockface(128, 1);
	dim3 gridface((face_num + blockface.x - 1) / blockface.x, 1);

	FaceToUniqueIndex << <gridface, blockface >> > (face_num, face, unique_vert_index, out_index_array);

	CUDA_CHECK_ERROR();
}

void VertexCoding(int face_num, const float* org_verts, TYPE_CODED_VERT*  coded_verts, float shift_x, float shift_y, float shift_z, float voxel_scale) 
{

	dim3 block(128, 1);
	dim3 grid((face_num + block.x - 1) / block.x, 1);

	VertexCodingKernel << <grid, block >> > (face_num, org_verts, coded_verts, shift_x, shift_y, shift_z, voxel_scale);

	CUDA_CHECK_ERROR();
}

void TexCoordCoding(int face_num, const float* org_uvs, TYPE_CODED_TEXC* coded_uvs)
{
	dim3 block(128, 1);
	dim3 grid((face_num + block.x - 1) / block.x, 1);

	TexCoordCodingKernel << <grid, block >> > (face_num, org_uvs, coded_uvs);

	CUDA_CHECK_ERROR();

}


void CalculateNormal(int face_num, const float* verts, float* normal)
{
	dim3 block(128, 1);
	dim3 grid((face_num + block.x - 1) / block.x, 1);

	CalculateNormalKernel << <grid, block >> > (face_num, verts, normal);

	CUDA_CHECK_ERROR();

}

__global__
void SmoothNormalKernel(int face_num, const unsigned int* face, const int* vid_unique, const int* vert_to_face_mapping, float* normal)
{
	unsigned int face_id = threadIdx.x + blockIdx.x * blockDim.x;

	if (face_id >= face_num)
		return;

	int iter = 0;

	while (iter < 1)
	{
		for (int i = 0; i < 3; ++i){

			int nbcount = 0;
			int nbfacelist[TOPOLOGY_LIST_LEN];
			
			int vid = face[3 * face_id + i];
			int uid = vid_unique[vid];

			for (int j = 0; j < TOPOLOGY_LIST_LEN; ++j)
			{
				int nbfid = vert_to_face_mapping[uid * TOPOLOGY_LIST_LEN + j];

				if (nbfid < 0 || nbfid >= face_num)
					break;

				if (nbfid != face_id)
					nbfacelist[nbcount++] = nbfid;
			}

			Unique(nbfacelist, nbcount);
			
			int count = 0;
			float3 this_normal = make_float3(normal[3 * (3 * face_id + i) + 0], normal[3 * (3 * face_id + i) + 1], normal[3 * (3 * face_id + i) + 2]);
			float3 nb_normal_sum = make_float3(0.0, 0.0, 0.0);

			for (int j = 0; j < nbcount; ++j)
			{
				int nbfid = nbfacelist[j];

				if (nbfid < 0 || nbfid >= face_num)
					break;

				float3 nn1 = make_float3(normal[3 * 3 * nbfid + 0], normal[3 * 3 * nbfid + 1], normal[3 * 3 * nbfid + 2]);
				float3 nn2 = make_float3(normal[3 * 3 * nbfid + 3], normal[3 * 3 * nbfid + 4], normal[3 * 3 * nbfid + 5]);
				float3 nn3 = make_float3(normal[3 * 3 * nbfid + 6], normal[3 * 3 * nbfid + 7], normal[3 * 3 * nbfid + 8]);

				if (getDotProduct(this_normal, nn1) >= 0){
					nb_normal_sum.x += nn1.x;
					nb_normal_sum.y += nn1.y;
					nb_normal_sum.z += nn1.z;
				}
				else{
					nb_normal_sum.x -= nn1.x;
					nb_normal_sum.y -= nn1.y;
					nb_normal_sum.z -= nn1.z;
				}

				if (getDotProduct(this_normal, nn2) >= 0) {
					nb_normal_sum.x += nn2.x;
					nb_normal_sum.y += nn2.y;
					nb_normal_sum.z += nn2.z;
				}
				else {
					nb_normal_sum.x -= nn2.x;
					nb_normal_sum.y -= nn2.y;
					nb_normal_sum.z -= nn2.z;
				}

				if (getDotProduct(this_normal, nn3) >= 0) {
					nb_normal_sum.x += nn3.x;
					nb_normal_sum.y += nn3.y;
					nb_normal_sum.z += nn3.z;
				}
				else {
					nb_normal_sum.x -= nn3.x;
					nb_normal_sum.y -= nn3.y;
					nb_normal_sum.z -= nn3.z;
				}

				++count;
			}

			if (count == 0) continue;

			float3 ave_normal;

			ave_normal.x = 0.9 * this_normal.x + 0.1 * nb_normal_sum.x / count;
			ave_normal.y = 0.9 * this_normal.y + 0.1 * nb_normal_sum.y / count;
			ave_normal.z = 0.9 * this_normal.z + 0.1 * nb_normal_sum.z / count;

			float nl = sqrtf(ave_normal.x * ave_normal.x + ave_normal.y * ave_normal.y + ave_normal.z * ave_normal.z);

			if (nl < 0.000001) continue;

			normal[3 * (3 * face_id + i) + 0] = ave_normal.x / nl;
			normal[3 * (3 * face_id + i) + 1] = ave_normal.y / nl;
			normal[3 * (3 * face_id + i) + 2] = ave_normal.z / nl;
		}

		iter++;
		__syncthreads(); // not strictly needed 
	}
}

__global__
void SmoothNormalFaceByFaceKernel(int face_num, const int* face_to_face_mapping, float* normal)
{
	unsigned int face_id = threadIdx.x + blockIdx.x * blockDim.x;

	if (face_id >= face_num)
		return;

	int iter = 0;

	while (iter < 3)
	{
		for (int i = 0; i < 3; ++i) {

			int count = 0;

			float3 this_normal = make_float3(normal[3 * (3 * face_id + i) + 0], normal[3 * (3 * face_id + i) + 1], normal[3 * (3 * face_id + i) + 2]);
			float3 nb_normal_sum = make_float3(0.0, 0.0, 0.0);

			for (int j = 0; j < TOPOLOGY_LIST_LEN; ++j)
			{
				int nbfid = face_to_face_mapping[face_id * TOPOLOGY_LIST_LEN + j];

				if (nbfid < 0 || nbfid >= face_num)
					break;

				float3 nn1 = make_float3(normal[3 * 3 * nbfid + 0], normal[3 * 3 * nbfid + 1], normal[3 * 3 * nbfid + 2]);
				float3 nn2 = make_float3(normal[3 * 3 * nbfid + 3], normal[3 * 3 * nbfid + 4], normal[3 * 3 * nbfid + 5]);
				float3 nn3 = make_float3(normal[3 * 3 * nbfid + 6], normal[3 * 3 * nbfid + 7], normal[3 * 3 * nbfid + 8]);

				if (getDotProduct(this_normal, nn1) >= 0) {
					nb_normal_sum.x += nn1.x;
					nb_normal_sum.y += nn1.y;
					nb_normal_sum.z += nn1.z;
				}
				else {
					nb_normal_sum.x -= nn1.x;
					nb_normal_sum.y -= nn1.y;
					nb_normal_sum.z -= nn1.z;
				}

				if (getDotProduct(this_normal, nn2) >= 0) {
					nb_normal_sum.x += nn2.x;
					nb_normal_sum.y += nn2.y;
					nb_normal_sum.z += nn2.z;
				}
				else {
					nb_normal_sum.x -= nn2.x;
					nb_normal_sum.y -= nn2.y;
					nb_normal_sum.z -= nn2.z;
				}

				if (getDotProduct(this_normal, nn3) >= 0) {
					nb_normal_sum.x += nn3.x;
					nb_normal_sum.y += nn3.y;
					nb_normal_sum.z += nn3.z;
				}
				else {
					nb_normal_sum.x -= nn3.x;
					nb_normal_sum.y -= nn3.y;
					nb_normal_sum.z -= nn3.z;
				}

				++count;
			}

			if (count == 0) continue;

			float3 ave_normal;

			ave_normal.x = 0.9 * this_normal.x + 0.1 * nb_normal_sum.x / count;
			ave_normal.y = 0.9 * this_normal.y + 0.1 * nb_normal_sum.y / count;
			ave_normal.z = 0.9 * this_normal.z + 0.1 * nb_normal_sum.z / count;

			float nl = sqrtf(ave_normal.x * ave_normal.x + ave_normal.y * ave_normal.y + ave_normal.z * ave_normal.z);

			if (nl < 0.000001) continue;

			normal[3 * (3 * face_id + i) + 0] = ave_normal.x / nl;
			normal[3 * (3 * face_id + i) + 1] = ave_normal.y / nl;
			normal[3 * (3 * face_id + i) + 2] = ave_normal.z / nl;
		}

		iter++;
		__syncthreads(); // not strictly needed 
	}
}

void SmoothNormal(int face_num, const unsigned int* face, const int* vid_unique, const int* vert_to_face_mapping, float* normal)
{
	dim3 block(128, 1);
	dim3 grid((face_num + block.x - 1) / block.x, 1);

	SmoothNormalKernel << <grid, block >> > (face_num, face, vid_unique, vert_to_face_mapping, normal);

	CUDA_CHECK_ERROR();
}

void SmoothNormalFaceByFace(int face_num, const int* face_to_face_mapping, float* normal)
{
	dim3 block(128, 1);
	dim3 grid((face_num + block.x - 1) / block.x, 1);

	SmoothNormalFaceByFaceKernel << <grid, block >> > (face_num, face_to_face_mapping, normal);

	CUDA_CHECK_ERROR();
}