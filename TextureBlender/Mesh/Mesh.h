#pragma once

#include <string>
#include "../Common/MeshDef.h"

namespace CuMesh {
	
	enum Device {
		CPU,
		GPU
	};

	struct VOXEL_INT_CODING_PARAM 
	{
		float   sift_x;
		float   sift_y;
		float   sift_z;
		float   voxel_scale;
	};

	class TriMesh {

	public:
		int m_gpuid;

		Device m_dev;
		VOXEL_INT_CODING_PARAM  m_voxel_param;

		int FN;
		int FN_Capacity;

		int VN_coded;
		int FN_coded;

		bool HasNormal() { return is_with_normal; }
		bool HasFace()   { return is_with_face; }
		bool HasTexCoord() { return is_with_texcoord; }

		void* Vert(Device dev) {
			return (dev == CuMesh::CPU ? vert_cpu : vert_gpu);
		}

		void* Face(Device dev) {
			return (dev == CuMesh::CPU ? face_cpu : face_gpu);
		}

		void* Normal(Device dev) {
			return (dev == CuMesh::CPU ? normal_cpu : normal_gpu);
		}

		void* TexCoord(Device dev) {
			return (dev == CuMesh::CPU ? texcoord_cpu : texcoord_gpu);
		}

		void* Coded_Vert(Device dev) {
			return (dev == CuMesh::CPU ? coded_vert_cpu : coded_vert_gpu);
		}

		void* Coded_Index(Device dev) {
			return (dev == CuMesh::CPU ? coded_index_cpu : coded_index_gpu);
		}

		void* Coded_TexCoord(Device dev) {
			return (dev == CuMesh::CPU ? coded_texcoord_cpu : coded_texcoord_gpu);
		}

		void* Texture_Raw(Device dev) {
			return (dev == CuMesh::CPU ? texture_rgb_cpu : texture_rgb_gpu);
		}

		void* FaceFaceTopology(Device dev) {
			return (dev == CuMesh::CPU ? NULL : face_to_face_mapping);
		}

		void SyncTexture();
		void SyncData();
		void HasSetTexCoord();
		void ComputeNormal();

		void GetMeshCenter();
		void CodingBeforeTransfer();

		void Update(const float *vertex, const size_t szV, int from_gpuid);
		void SaveToPly(std::string filename, const size_t fid, bool isBinaryFile);
		void SaveCodedMeshToPly(std::string filename, const size_t fid);
		void SaveTexture(std::string filename);

		TriMesh();
		~TriMesh();

	private:

		bool is_with_normal;
		bool is_with_texcoord;
		bool is_with_face;
		bool is_with_topology;

		// mesh on CPU
		TYPE_VERT*	vert_cpu;
		TYPE_FACE*	face_cpu;
		TYPE_NORM*	normal_cpu;
		TYPE_TEXC*	texcoord_cpu;

		// mesh on GPU
		void*	vert_gpu;
		void*	face_gpu;
		void*	normal_gpu;
		void*	texcoord_gpu;

		// coded mesh for transfer
		void*	coded_vert_gpu;
		void*	coded_index_gpu;
		void*	coded_face_gpu;
		void*	coded_normal_gpu;
		void*	coded_texcoord_gpu;

		TYPE_CODED_VERT*	coded_vert_cpu;
		TYPE_CODED_INDX*	coded_index_cpu;
		TYPE_CODED_FACE*	coded_face_cpu;
		TYPE_CODED_NORM*	coded_normal_cpu;
		TYPE_CODED_TEXC*	coded_texcoord_cpu;

		void*	texture_rgb_cpu;
		void*   texture_rgb_gpu;

		// for mesh topology evaluation

		TYPE_VERT*	vert_sorted_gpu;

		int*	org_vert_index;
		int*	uqe_vert_index;

		int*    vert_to_face_mapping;
		int*    face_to_face_mapping;
	};

}