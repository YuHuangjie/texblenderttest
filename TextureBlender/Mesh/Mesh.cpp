#include  <numeric>

#include "Mesh.h"
#include "opencv2/opencv.hpp"
#include "../Common/comm.h"

using namespace CuMesh;
//using namespace TexBlender;

void ComputeMeshCenter(int face_num, const float* verts, float scale, float& cpu_x, float& cpu_y, float& cpu_z);
void CalculateNormal(int face_num, const float* verts, float* normal);
void SmoothNormal(int face_num, const unsigned int* face, const int* vid_unique, const int* vert_to_face_mapping, float* normal);
void SmoothNormalFaceByFace(int face_num, const int* face_to_face_mapping, float* normal);

void VertexReducing(int face_num, TYPE_CODED_VERT*  coded_verts, TYPE_CODED_INDX* index_array, int* fn_vert_num);
void VertexTexCoordReducing(int face_num, TYPE_FACE* face, TYPE_CODED_VERT* coded_verts, TYPE_CODED_TEXC* coded_texcs, int* org_vert_index, int* unique_vert_index, TYPE_CODED_INDX* out_index_array, int* vert_num_reduced);

void VertexCoding(int face_num, const float* org_verts, TYPE_CODED_VERT*  coded_verts, float shift_x, float shift_y, float shift_z, float voxel_scale);
void TexCoordCoding(int face_num, const float* org_uvs, TYPE_CODED_TEXC* coded_uvs);

void TopologyInterpretation(int face_num, TYPE_VERT* verts_copy, TYPE_FACE* faces, int* org_vert_index, int* unique_vert_index, int* vert_to_face_mapping, int* face_to_face_mapping);

TriMesh::TriMesh()
{
	m_gpuid = 0;

	m_dev = CPU;

	FN = 0;
	FN_Capacity = 0;

	FN_coded = 0;
	VN_coded = 0;

	is_with_normal = false;
	is_with_texcoord = false;
	is_with_face = false;
	is_with_topology = false;

	vert_cpu = 0;
	face_cpu = 0;
	normal_cpu = 0;
	texcoord_cpu = 0;

	vert_gpu = 0;
	face_gpu = 0;
	normal_gpu = 0;
	texcoord_gpu = 0;

	// mesh coding
	m_voxel_param.voxel_scale  = 1.0;
	m_voxel_param.sift_x = 0.0;
	m_voxel_param.sift_y = 0.0;
	m_voxel_param.sift_z = 0.0;

	coded_vert_gpu = 0;
	coded_index_gpu = 0;
	coded_face_gpu = 0;
	coded_normal_gpu = 0;
	coded_texcoord_gpu = 0;

	coded_vert_cpu = 0;
	coded_index_cpu = 0;
	coded_face_cpu = 0;
	coded_normal_cpu = 0;
	coded_texcoord_cpu = 0;

	// topology
	vert_sorted_gpu = 0;

	org_vert_index = 0;
	uqe_vert_index = 0;

	vert_to_face_mapping = 0;
	face_to_face_mapping = 0;

	texture_rgb_cpu = new unsigned char[3 * TEX_WIDTH * TEX_HEIGHT];
	CUDA_SAFE_CALL(cudaMalloc(&texture_rgb_gpu, 3 * TEX_WIDTH * TEX_HEIGHT));
}

TriMesh::~TriMesh() {

	if (vert_cpu != 0)
		delete[] vert_cpu;

	if (face_cpu != 0)
		delete[] face_cpu;

	if (normal_cpu != 0)
		delete[] normal_cpu;

	if (texcoord_cpu != 0)
		delete[] texcoord_cpu;

	if (vert_gpu != 0)
		CUDA_SAFE_CALL(cudaFree(vert_gpu));

	if (face_gpu != 0)
		CUDA_SAFE_CALL(cudaFree(face_gpu));

	if (normal_gpu != 0)
		CUDA_SAFE_CALL(cudaFree(normal_gpu));

	if (texcoord_gpu != 0)
		CUDA_SAFE_CALL(cudaFree(texcoord_gpu));

	if (coded_vert_cpu != 0)
		delete[] coded_vert_cpu;

	if (coded_index_cpu != 0)
		delete[] coded_index_cpu;

	if (coded_face_cpu != 0)
		delete[] coded_face_cpu;

	if (coded_normal_cpu != 0)
		delete[] coded_normal_cpu;

	if (coded_texcoord_cpu != 0)
		delete[] coded_texcoord_cpu;

	if (texture_rgb_cpu != 0)
		delete[] texture_rgb_cpu;

	if (coded_vert_gpu != 0)
		CUDA_SAFE_CALL(cudaFree(coded_vert_gpu));

	if (coded_index_gpu != 0)
		CUDA_SAFE_CALL(cudaFree(coded_index_gpu));

	if (coded_face_gpu != 0)
		CUDA_SAFE_CALL(cudaFree(coded_face_gpu));

	if (coded_normal_gpu != 0)
		CUDA_SAFE_CALL(cudaFree(coded_normal_gpu));

	if (coded_texcoord_gpu != 0)
		CUDA_SAFE_CALL(cudaFree(coded_texcoord_gpu));

	// Topology
	if (vert_sorted_gpu != 0)
		CUDA_SAFE_CALL(cudaFree(vert_sorted_gpu));

	if (org_vert_index != 0)
		CUDA_SAFE_CALL(cudaFree(org_vert_index));

	if (uqe_vert_index != 0)
		CUDA_SAFE_CALL(cudaFree(uqe_vert_index));

	if (vert_to_face_mapping != 0)
		CUDA_SAFE_CALL(cudaFree(vert_to_face_mapping));

	if (face_to_face_mapping != 0)
		CUDA_SAFE_CALL(cudaFree(face_to_face_mapping));

}

void TriMesh::Update(const float *inV, const size_t szV, int from_gpuid)
{
	CUDA_SAFE_CALL(cudaSetDevice(m_gpuid));

	int nfaces = (szV / (MULT_VERT * sizeof(TYPE_VERT)));

	if (nfaces > FN_Capacity)
	{
		FN_Capacity = 2 * nfaces;

		if (vert_cpu != NULL)
			delete[] vert_cpu;

		if (face_cpu != NULL)
			delete[] face_cpu;

		if (normal_cpu != NULL)
			delete[] normal_cpu;

		if (texcoord_cpu != NULL)
			delete[] texcoord_cpu;

		vert_cpu	 = new TYPE_VERT[FN_Capacity * MULT_VERT];
		face_cpu	 = new TYPE_FACE[FN_Capacity * MULT_FACE];
		normal_cpu	 = new TYPE_NORM[FN_Capacity * MULT_NORM];
		texcoord_cpu = new TYPE_TEXC[FN_Capacity * MULT_TEXC];

		if (vert_gpu != NULL)
			CUDA_SAFE_CALL(cudaFree(vert_gpu));

		if (face_gpu != NULL)
			CUDA_SAFE_CALL(cudaFree(face_gpu));

		if (normal_gpu != NULL)
			CUDA_SAFE_CALL(cudaFree(normal_gpu));

		if (texcoord_gpu != NULL)
			CUDA_SAFE_CALL(cudaFree(texcoord_gpu));

		CUDA_SAFE_CALL(
			cudaMalloc(&vert_gpu,  FN_Capacity * MULT_VERT * sizeof(TYPE_VERT))
		);

		CUDA_SAFE_CALL(
			cudaMalloc(&face_gpu,  FN_Capacity * MULT_FACE * sizeof(TYPE_FACE))
		);

		CUDA_SAFE_CALL(
			cudaMalloc(&normal_gpu, FN_Capacity * MULT_NORM * sizeof(TYPE_NORM))
		);

		CUDA_SAFE_CALL(
			cudaMalloc(&texcoord_gpu, FN_Capacity * MULT_TEXC * sizeof(TYPE_TEXC))
		);

		if (USE_MESH_CODING)
		{
			if (coded_vert_cpu != NULL)
				delete[] coded_vert_cpu;

			if (coded_face_cpu != NULL)
				delete[] coded_face_cpu;

			if (coded_normal_cpu != NULL)
				delete[] coded_normal_cpu;

			if (coded_texcoord_cpu != NULL)
				delete[] coded_texcoord_cpu;
			
			if (REMOVE_DUPLICATE_VERTEX)
			{
				if (coded_index_cpu != NULL)
					delete[] coded_index_cpu;
			}

			coded_vert_cpu     = new TYPE_CODED_VERT[FN_Capacity  * MULT_CODED_VERT];
			coded_face_cpu     = new TYPE_CODED_FACE[FN_Capacity  * MULT_CODED_FACE];
			coded_normal_cpu   = new TYPE_CODED_NORM[FN_Capacity  * MULT_CODED_NORM];
			coded_texcoord_cpu = new TYPE_CODED_TEXC[FN_Capacity  * MULT_CODED_TEXC];

			if (REMOVE_DUPLICATE_VERTEX)
			{
				coded_index_cpu = new TYPE_CODED_INDX[FN_Capacity * MULT_CODED_INDX];
			}

			if (coded_vert_gpu != NULL)
				CUDA_SAFE_CALL(cudaFree(coded_vert_gpu));

			if (coded_face_gpu != NULL)
				CUDA_SAFE_CALL(cudaFree(coded_face_gpu));

			if (coded_normal_gpu != NULL)
				CUDA_SAFE_CALL(cudaFree(coded_normal_gpu));

			if (coded_texcoord_gpu != NULL)
				CUDA_SAFE_CALL(cudaFree(coded_texcoord_gpu));

			if (REMOVE_DUPLICATE_VERTEX)
			{
				if (coded_index_gpu != NULL)
					CUDA_SAFE_CALL(cudaFree(coded_index_gpu));
			}

			CUDA_SAFE_CALL(
				cudaMalloc(&coded_vert_gpu,  FN_Capacity * MULT_CODED_VERT * sizeof(TYPE_CODED_VERT))
			);

			if (REMOVE_DUPLICATE_VERTEX)
			{
				CUDA_SAFE_CALL(
					cudaMalloc(&coded_index_gpu, FN_Capacity * MULT_CODED_INDX * sizeof(TYPE_CODED_INDX))
				);
			}
			
			CUDA_SAFE_CALL(
				cudaMalloc(&coded_face_gpu,  FN_Capacity * MULT_CODED_FACE * sizeof(TYPE_CODED_FACE))
			);

			CUDA_SAFE_CALL(
				cudaMalloc(&coded_normal_gpu,   FN_Capacity * MULT_CODED_NORM * sizeof(TYPE_CODED_NORM))
			);

			CUDA_SAFE_CALL(
				cudaMalloc(&coded_texcoord_gpu, FN_Capacity * MULT_CODED_TEXC * sizeof(TYPE_CODED_TEXC))
			);
		}

		if (INTERPRETATE_MESH_TOPOLOGY)
		{
			if (vert_sorted_gpu != 0)
				CUDA_SAFE_CALL(cudaFree(vert_sorted_gpu));

			if (org_vert_index != 0)
				CUDA_SAFE_CALL(cudaFree(org_vert_index));

			if (uqe_vert_index != 0)
				CUDA_SAFE_CALL(cudaFree(uqe_vert_index));

			if (vert_to_face_mapping != 0)
				CUDA_SAFE_CALL(cudaFree(vert_to_face_mapping));

			if (face_to_face_mapping != 0)
				CUDA_SAFE_CALL(cudaFree(face_to_face_mapping));

			
			CUDA_SAFE_CALL(
				cudaMalloc(&vert_sorted_gpu, FN_Capacity * MULT_VERT * sizeof(TYPE_VERT))
			);

			CUDA_SAFE_CALL(
				cudaMalloc(&org_vert_index, FN_Capacity * MULT_FACE * sizeof(int))
			);

			CUDA_SAFE_CALL(
				cudaMalloc(&uqe_vert_index, FN_Capacity * MULT_FACE * sizeof(int))
			);

			CUDA_SAFE_CALL(
				cudaMalloc(&vert_to_face_mapping, FN_Capacity * MULT_FACE * TOPOLOGY_LIST_LEN * sizeof(int))
			);

			CUDA_SAFE_CALL(
				cudaMalloc(&face_to_face_mapping, FN_Capacity * TOPOLOGY_LIST_LEN * sizeof(int))
			);

		}

	}

	/* Copy vertex */
	if (from_gpuid < 0) {
		// from CPU
		CUDA_SAFE_CALL(
			cudaMemcpy(vert_gpu, inV, szV, cudaMemcpyHostToDevice));
	}
	else if (m_gpuid == from_gpuid) {
		CUDA_SAFE_CALL(
			cudaMemcpy(vert_gpu, inV, szV, cudaMemcpyDeviceToDevice));
	}
	else {
		CUDA_SAFE_CALL(
			cudaMemcpyPeer(vert_gpu, m_gpuid, inV, from_gpuid, szV)
			);
	}

	/* Generate indices */
	CUDA_SAFE_CALL(cudaSetDevice(m_gpuid));

	std::vector<TYPE_FACE> vert_id(FN_Capacity * MULT_FACE, 0);
	std::iota(std::begin(vert_id), std::end(vert_id), TYPE_FACE(0));

	CUDA_SAFE_CALL(
		cudaMemcpy(face_gpu, vert_id.data(), 
			FN_Capacity * MULT_FACE * sizeof(TYPE_FACE), cudaMemcpyHostToDevice));
		
	CUDA_SAFE_CALL(cudaSetDevice(m_gpuid));

	m_dev = GPU;
	FN = nfaces;

	is_with_normal   = false;
	is_with_face	 = true;
	is_with_texcoord = false;

	if (FN >= 100)
	{
		if (INTERPRETATE_MESH_TOPOLOGY)
		{
			CUDA_SAFE_CALL(
				cudaMemcpy(vert_sorted_gpu,
					vert_gpu,
					nfaces * MULT_VERT * sizeof(TYPE_VERT),
					cudaMemcpyDeviceToDevice));

			TopologyInterpretation(FN, vert_sorted_gpu, (TYPE_FACE*)face_gpu, org_vert_index, uqe_vert_index, vert_to_face_mapping, face_to_face_mapping);

			is_with_topology = true;
		}

		ComputeNormal();
	}

	CUDA_CHECK_ERROR();
}

void TriMesh::HasSetTexCoord()
{
	is_with_texcoord = true;
}

void CuMesh::TriMesh::SyncTexture()
{
	if (m_dev == GPU)
	{
		CUDA_SAFE_CALL(cudaMemcpy(texture_rgb_cpu, texture_rgb_gpu,
			TEX_WIDTH*TEX_HEIGHT * 3 * sizeof(uint8_t),	cudaMemcpyDeviceToHost));
	}
	else
	{
		CUDA_SAFE_CALL(cudaMemcpy(texture_rgb_gpu, texture_rgb_cpu,
			TEX_WIDTH * TEX_HEIGHT * 3 * sizeof(uint8_t), cudaMemcpyHostToDevice));
	}
}

void TriMesh::SyncData()
{
	if (m_dev == GPU)
	{
		CUDA_SAFE_CALL(
			cudaMemcpy(vert_cpu, vert_gpu,
				FN * MULT_VERT * sizeof(TYPE_VERT),
				cudaMemcpyDeviceToHost));

		if (is_with_face)
			CUDA_SAFE_CALL(
				cudaMemcpy(face_cpu, face_gpu,
					FN * MULT_FACE * sizeof(TYPE_FACE),
					cudaMemcpyDeviceToHost));

		if (is_with_normal)
			CUDA_SAFE_CALL(
				cudaMemcpy(normal_cpu, normal_gpu,
					FN * MULT_NORM * sizeof(TYPE_NORM),
					cudaMemcpyDeviceToHost));

		if (is_with_texcoord)
			CUDA_SAFE_CALL(
				cudaMemcpy(texcoord_cpu, texcoord_gpu,
					FN * MULT_TEXC * sizeof(TYPE_TEXC),
					cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(
			cudaMemcpy(texture_rgb_cpu, texture_rgb_gpu,
				TEX_WIDTH*TEX_HEIGHT * 3 * sizeof(uint8_t),
				cudaMemcpyDeviceToHost));
	}
	else
	{
		CUDA_SAFE_CALL(
			cudaMemcpy(vert_gpu, vert_cpu,
				FN * MULT_VERT * sizeof(TYPE_VERT),
				cudaMemcpyHostToDevice));

		if (is_with_face)
			CUDA_SAFE_CALL(
				cudaMemcpy(face_gpu, face_cpu,
					FN * MULT_FACE * sizeof(TYPE_FACE),
					cudaMemcpyHostToDevice));

		if (is_with_normal)
			CUDA_SAFE_CALL(
				cudaMemcpy(normal_gpu, normal_cpu,
					FN * MULT_NORM * sizeof(TYPE_NORM),
					cudaMemcpyHostToDevice));

		if (is_with_texcoord)
			CUDA_SAFE_CALL(
				cudaMemcpy(texcoord_gpu, texcoord_cpu,
					FN * MULT_TEXC * sizeof(TYPE_TEXC),
					cudaMemcpyHostToDevice));
		CUDA_SAFE_CALL(
			cudaMemcpy(texture_rgb_gpu, texture_rgb_cpu,
				TEX_WIDTH * TEX_HEIGHT * 3 * sizeof(uint8_t),
				cudaMemcpyHostToDevice));
	}
}

void TriMesh::ComputeNormal()
{
	CalculateNormal(FN, (TYPE_VERT*)vert_gpu, (TYPE_NORM*)normal_gpu);

	is_with_normal = true;

	if (is_with_topology)
	{
		//SmoothNormal(FN, (unsigned int*)face_gpu, (int*)uqe_vert_index, (int*)vert_to_face_mapping, (float*)normal_gpu);
		SmoothNormalFaceByFace(FN, (int*)face_to_face_mapping, (float*)normal_gpu);
	}
}

void TriMesh::SaveToPly(std::string filename, const size_t fid, bool isBinaryFile)
{
	SyncData();

	std::ofstream outputFileStream;

	if (isBinaryFile)
	{
		outputFileStream.open(filename, std::ofstream::out | std::ofstream::binary);
		outputFileStream << "ply\nformat binary_little_endian 1.0\n";
	}
	else
	{
		outputFileStream.open(filename, std::ofstream::out);
		outputFileStream << "ply\nformat ascii 1.0\n";
	}

	outputFileStream << "comment TextureFile uv" + std::to_string(fid) + ".png\n";

	/// write data to ply
	outputFileStream << "element vertex " << FN * 3 << "\n";
	outputFileStream << "property float x\nproperty float y\nproperty float z" << std::endl;

	if (is_with_normal)
		outputFileStream << "property float nx\nproperty float ny\nproperty float nz" << std::endl;

	if (is_with_face)
	{
		outputFileStream << "element face " << FN << std::endl;
		outputFileStream << "property list uchar int vertex_indices" << std::endl;
	}

	if (is_with_texcoord)
		outputFileStream << "property list uchar float texcoord" << std::endl;

	outputFileStream << "end_header" << std::endl;

	if (isBinaryFile)
	{
		for (int i = 0, ind3 = 0; i < FN; ++i, ind3 += 3)
		{
			outputFileStream.write((char *)(&vert_cpu[ind3]), sizeof(float) * 3);

			if (is_with_normal)
				outputFileStream.write((char *)(&normal_cpu[ind3]), sizeof(float) * 3);
		}

		if (is_with_face)
		{
			for (int i = 0; i < FN; ++i)
			{
				int tri = 3;
				outputFileStream.write((char *)(&tri), sizeof(int));
				outputFileStream.write((char *)(&face_cpu[i]), sizeof(int) * 3);

				if (is_with_texcoord)
				{
					int tex = 6;
					outputFileStream.write((char *)(&tex), sizeof(int));
					outputFileStream.write((char *)(&texcoord_cpu[i]), sizeof(float) * 6);
				}
			}
		}
	}
	else
	{
		for (int i = 0, ind3 = 0; i < FN * 3; ++i, ind3 += 3)
		{
			outputFileStream << vert_cpu[ind3] << ' ' << vert_cpu[ind3 + 1] << ' ' << vert_cpu[ind3 + 2];

			if (is_with_normal)
				outputFileStream << ' ' << normal_cpu[ind3] << ' ' << normal_cpu[ind3 + 1] << ' ' << normal_cpu[ind3 + 2];

			outputFileStream << '\n';
		}

		if (is_with_face)
		{
			for (int i = 0; i < FN; ++i)
			{
				outputFileStream << 3 << " " << face_cpu[i * 3] << ' ' << face_cpu[i * 3 + 1] << ' ' << face_cpu[i * 3 + 2];

				if (is_with_texcoord)
				{
					outputFileStream << " " << 6 << " "
						<< texcoord_cpu[i * 6] << ' '
						<< texcoord_cpu[i * 6 + 1] << ' '
						<< texcoord_cpu[i * 6 + 2] << ' '
						<< texcoord_cpu[i * 6 + 3] << ' '
						<< texcoord_cpu[i * 6 + 4] << ' '
						<< texcoord_cpu[i * 6 + 5];
				}

				outputFileStream << '\n';
			}
		}
	}

	outputFileStream.flush();
	outputFileStream.close();
}

void TriMesh::SaveCodedMeshToPly(std::string filename, const size_t fid)
{
	std::ofstream outputFileStream;

	outputFileStream.open(filename, std::ofstream::out);

	outputFileStream << "ply\nformat ascii 1.0\n";
	outputFileStream << "comment TextureFile uv" + std::to_string(fid) + ".png\n";
	outputFileStream << "element vertex " << VN_coded << "\n";
	outputFileStream << "property float x\nproperty float y\nproperty float z" << std::endl;

	if (is_with_face)
	{
		outputFileStream << "element face " << FN_coded << std::endl;
		outputFileStream << "property list uchar int vertex_indices" << std::endl;
	}

	if (is_with_texcoord)
		outputFileStream << "property list uchar float texcoord" << std::endl;

	outputFileStream << "end_header" << std::endl;

	for (int i = 0; i < VN_coded; ++i)
	{
		int code_vert = coded_vert_cpu[i];
		float p[3];

		int ipz = code_vert & 0x3FF;
		int ipy = (code_vert >> 10) & 0x3FF;
		int ipx = (code_vert >> 20) & 0x3FF;

		p[0] = (ipx - m_voxel_param.sift_x) / m_voxel_param.voxel_scale;
		p[1] = (ipy - m_voxel_param.sift_y) / m_voxel_param.voxel_scale;
		p[2] = (ipz - m_voxel_param.sift_z) / m_voxel_param.voxel_scale;

		outputFileStream << p[0] << ' ' << p[1] << ' ' << p[2];
		outputFileStream << '\n';
	}

	if (is_with_face)
	{
		for (int i = 0; i < FN_coded; ++i)
		{
			int idx1 = REMOVE_DUPLICATE_VERTEX ? coded_index_cpu[i * 3] : i * 3;
			int idx2 = REMOVE_DUPLICATE_VERTEX ? coded_index_cpu[i * 3 + 1] : i * 3 + 1;
			int idx3 = REMOVE_DUPLICATE_VERTEX ? coded_index_cpu[i * 3 + 2] : i * 3 + 2;
			outputFileStream << 3 << " " << idx1 << ' ' << idx2 << ' ' << idx3;

			if (is_with_texcoord)
			{
				outputFileStream << " " << 6 << " "
					<< coded_texcoord_cpu[idx1 * 2 + 0] / 65535.0 << ' '
					<< coded_texcoord_cpu[idx1 * 2 + 1] / 65535.0 << ' '
					<< coded_texcoord_cpu[idx2 * 2 + 0] / 65535.0 << ' '
					<< coded_texcoord_cpu[idx2 * 2 + 1] / 65535.0 << ' '
					<< coded_texcoord_cpu[idx3 * 2 + 0] / 65535.0 << ' '
					<< coded_texcoord_cpu[idx3 * 2 + 1] / 65535.0;
			}

			outputFileStream << '\n';
		}
	}

	outputFileStream.flush();
	outputFileStream.close();

}

void TriMesh::GetMeshCenter()
{
	if (m_dev == GPU)
	{
		ComputeMeshCenter(FN, (float*)vert_gpu, m_voxel_param.voxel_scale, m_voxel_param.sift_x, m_voxel_param.sift_y, m_voxel_param.sift_z);
	}
	else if (m_dev == CPU)
	{
		double sumx = 0.0, sumy = 0.0, sumz = 0.0;

		for (int i = 0; i < 3 * FN; ++i)
		{
			sumx += vert_cpu[3 * i + 0];
			sumy += vert_cpu[3 * i + 1];
			sumz += vert_cpu[3 * i + 2];
		}

		m_voxel_param.sift_x = sumx / FN;
		m_voxel_param.sift_y = sumy / FN;
		m_voxel_param.sift_z = sumz / FN;
	}
}

#if USE_MESH_CODING
void TriMesh::CodingBeforeTransfer()
{
	if (m_dev != GPU) {
		return;
	}

	GetMeshCenter();
	VertexCoding(FN, (float*)vert_gpu, (TYPE_CODED_VERT*)coded_vert_gpu, m_voxel_param.sift_x, 
		m_voxel_param.sift_y, m_voxel_param.sift_z, m_voxel_param.voxel_scale);

	if (is_with_texcoord)
	{
		TexCoordCoding(FN, (float*)texcoord_gpu, (TYPE_CODED_TEXC*)coded_texcoord_gpu);

		if (REMOVE_DUPLICATE_VERTEX) {

			VertexTexCoordReducing(FN, (unsigned int*)face_gpu, (int*)coded_vert_gpu, 
				(TYPE_CODED_TEXC*)coded_texcoord_gpu, org_vert_index, uqe_vert_index, 
				(int*)coded_index_gpu, &VN_coded);

			FN_coded = FN;

			CUDA_SAFE_CALL(
				cudaMemcpy(coded_vert_cpu, coded_vert_gpu,
					VN_coded * MULT_CODED_VERT_PERVN * sizeof(TYPE_CODED_VERT),
					cudaMemcpyDeviceToHost));
				
			CUDA_SAFE_CALL(
				cudaMemcpy(coded_texcoord_cpu, coded_texcoord_gpu,
					VN_coded * MULT_CODED_TEXC_PERVN * sizeof(TYPE_CODED_TEXC),
					cudaMemcpyDeviceToHost));

			CUDA_SAFE_CALL(
				cudaMemcpy(coded_index_cpu, coded_index_gpu,
					FN_coded * MULT_CODED_INDX * sizeof(TYPE_CODED_INDX),
					cudaMemcpyDeviceToHost));
		}
		else {
			FN_coded = FN;
			VN_coded = FN * 3;
			CUDA_SAFE_CALL(
				cudaMemcpy(coded_vert_cpu, coded_vert_gpu,
					VN_coded * MULT_CODED_VERT_PERVN * sizeof(TYPE_CODED_VERT),
					cudaMemcpyDeviceToHost));

			CUDA_SAFE_CALL(
				cudaMemcpy(coded_texcoord_cpu, coded_texcoord_gpu,
					VN_coded * MULT_CODED_TEXC_PERVN * sizeof(TYPE_CODED_TEXC),
					cudaMemcpyDeviceToHost));
		}
	}
}
#endif 

void TriMesh::SaveTexture(std::string filename)
{
	cv::Mat tex(TEX_HEIGHT, TEX_WIDTH, CV_8UC3, texture_rgb_cpu);
	cv::cvtColor(tex, tex, CV_BGR2RGB);
	imwrite(filename, tex);
}
