#pragma once

#define USE_TEXTURE     true

#define REMOVE_DUPLICATE_VERTEX	false

#define USE_MESH_CODING true
#define USE_NORMAL_WEIGHTING true

#define INTERPRETATE_MESH_TOPOLOGY true

//requires good mask if set to true
#define MASK_RGBIMAGE_FOR_TEXTURE  true

#define TEXTURE_SEND_SIZE  (3 * TEX_WIDTH * TEX_HEIGHT * sizeof(unsigned char))
#define TEXTURE_HEADER 12  

#define MULT_VERT	9
#define MULT_NORM	9
#define MULT_FACE	3
#define MULT_TEXC	6
#define MULT_VISB	3

#define TYPE_VERT	float
#define TYPE_NORM	float
#define TYPE_FACE	unsigned int
#define TYPE_TEXC	float
#define TYPE_VISB	int

// mesh coding

#define MULT_CODED_VERT  3
#define MULT_CODED_NORM  3
#define MULT_CODED_INDX  3
#define MULT_CODED_FACE  1
#define MULT_CODED_TEXC  6

#define MULT_CODED_VERT_PERVN  1
#define MULT_CODED_TEXC_PERVN  2

#define TYPE_CODED_VERT  int
#define TYPE_CODED_INDX	 int
#define TYPE_CODED_FACE  int
#define TYPE_CODED_NORM  int
#define TYPE_CODED_TEXC  unsigned short

// topology evaluation

#define TOPOLOGY_LIST_LEN  20