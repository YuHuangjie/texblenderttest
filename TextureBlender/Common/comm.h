#pragma once

#include <sstream>
#include <stdexcept>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>

#include "../Common/cuda/helper_cuda.h"
#include "../Common/cuda/helper_functions.h"

#include <texture_types.h>
#include <texture_fetch_functions.h>

#define TEX_WIDTH 1024
#define TEX_HEIGHT 1024

#define MAX_BINS   5000

#define TEX_PADDING 6

#define LOOSE_PERCENT	 1.40
#define MAX_CHILD_NUMBER (20 * MAX_BINS + 2)

#define CUDA_SAFE_CALL(err) _CUDA_SAFE_CALL(err, __FILE__, __LINE__)
#define CUDA_CHECK_ERROR()    __cudaCheckError( __FILE__, __LINE__ )

inline void _CUDA_SAFE_CALL(cudaError err, const char *file, const int line)
{
	if (cudaSuccess != err) {
		std::ostringstream oss;
		oss << file << "(" << line << ")" << " : cudaSafeCall() Runtime API error : "
			<< cudaGetErrorString(err) << std::endl;
		throw std::runtime_error(oss.str());
	}
};

inline void __cudaCheckError(const char *file, const int line)
{
	cudaError err = cudaGetLastError();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "cudaCheckError() failed at %s:%i : %s\n",
			file, line, cudaGetErrorString(err));
		//exit(-1);
	}

	// More careful checking. However, this will affect performance.
	// Comment away if needed.
	err = cudaDeviceSynchronize();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "cudaCheckError() with sync failed at %s:%i : %s\n",
			file, line, cudaGetErrorString(err));
		//exit(-1);
	}
};


struct Bin2D {

	int vid;
	int label;

	int im_x;
	int im_y;

	int W;
	int H;

	int tex_x;
	int tex_y;
};

struct TreeNode {

	int BinId;

	int left_child;
	int right_child;

	int x;
	int y;

	int Wr;
	int Hr;
};

struct STimer {

	enum TimerMode {
		SILENT,
		CONSOLE,
		FILE
	};

	struct TimerParam {
		TimerMode mode;
		std::stringstream ss;

		LARGE_INTEGER t_start;
		LARGE_INTEGER t1, t2;
		LARGE_INTEGER frequency;

		double elapsedTime;
	};

	static TimerParam timers;

	static void SetTimerMode(TimerMode m) {
		timers.mode = m;
	}
	
	static void Start() {
		if (timers.mode == SILENT)
			return;

		timers.ss.str("");
		QueryPerformanceFrequency(&timers.frequency);
		QueryPerformanceCounter(&timers.t_start);
		timers.t1 = timers.t_start;
	}

	static void Elapse(std::string measure_name) {
		
		if (timers.mode == SILENT) return;

		QueryPerformanceCounter(&timers.t2);
		timers.elapsedTime = (timers.t2.QuadPart - timers.t1.QuadPart) * 1000.0 / timers.frequency.QuadPart;

		if (timers.mode == CONSOLE)
			std::cout << measure_name.c_str() << " Process Time:\t" << timers.elapsedTime << " msecs" << std::endl;
		else
			timers.ss << measure_name.c_str() << " Process Time:\t" << timers.elapsedTime << " msecs" << std::endl;

		QueryPerformanceCounter(&timers.t1);
	}

	static void Total() {
		
		if (timers.mode == SILENT)
			return;
		
		QueryPerformanceCounter(&timers.t2);
		timers.elapsedTime = (timers.t2.QuadPart - timers.t_start.QuadPart) * 1000.0 / timers.frequency.QuadPart;

		if (timers.mode == CONSOLE)
			std::cout << " Total Time:\t" << timers.elapsedTime << " msecs" << std::endl;
		else
		{
			timers.ss << " Total Time:\t" << timers.elapsedTime << " msecs" << std::endl;

			std::ofstream ofstr("time_log.txt", std::ios_base::app | std::ios_base::out);

			ofstr.write(timers.ss.str().c_str(), timers.ss.str().length());
			ofstr.close();
		}
	}
};

struct SDebugInfo
{
	bool SaveFile;
	int  FrameNumber;

    std::string Folder;

	void AddFrameNumber()
	{
		FrameNumber++;
	}

	SDebugInfo()
	{
		SaveFile = false;
        FrameNumber = 0;

        Folder = "Save_RealTime\\";

        CreateDirectoryA(Folder.c_str(), NULL);
	}
};