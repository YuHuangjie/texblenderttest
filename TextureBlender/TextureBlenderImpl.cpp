#include <numeric>
#include <algorithm>
#include <mutex>
#include <condition_variable>

#include "TextureBlenderImpl.h"
#include "TextureThread.h"
#include "TexBlender/TexBlenderRT.h"
#include "MeshQueue.h"

#include "../Visualizer/GLCallbacks.h"

/* global condition variables and locks */
std::mutex gLckInit;
std::mutex gLckEndTexturing;
std::mutex gLckBegTexturing;

std::condition_variable gCVIntTexturing;
std::condition_variable gCVEndTexturing;
std::condition_variable gCVBegTexturing;

const float TextureBlenderImpl::PACKING_LOOSE = 2.0f;

/* Defined in CreateGLContext.cpp */
void * CreateOpenGLContext(const char *title, const int width, const int height);

TextureBlenderImpl::TextureBlenderImpl(const size_t NCAM)
	: _isConfigured(false),
	_stopTexturing(true),
	_texureThread(),
	_pMeshQueue(nullptr),
	_pTexBlender(nullptr),
	_nCams(NCAM),
	_cIsFixed(NCAM, false),
	_Ks(NCAM),
	_Ms(NCAM),
	_Ws(NCAM, 0),
	_Hs(NCAM, 0)
{
}

TextureBlenderImpl::~TextureBlenderImpl()
{
	/* end texturing thread */
	if (_texureThread.joinable()) {
		_stopTexturing = true;
		_texureThread.join();
	}
}

size_t TextureBlenderImpl::GetTextureSize() const
{
	if (!_isConfigured) return 0;
	else				return TEX_WIDTH * TEX_HEIGHT * 3;
}

#define CHECK_ID(id) do { if ((id) < 0 || (id) >= _nCams) return false; } while (0)

bool TextureBlenderImpl::SetCamera(const size_t id, const int32_t w, const int32_t h,
	const float intrin[9], const float extrin[16])
{
	CHECK_ID(id);

	/* Sanity check */
	if (w < 1 || h < 1) { return false; }

	/* Couldn't change camera parametes once it's set. */
	if (_cIsFixed[id]) { return false; }

	/* Update camera resolution and parameters */
	_Ws[id] = w;
	_Hs[id] = h;
	_Ks[id] = cv::Mat(3, 3, CV_32FC1);
	std::memcpy(_Ks[id].ptr<float>(), intrin, sizeof(float)*9);
	_Ms[id] = cv::Mat(4, 4, CV_32FC1);
	std::memcpy(_Ms[id].ptr<float>(), extrin, sizeof(float)*16);

	/* This camera is fixed and could no more be changed. */
	_cIsFixed[id] = true;

	return true;
}

bool TextureBlenderImpl::HaveSetCameras()
{
	for (auto b : _cIsFixed) { if (!b) return false; }

	/* Initialize multithreading mesh queue */
	_pMeshQueue.reset(new MeshQueue(GPU_ID, 3, _nCams, 2.0 / 0.01));

	/* Initialize internal texture blender */
	_pTexBlender.reset(new TexBlender::TexBlender_RealTime);
	_stopTexturing = false;

	/* Create visualizer */
	const int VIS_WIDTH = 1920, VIS_HEIGHT = 1080;
	void *visCtx = CreateOpenGLContext("Ty Reconstruction", VIS_WIDTH, VIS_HEIGHT);

	SetArcball(VIS_WIDTH, VIS_HEIGHT);
	glfwSetMouseButtonCallback(static_cast<GLFWwindow*>(visCtx), MouseCallback);
	glfwSetCursorPosCallback(static_cast<GLFWwindow*>(visCtx), CursorCallback);
	glfwSetScrollCallback(static_cast<GLFWwindow*>(visCtx), ScrollCallback);

	/* Create blender context */
	void *bldCtx = CreateOpenGLContext("Blender", _Ws[0], _Hs[0]);
	glfwHideWindow(static_cast<GLFWwindow*>(bldCtx));

	/* Open texturing thread */
	_texureThread = std::thread(TexBlenderThreadProc, _pTexBlender,
		GPU_ID,
		std::ref(_stopTexturing),
		PACKING_LOOSE,
		_nCams,
		TEX_WIDTH,
		TEX_HEIGHT,
		_Ws,
		_Hs,
		_Ks,
		_Ms,
		_pMeshQueue,
		bldCtx,
		visCtx,
		_stream);
	_texureThread.detach();

	/* Wait for texture blender to finish initializing */
	std::unique_lock<std::mutex> initlocker(gLckInit);

	/* Wait for texturing blender to finish initializing */
	while (!_pTexBlender->m_init_done)
	{
		gCVIntTexturing.wait(initlocker, [&] {return _pTexBlender->m_init_done; });
	}

	/* Until now, texture blender is fully configured */
	_isConfigured = true;

	return true;
}

void TextureBlenderImpl::WaitFree()
{
	/* Mesh queue is busy */
	while (!_pMeshQueue->HasSpaceToProcess())
	{
		std::unique_lock<std::mutex> lock(gLckEndTexturing);
		gCVEndTexturing.wait(lock, [&] { return _pMeshQueue->HasSpaceToProcess(); });
	}
}

bool TextureBlenderImpl::UpdateImage(const size_t id, const uint8_t * const img, 
	const size_t szImg)
{
	CHECK_ID(id);

	/* Sanity check */
	if (!img || szImg != _Ws[id] * _Hs[id] * 3) { return false; }

	/* Copy images */
	if (_pMeshQueue->TexImagesToProcess()[id] == NULL)
		_pMeshQueue->AllocateTexImageToProcess(id, szImg);

	memcpy(_pMeshQueue->TexImagesToProcess()[id], img, szImg);

	return true;
}

bool TextureBlenderImpl::UpdateMesh(const float *v, const size_t szV, bool GPU)
{
	try {
		/* Copy vertices into mesh queue */
		_pMeshQueue->MeshToProcess()->Update(v, szV, GPU ? GPU_ID : -1);
		/* Move process pointer one step forward */
		_pMeshQueue->FinishProcess();
	}
	catch (std::exception &e) {
		std::cerr << e.what() << std::endl;
		/* TODO: thrust::sort_by_key randomly throws exception */
		//throw e;
	}
	return true;
}

void TextureBlenderImpl::Run()
{
	/* Notify texturing thread to start texturing */
	gCVBegTexturing.notify_one();
}

void TextureBlenderImpl::RegisterStream(std::weak_ptr<Stream> s)
{
	_stream = s;
}
