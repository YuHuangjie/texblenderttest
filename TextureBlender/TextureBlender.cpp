#include "TextureBlender.h"
#include "TextureBlenderImpl.h"

TextureBlender::TextureBlender()
	: _pImpl(nullptr)
{}

TextureBlender::TextureBlender(const size_t NCAM)
	: _pImpl(new TextureBlenderImpl(NCAM))
{}

TextureBlender::~TextureBlender() { _pImpl = nullptr; }

bool TextureBlender::IsConfigured() const { 
	if (_pImpl) return _pImpl->IsConfigured(); 
	return false;
}

size_t TextureBlender::GetTextureSize() const
{
	if (_pImpl) return _pImpl->GetTextureSize();
	return 0;
}

bool TextureBlender::SetCamera(const size_t id, const int32_t w, 
	const int32_t h, const float intrin[9], const float extrin[16])
{
	if (_pImpl) return _pImpl->SetCamera(id, w, h, intrin, extrin);
	return false;
}
bool TextureBlender::HaveSetCameras()
{
	if (_pImpl) return _pImpl->HaveSetCameras();
	return false;
}
void TextureBlender::WaitFree()
{
	if (_pImpl) return _pImpl->WaitFree();
}
bool TextureBlender::UpdateImage(const size_t id, const uint8_t * const img, const size_t szImg)
{
	if (_pImpl) return _pImpl->UpdateImage(id, img, szImg);
	return false;
}
bool TextureBlender::UpdateMesh(const float *v, const size_t szV, bool GPU)
{
	if (_pImpl) return _pImpl->UpdateMesh(v, szV, GPU);
	return false;
}

void TextureBlender::Run()
{
	if (_pImpl) return _pImpl->Run();
}

void TextureBlender::RegisterStream(std::weak_ptr<Stream> s)
{
	_pImpl->RegisterStream(s);
}
