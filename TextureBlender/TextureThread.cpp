#include <mutex>
#include <condition_variable>
#include <string>
#include <iostream>

#include "JpegCompressor.h"
#include "TextureThread.h"
#include "TexBlender/TexBlenderRT.h"
#include "Common/comm.h"
#include "MeshQueue.h"
#include "../Visualizer/Visualizer.h"
#include "../Visualizer/GLCallbacks.h"
#include "../Common/Stream.h"
#include "ReconstructionHeader.h"

#define SAVE_MESH_AND_TEXTURE 0
#define SAVE_FOLDER std::string("D:/Ty-Reconstruction/debug/")

using namespace std;

extern std::mutex gLckBegTexturing;

extern std::condition_variable gCVIntTexturing;
extern std::condition_variable gCVEndTexturing;
extern std::condition_variable gCVBegTexturing;

void StreamOutMesh(std::shared_ptr<Stream> stream, CuMesh::TriMesh* mesh);

static shared_ptr<JpegCompressor> compressor = nullptr;

void TexBlenderThreadProc(
	std::shared_ptr<TexBlender::TexBlender_RealTime> texBlender,
	int gpuId,
	bool &stopTexturing,
	float packingLoose,
	size_t nCams,
	int out_width,
	int out_height,
	std::vector<int> Ws,
	std::vector<int> Hs,
	std::vector<cv::Mat> Ks,
	std::vector<cv::Mat> Ms,
	std::shared_ptr<MeshQueue> meshQueue,
	void *ctx,
	void *visCtx,
	std::weak_ptr<Stream> stream)
{
	/* Initialize texture blender */
	CUDA_SAFE_CALL(cudaSetDevice(gpuId));

	texBlender->InitRender(ctx, OBJRender::RenderType::VISIBILITY_DEPTH_PASS,
		1, Ws[0], Hs[0], out_width, out_height);

	for (int i = 0; i < nCams; ++i) {
		texBlender->AppendViews(i,
			Ws[i],
			Hs[i],
			reinterpret_cast<float*>(Ks[i].data),
			reinterpret_cast<float*>(Ms[i].data),
			NULL);
	}

	/* Initialize visualier */
	Visualizer viser(visCtx);

	/* Initialize jpeg compressor */
	compressor.reset(new JpegCompressor(TEX_WIDTH, TEX_HEIGHT));

	/* Notify caller that texture blender is fully initialized */
	texBlender->m_init_done = true;
	gCVIntTexturing.notify_one();

	try {
		while (!stopTexturing)
		{
			// sync - wait if no mesh for texturing
			while (!meshQueue->HasMeshToTexture())
			{
				std::unique_lock<std::mutex> lock(gLckBegTexturing);
				gCVBegTexturing.wait(lock, [&] { return meshQueue->HasMeshToTexture(); });

				if (stopTexturing) {
					return;
				}
			}

			/* Do texturing */
			int nf = meshQueue->frame_texture;

			if (USE_TEXTURE && meshQueue->MeshToTexture()->FN >= 100 && meshQueue->MeshToTexture()->FN <= 1000000)
			{
				texBlender->SetMeshAndTexRGB(meshQueue->MeshToTexture(), meshQueue->TexImagesToTexture());
				texBlender->BuildTexture(packingLoose);
			}

	#if SAVE_MESH_AND_TEXTURE
			// save mesh
			meshQueue->MeshToTexture()->SaveToPly(
				SAVE_FOLDER + "mesh" +	std::to_string(nf) + ".ply", nf, false);

			// save texture
			meshQueue->MeshToTexture()->SaveTexture(SAVE_FOLDER + "uv" + std::to_string(nf) + ".png");

			// save reference images
			for (size_t i = 0; i < nCams; ++i) {
				cv::Mat image(Hs[i], Ws[i], CV_8UC3, meshQueue->TexImagesToTexture()[i]);
				cv::imwrite(SAVE_FOLDER + "rgb" + std::to_string(nf) + "_" + std::to_string(i) + ".jpg", image);
		}
	#endif

			/* Update visulizer */
			viser.UpdateMesh(static_cast<float*>(meshQueue->MeshToTexture()->Vert(CuMesh::GPU)),
				meshQueue->MeshToTexture()->FN * MULT_VERT * sizeof(TYPE_VERT),
				static_cast<float*>(meshQueue->MeshToTexture()->TexCoord(CuMesh::GPU)),
				meshQueue->MeshToTexture()->FN * MULT_TEXC * sizeof(TYPE_TEXC));
			viser.UpdateTexture(static_cast<uint8_t*>(meshQueue->MeshToTexture()->Texture_Raw(CuMesh::GPU)),
				TEX_WIDTH, TEX_HEIGHT);

			float M[9], T[3] = { -0.728051, -0.955092, 1.628250 };
			GetArcRotation(M);
			viser.SetModelMatrix(M, T);
			viser.SetScale(GetScale());
			viser.Render();

			glfwSwapBuffers(static_cast<GLFWwindow*>(visCtx));

			/* Stream out mesh and texture */
			auto s = stream.lock();

			if (s && s->CanWrite()) {
				meshQueue->MeshToTexture()->CodingBeforeTransfer();
				meshQueue->MeshToTexture()->SyncTexture();
				StreamOutMesh(s, meshQueue->MeshToTexture());
				s->OnCompleted();
			}

			// sync - texturing finish
			{
				meshQueue->FinishTexture();
				gCVEndTexturing.notify_one();
			}
		}
	}
	catch (std::exception &e) {
		std::cerr << "[ERROR] TextureThread: " << e.what() << std::endl;
	}

	compressor = nullptr;
	return;
}

void StreamOutMesh(std::shared_ptr<Stream> stream, CuMesh::TriMesh* mesh)
{
	ReconstructDataHeader ohead;

	/* Write voxel parameters */
	ohead.type = ReconstructDataType::VOXEL_PARAM;
	ohead.length = 4 * sizeof(float);
	stream->Write(&ohead, sizeof(ReconstructDataHeader));
	stream->Write(&mesh->m_voxel_param, ohead.length);

	/* Write face indices */
	if (REMOVE_DUPLICATE_VERTEX)
	{
		ohead.type = ReconstructDataType::INDEX_INT;
		ohead.length = mesh->FN_coded * sizeof(TYPE_CODED_INDX) * MULT_CODED_INDX;
		stream->Write(&ohead, sizeof(ReconstructDataHeader));

		if (ohead.length > 0)
			stream->Write(mesh->Coded_Index(CuMesh::Device::CPU), ohead.length);
	}

	/* Write vertices */
	ohead.type = ReconstructDataType::VERTEX_INT_CODED;
	ohead.length = mesh->VN_coded * sizeof(TYPE_CODED_VERT) * MULT_CODED_VERT_PERVN;
	stream->Write(&ohead, sizeof(ReconstructDataHeader));

	if (ohead.length > 0)
		stream->Write(mesh->Coded_Vert(CuMesh::Device::CPU), ohead.length);

	if (USE_TEXTURE)
	{
		/* Write texture coordinates */
		ohead.type = ReconstructDataType::TEXCOORD_SHORT_CODED;
		ohead.length = mesh->VN_coded * sizeof(TYPE_CODED_TEXC) * MULT_CODED_TEXC_PERVN;
		stream->Write(&ohead, sizeof(ReconstructDataHeader));

		if (ohead.length > 0)
			stream->Write(mesh->Coded_TexCoord(CuMesh::Device::CPU), ohead.length);

		/* Write jpeg texture */
		uint8_t *p = nullptr;
		size_t sz = 0;
		uint8_t *src = (uint8_t*)mesh->Texture_Raw(CuMesh::Device::CPU);
		compressor->Compress(src, &p, &sz);
		cout << "size " << sz << endl;

		ohead.type = ReconstructDataType::TEXTURE_JPG;
		ohead.length = sz + TEXTURE_HEADER;
		stream->Write(&ohead, sizeof(ReconstructDataHeader));

		int header[3];
		header[0] = TEX_WIDTH;
		header[1] = TEX_HEIGHT;
		header[2] = 3;

		if (ohead.length > 0)
		{
			stream->Write(&header[0], TEXTURE_HEADER);
			stream->Write(p, sz);
		}
	}
}
