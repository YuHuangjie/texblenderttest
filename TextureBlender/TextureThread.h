#ifndef TEXTURE_THREAD_H
#define TEXTURE_THREAD_H

#include <memory>
#include <vector>
#include <opencv2/Core.hpp>

namespace TexBlender {
	class TexBlender_RealTime;
}
class MeshQueue;
class Stream;

void TexBlenderThreadProc(
	std::shared_ptr<TexBlender::TexBlender_RealTime> texBlender,
	int gpuId,
	bool &stopTexturing,
	float packingLoose,
	size_t nCams,
	int out_width,
	int out_height,
	std::vector<int> Ws,
	std::vector<int> Hs,
	std::vector<cv::Mat> Ks,
	std::vector<cv::Mat> Ms,
	std::shared_ptr<MeshQueue> meshQueue,
	void *bldCtx,
	void *visCtx,
	std::weak_ptr<Stream> stream);

#endif /* TEXTURE_THREAD_H */
