#ifndef TEXTURE_BLENDER_IMPL_H
#define TEXTURE_BLENDER_IMPL_H

#include <memory>
#include <thread>
#include <vector>
#include <opencv2/Core.hpp>
#include "../Common/Stream.h"

using namespace std;

namespace TexBlender {
	class TexBlender_RealTime;
}
class MeshQueue;

class TextureBlenderImpl
{
public:
	explicit TextureBlenderImpl(const size_t NCAM);
	~TextureBlenderImpl();

	bool IsConfigured() const { return _isConfigured; }
	size_t GetTextureSize() const;

	bool SetCamera(const size_t id, const int32_t w, const int32_t h,
		const float intrin[9], const float extrin[16]);
	bool HaveSetCameras();

	void WaitFree();
	bool UpdateImage(const size_t id, const uint8_t *const img,
		const size_t szImg);
	bool UpdateMesh(const float *v, const size_t szV, bool GPU);

	void Run();

	void RegisterStream(weak_ptr<Stream>);

private:
	static const int GPU_ID = 0;
	static const float PACKING_LOOSE;

private:
	bool _isConfigured;			/* ready to do texturing */

	bool _stopTexturing;		/* flag controlling texturing thread */
	std::thread _texureThread;	/* texturing thread */

	/* Multithreading queue */
	shared_ptr<MeshQueue> _pMeshQueue;
	/* texture blender */
	shared_ptr<TexBlender::TexBlender_RealTime> _pTexBlender;

	const size_t _nCams;					/* camera id */
	vector<bool> _cIsFixed;					/* fixed camera allow no changes */
	vector<cv::Mat> _Ks;					/* 3x3 intrinsic */
	vector<cv::Mat> _Ms;					/* 4x4 extrinsic */
	vector<int> _Ws;						/* image width */
	vector<int> _Hs;						/* image height */
	
	weak_ptr<Stream> _stream;				/* stream out mesh/textures */
};

#endif /* TEXTURE_BLENDER_IMPL_H */
