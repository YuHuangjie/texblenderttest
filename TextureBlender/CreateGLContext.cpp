#include <GL/glew.h>
#include <GLFW/glfw3.h>

void * CreateOpenGLContext(const char *title, const int width, const int height)
{
	/* Create OpenGL context for blender thread */
	if (glfwInit() != GLFW_TRUE) {
		return nullptr;
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);	// Want OpenGL 4
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	GLFWwindow *ctx = glfwCreateWindow(width, height, title, NULL, NULL);

	return ctx;
}