#ifndef JPEG_COMPRESSOR_H
#define JPEG_COMPRESSOR_H

#include <vector>
#include <turbojpeg.h>

class JpegCompressor
{
public:
	JpegCompressor(int width, int height);
	~JpegCompressor();

	void Compress(const uint8_t *src, uint8_t **jpegBuf, size_t *jpegSz);

private:
	int _width;
	int _height;
	tjhandle _compressor;
	std::vector<uint8_t> _buf;
};

#endif /* JPEG_COMPRESSOR_H */
