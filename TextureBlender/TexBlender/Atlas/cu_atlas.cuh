#include "TexAtlas.h"

__device__	int		g_vid;
__device__	float	g_bins_area;

__device__  Bin2D	g_dev_bins[MAX_BINS];
__device__	int		g_dev_bincount;