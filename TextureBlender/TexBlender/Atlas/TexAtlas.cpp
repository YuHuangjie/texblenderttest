#include "TexAtlas.h"

using namespace TexBlender;

void CompareProjectedArea(const int face_num, const int vid, const float* p_area, int* bsv, float* dst, float* in_uv, float* out_uv);
void GetFaceLabel(const int vid, const int img_W, const int img_H, const int face_num, const int* sel_view, const float* puv, const int* label_image, int* outlabel);

//void ExtraceBins(const int vid, const int img_W, const int img_H, const int face_num, const int* sel_view, const float* puv, const int* label_image, int* outlabel);

void TreeBasedBinPacking();
void GetUVCoordinate(int nface, const int* vid, const int* label, const float* puv, float* out_uv);

void FillNoViewFaceFromFaceFaceTopology(int nface, int* dev_selview, int* faceface_topology, int topy_list_len);
void CleanSelViewFromFaceFaceTopology(int nface, int* dev_selview, int* faceface_topology, int topy_list_len);
void FixNoViewFaceUV(int nface, int vid, const int* selview, const float* proj_uv, float* orgin_uv);

TexAtlas::TexAtlas()
{
	m_face_capacity = 0;
	m_face_useful = 0;

	m_cpu_tex_size = 0;
	m_cpu_selectview_tex = NULL;

	//m_dev_tex_size = 0;

	m_dev_viewid = NULL;
	m_dev_maxarea = NULL;

	m_dev_labelid = NULL;

	m_dev_orgin_uv = NULL;
	//m_dev_packed_uv = NULL;

	//m_dev_selectview_tex = NULL;
}

TexAtlas::~TexAtlas()
{
	if (m_dev_viewid != NULL)
		CUDA_SAFE_CALL(cudaFree(m_dev_viewid));

	if (m_dev_maxarea != NULL)
		CUDA_SAFE_CALL(cudaFree(m_dev_maxarea));

	if (m_dev_orgin_uv != NULL)
		CUDA_SAFE_CALL(cudaFree(m_dev_orgin_uv));

	//if (m_dev_packed_uv != NULL)
	//	CUDA_SAFE_CALL(cudaFree(m_dev_packed_uv));

	//if (m_dev_selectview_tex != NULL)
	//	CUDA_SAFE_CALL(cudaFree(m_dev_selectview_tex));
}

void TexAtlas::UpdateBufferSize(int face_size, int face_capacity)
{
	if (m_face_capacity < face_capacity)
	{
		m_face_capacity = face_capacity;

		if (m_dev_maxarea != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_maxarea));

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_maxarea, m_face_capacity * sizeof(float))
		);

		if (m_dev_viewid != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_viewid));

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_viewid, m_face_capacity * 3 * sizeof(int))
		);

		if (m_dev_orgin_uv != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_orgin_uv));

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_orgin_uv, m_face_capacity * MULT_TEXC * sizeof(TYPE_TEXC))
		);

		//if (m_dev_packed_uv != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_packed_uv));

		//CUDA_SAFE_CALL(
		//	cudaMalloc(&m_dev_packed_uv, m_face_capacity * MULT_TEXC * sizeof(TYPE_TEXC))
		//);

		if (m_dev_labelid != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_labelid));

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_labelid, m_face_capacity * sizeof(TYPE_VISB))
		);
	}

	{
		CUDA_SAFE_CALL(
			cudaMemset(m_dev_maxarea, 0, m_face_capacity * sizeof(float))
		);

		CUDA_SAFE_CALL(
			cudaMemset(m_dev_viewid, -1, m_face_capacity * 3 * sizeof(int))
		);

		CUDA_SAFE_CALL(
			cudaMemset(m_dev_labelid, -2, m_face_capacity * sizeof(int))
		);

		CUDA_SAFE_CALL(
			cudaMemset(m_dev_orgin_uv, 0, m_face_capacity * MULT_TEXC * sizeof(TYPE_TEXC))
		);
	}

	m_face_useful = face_size;
}


void TexAtlas::Compare(int vid, void* proj_area, void* proj_uv)
{
	CompareProjectedArea(m_face_useful, vid, (float*)proj_area, (int*)m_dev_viewid, (float*)m_dev_maxarea, (float*)proj_uv, (float*)m_dev_orgin_uv);
}

void TexAtlas::FillNoViewFace(int* faceface_topology, int topy_list_len)
{
	FillNoViewFaceFromFaceFaceTopology(m_face_useful, (int*)m_dev_viewid, (int*)faceface_topology, topy_list_len);

	CleanSelViewFromFaceFaceTopology(m_face_useful, (int*)m_dev_viewid, (int*)faceface_topology, topy_list_len);
}

void TexAtlas::FillNoViewFaceUV(int vid, void* proj_uv)
{
	FixNoViewFaceUV(m_face_useful, vid, (int*)m_dev_viewid, (float*)proj_uv, (float*)m_dev_orgin_uv);
}

void TexAtlas::QuerryLabel(int vid, int w, int h, int* vimg)
{
	GetFaceLabel( vid, w, h, m_face_useful, (int*)m_dev_viewid, (float*)m_dev_orgin_uv, (int*)m_devptr_cclres, (int*)m_dev_labelid );
}

//void ShowPacking(PackingTreeNode * node, std::stringstream& debug_outputstream)
//{
//	if (node != NULL)
//	{
//		if (node->filled)
//		{
//			//std::cout << (float)node->binRectangle->tex_x << "; " << (float)node->binRectangle->tex_y << ";"
//			//	<< (float)node->binRectangle->tex_x + (float)node->binRectangle->W << "; " << (float)node->binRectangle->tex_y << ";"
//			//	<< (float)node->binRectangle->tex_x << "; " << (float)node->binRectangle->tex_y + (float)node->binRectangle->H << ";"
//			//	<< (float)node->binRectangle->tex_x + (float)node->binRectangle->W << ";"  <<  (float)node->binRectangle->tex_y + (float)node->binRectangle->H << std::endl;
//
//			debug_outputstream << (float)node->binRectangle->tex_x << "," << (float)node->binRectangle->tex_y << ","
//				<< (float)node->binRectangle->tex_x + (float)node->binRectangle->W << ", " << (float)node->binRectangle->tex_y << ","
//				<< (float)node->binRectangle->tex_x + (float)node->binRectangle->W << "," << (float)node->binRectangle->tex_y + (float)node->binRectangle->H << ","
//				<< (float)node->binRectangle->tex_x << "," << (float)node->binRectangle->tex_y + (float)node->binRectangle->H << ";" << std::endl;
//		}
//
//		ShowPacking(node->leftChild, debug_outputstream);
//		ShowPacking(node->rightChild, debug_outputstream);
//	}
//}

void TexAtlas::AtlasPrepare()
{
	m_cudaCCL_uf.Clear();
}
/*
void TexAtlas::Bin2DFromCuda()
{
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	ExtractUVCoordinate();
	m_cudaCCL_uf.GetBinsData(m_bins2D, m_bins_useful, m_bins_area);

	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;

	std::cout << "Packing Time:\t" << elapsedTime << " msecs" << std::endl;

	std::stringstream debug_outputstream;
	std::cout << "Packing Results:" << std::endl;
	debug_outputstream << "Bins = [";

	for (int i = 0; i < m_bins_useful; ++i) {
		debug_outputstream << (float)m_bins2D[i].tex_x << "," << (float)m_bins2D[i].tex_y << ","
			<< (float)m_bins2D[i].tex_x + (float)m_bins2D[i].W << ", " << (float)m_bins2D[i].tex_y << ","
			<< (float)m_bins2D[i].tex_x + (float)m_bins2D[i].W << "," << (float)m_bins2D[i].tex_y + (float)m_bins2D[i].H << ","
			<< (float)m_bins2D[i].tex_x << "," << (float)m_bins2D[i].tex_y + (float)m_bins2D[i].H << ","
			<< (float)m_bins2D[i].vid << "," << (float)m_bins2D[i].label << ";" << std::endl;
	}

	debug_outputstream << "];";
	std::string debug_outputfile = "debug_packing.m";
	std::ofstream ofstr(debug_outputfile.c_str(), std::ios_base::trunc | std::ios_base::out);
	
	ofstr.write(debug_outputstream.str().c_str(), debug_outputstream.str().length());
	ofstr.close();

	//float edge_len = sqrt(m_bins_area);

	//Bin2D bin_tex;

	//bin_tex.vid = -1;
	//bin_tex.label = -1;
	//bin_tex.W = edge_len * 1.01;
	//bin_tex.H = edge_len * 1.01;
	//bin_tex.tex_x = 0;
	//bin_tex.tex_y = 0;

	//LARGE_INTEGER frequency;        // ticks per second
	//LARGE_INTEGER t1, t2;           // ticks
	//double elapsedTime;

	//QueryPerformanceFrequency(&frequency);
	//QueryPerformanceCounter(&t1);

	//m_packing_root = new PackingTreeNode(&bin_tex);

	//for (int i = 0; i < m_bins_useful; ++i)
	//{
	//	//std::cout << "Packing :" << i <<"," << m_bins2D[i].W << "," << m_bins2D[i].H << std::endl;
	//	m_packing_root->insert(&m_bins2D[i]);
	//}

	//QueryPerformanceCounter(&t2);
	//elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;

	//std::cout << "Packing Time:\t" << elapsedTime << " msecs" << std::endl;

	//std::cout << "Edge Len:" << edge_len << std::endl;

	//std::stringstream debug_outputstream;
	//std::cout << "Packing Results:" << std::endl;
	//debug_outputstream << "Bins = [";
	//ShowPacking(m_packing_root, debug_outputstream);
	//debug_outputstream << "];";
	//std::string debug_outputfile = "debug_packing.m";
	//std::ofstream ofstr(debug_outputfile.c_str(), std::ios_base::trunc | std::ios_base::out);
	//
	//ofstr.write(debug_outputstream.str().c_str(), debug_outputstream.str().length());
	//ofstr.close();
}
*/

void TexAtlas::ExtractConnectedComponents(int vid, int w, int h)
{
	//if (m_dev_tex_size != m_cpu_tex_size)
	//{
	//	m_dev_tex_size  = m_cpu_tex_size;
	//	if (m_dev_selectview_tex != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_selectview_tex));

	//	CUDA_SAFE_CALL(cudaMalloc(&m_dev_selectview_tex, m_dev_tex_size));
	//}

	//CUDA_SAFE_CALL(cudaMemcpy(m_dev_selectview_tex, m_cpu_selectview_tex, m_dev_tex_size, cudaMemcpyHostToDevice));

	//m_ccl_cuda.Label((int*)m_dev_selectview_tex, w, h, 8, 0, &m_devptr_cclres);
	//m_ccl_cpu.Label((int*)m_cpu_selectview_tex, w, h, 8, 0);

	//m_cudaCCL_tex.Label(m_dev_tex_array, w, h, &m_devptr_cclres);
	
	m_cudaCCL_uf.Label(m_dev_tex_array, vid, w, h, &m_devptr_cclres);

	QuerryLabel(vid, w, h, (int*)m_devptr_cclres);

	//m_cudaCCL_uf.GetBinsData(m_bins2D, m_bins_useful);
}


void TexAtlas::ExtractUVCoordinate(void* mesh_uv)
{

	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;

	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);

	TreeBasedBinPacking();

	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;

	std::cout << "TreeBasedBinPacking Time:\t" << elapsedTime << " msecs" << std::endl;

	QueryPerformanceCounter(&t1);

	GetUVCoordinate(m_face_useful, (int*)m_dev_viewid, (int*)m_dev_labelid, (float*)m_dev_orgin_uv, (float*)mesh_uv);
	
	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;

	std::cout << "GetUVCoordinate Time:\t" << elapsedTime << " msecs" << std::endl;

	m_cudaCCL_uf.GetBinsData(m_bins2D, m_bins_useful, m_bins_area);

	std::stringstream debug_outputstream;
	debug_outputstream << "Bins = [";

	for (int i = 0; i < m_bins_useful; ++i) {
		debug_outputstream << (float)m_bins2D[i].tex_x << "," << (float)m_bins2D[i].tex_y << ","
			<< (float)m_bins2D[i].tex_x + (float)m_bins2D[i].W << ", " << (float)m_bins2D[i].tex_y << ","
			<< (float)m_bins2D[i].tex_x + (float)m_bins2D[i].W << "," << (float)m_bins2D[i].tex_y + (float)m_bins2D[i].H << ","
			<< (float)m_bins2D[i].tex_x << "," << (float)m_bins2D[i].tex_y + (float)m_bins2D[i].H << ","
			<< (float)m_bins2D[i].vid << "," << (float)m_bins2D[i].label << ";" << std::endl;
	}

	debug_outputstream << "];";
	std::string debug_outputfile = "debug_packing.m";
	std::ofstream ofstr(debug_outputfile.c_str(), std::ios_base::trunc | std::ios_base::out);

	ofstr.write(debug_outputstream.str().c_str(), debug_outputstream.str().length());
	ofstr.close();
}

void TexAtlas::ExtractUVCoordinateCPU(void* mesh_uv, double loose_factor)
{
	m_cudaCCL_uf.GetBinsData(m_bins2D, m_bins_useful, m_bins_area);

	int texlen = int(sqrt(m_bins_area) * loose_factor + 1.0);
	m_packing_fast.PackingRecursive(m_bins_useful, texlen, texlen, m_bins2D);

	m_cudaCCL_uf.SetBinsData(m_bins2D, m_bins_useful, (float)texlen);

	GetUVCoordinate(m_face_useful, (int*)m_dev_viewid, (int*)m_dev_labelid, (float*)m_dev_orgin_uv, (float*)mesh_uv);

	//m_cudaCCL_uf.GetBinsData(m_bins2D, m_bins_useful, m_bins_area);
}