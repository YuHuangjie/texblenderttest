#pragma once

#include "Common/comm.h"

#include "Mesh/Mesh.h"
#include "FastPackingCPU.h"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

namespace CCL_CUDA {
	class CCL_CUDA {
	private:
		int  m_Width;
		int  m_Height;

		int* Ld;
		int* Rd;

	public:

		void Label(int* image, int W, int H, int degree_of_connectivity, int threshold, void** label);

		CCL_CUDA();
		~CCL_CUDA();
	};
}

namespace CCL_CPU {
	class CCL_CPU {

	public:

		int W;
		int H;

		int* L;
		int* R;

		void Label(int* image, int W, int H, int degree_of_connectivity, int threshold);

		CCL_CPU();
		~CCL_CPU();
	};

}

namespace CCL_CUDA_TEX {
	class CCL_CUDA_TEX {

	public:

		int W;
		int H;

		int* L;
		int* R;

		void Label(cudaArray* ImgArry, int w_, int h_, void** label);

		CCL_CUDA_TEX();
		~CCL_CUDA_TEX();
	};
}

namespace UnionFind {
	class CCL_UF {

	public:

		int W;
		int H;

		int* L;
		int* L_Clean;

		int* Minx;
		int* Miny;
		int* Maxx;
		int* Maxy;

		void Clear();
		void Label(cudaArray* ImgArry, int vid, int w_, int h_, void** label);
		void GetBinsData(Bin2D* bins, int& n, float& bin_area);
		void SetBinsData(Bin2D* bins, int n, float bin_len);

		 CCL_UF();
		~CCL_UF();
	};
}

namespace TexBlender {
	
	class TexAtlas {
	
	public:

		int m_Width;
		int m_Height;

		UnionFind::CCL_UF  m_cudaCCL_uf;
		FastPackingThreadRecursiveCPU   m_packing_fast;

		cudaArray *m_dev_tex_array;

		int m_face_capacity;
		int m_face_useful;

		//int   m_dev_tex_size;
		//void* m_dev_selectview_tex;

		int   m_cpu_tex_size;
		void* m_cpu_selectview_tex;

		void* m_devptr_cclres;

		void* m_dev_maxarea;
		void* m_dev_viewid;
		void* m_dev_labelid;

		void* m_dev_orgin_uv;
		//void* m_dev_packed_uv;

		// packing 
		
		int   m_bins_useful;
		float m_bins_area;
		Bin2D m_bins2D[MAX_BINS];

		void AtlasPrepare();
		void UpdateBufferSize(int face_size, int face_capacity);
		void Compare(int vid, void* proj_area, void* proj_uv);
		void FillNoViewFace(int* faceface_topology, int topy_list_len);
		void FillNoViewFaceUV(int vid, void* proj_uv);

		void ExtractConnectedComponents(int vid, int w, int h);
		void ExtractUVCoordinate(void* mesh_uv);
		void ExtractUVCoordinateCPU(void* mesh_uv, double loose_factor = 1.35);

		void QuerryLabel(int vid, int w, int h, int* vimg);
		//void Bin2DFromCuda();

		 TexAtlas();
		~TexAtlas();
	};


	//bool TexAtlas(OBJRender& render, TexBlender::TriMesh& mesh, TexBlender::Views_Cuda& views);

}