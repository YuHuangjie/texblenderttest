#include <stdio.h>  
#include <stdlib.h>
#include <assert.h>

#include "TexAtlas.h"

texture<int, 2, cudaReadModeElementType> imgtex;
texture<int, 1, cudaReadModeElementType> Ltex;
texture<int, 1, cudaReadModeElementType> Rtex;

namespace CCL_CUDA_TEX {

#define LEQUIV_BLOCK_SIZE_X 16
#define LEQUIV_BLOCK_SIZE_Y 16

	__global__ void LEQUIV_prescan(int* L, int* R, int w, int h) {
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;
		int index = x + y*w;

		if (x < w && y < h) {
			L[index] = index;
			R[index] = index;
		}
	}

	__global__ void LEQUIV_scan(int* R, int w, int h, int* d_stop) {
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;
		int index = x + y*w;

		if (x < w && y < h) {
			int v = tex2D(imgtex, x, y);
			int label = tex1Dfetch(Ltex, index);
			int newlabel = w*h;

			if (y > 0 && tex2D(imgtex, x, y - 1) == v) {
				newlabel = min(newlabel, tex1Dfetch(Ltex, index - w));
			}
			if (y < h - 1 && tex2D(imgtex, x, y + 1) == v) {
				newlabel = min(newlabel, tex1Dfetch(Ltex, index + w));
			}
			if (x > 0 && tex2D(imgtex, x - 1, y) == v) {
				newlabel = min(newlabel, tex1Dfetch(Ltex, index - 1));
			}
			if (x < w - 1 && tex2D(imgtex, x + 1, y) == v) {
				newlabel = min(newlabel, tex1Dfetch(Ltex, index + 1));
			}

			if (newlabel < label) {
				R[label] = newlabel;
				*d_stop = 0;
			}
		}
	}

	__global__ void LEQUIV_analysis(int* L, int* R, int w, int h) {
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;
		int index = x + y*w;
		int label;

		if (x < w && y < h) {
			label = L[index];
			if (label == index) {
				int deep = 128;
				int rf = label;
				//label = tex1Dfetch(Rtex, rf);
				label = R[rf];
				while (rf != label && deep > 0) {
					rf = label;
					label = tex1Dfetch(Rtex, rf);
					deep--;
				}
				//texture will be invalid
				R[index] = label;
			}
		}
	}

	__global__ void LEQUIV_labeling(int* L, int w, int h) {
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;
		int index = x + y*w;
		if (x < w && y < h) {
			int label = L[index];
			int cc = tex1Dfetch(Rtex, label);
			L[index] = tex1Dfetch(Rtex, cc);
		}
	}

	CCL_CUDA_TEX::CCL_CUDA_TEX()
	{
		W = 0;
		H = 0;

		L = NULL;
		R = NULL;
	}

	CCL_CUDA_TEX::~CCL_CUDA_TEX()
	{
		if (L != NULL)
			CUDA_SAFE_CALL(cudaFree(L));

		if (R != NULL)
			CUDA_SAFE_CALL(cudaFree(R));
	}

	void CCL_CUDA_TEX::Label(cudaArray* ImgArry, int w_, int h_, void** label) {

		cudaError_t err;

		int  N = W * H;

		if (w_ != W || h_ != H)
		{
			W = w_;
			H = h_;

			N = W * H;

			if (L != NULL) CUDA_SAFE_CALL(cudaFree(L));
			if (R != NULL) CUDA_SAFE_CALL(cudaFree(R));

			CUDA_SAFE_CALL(cudaMalloc((void**)&L, sizeof(int) * N));
			CUDA_SAFE_CALL(cudaMalloc((void**)&R, sizeof(int) * N));
		}

		cudaChannelFormatDesc intdesc = cudaCreateChannelDesc<int>();
		cudaBindTextureToArray(imgtex, ImgArry, intdesc);

		cudaBindTexture(NULL, Ltex, L, intdesc, N);
		cudaBindTexture(NULL, Rtex, R, intdesc, N);

		int stop;
		int* d_stop;
		cudaMalloc((void**)&d_stop, sizeof(int));

		dim3 block(LEQUIV_BLOCK_SIZE_X, LEQUIV_BLOCK_SIZE_Y);
		dim3 grid((W + LEQUIV_BLOCK_SIZE_X - 1) / LEQUIV_BLOCK_SIZE_X, (H + LEQUIV_BLOCK_SIZE_Y - 1) / LEQUIV_BLOCK_SIZE_Y);

		LEQUIV_prescan << <grid, block >> > (L, R, W, H);

		stop = 0;
		while (stop == 0)
		{
			cudaMemset(d_stop, 0xFF, sizeof(int));

			LEQUIV_scan     << <grid, block >> > (R, W, H, d_stop);

			LEQUIV_analysis << <grid, block >> > (L, R, W, H);

			LEQUIV_labeling << <grid, block >> > (L, W, H);

			cudaMemcpy(&stop, d_stop, sizeof(int), cudaMemcpyDeviceToHost);
		}

		*label = L;
	}

}
