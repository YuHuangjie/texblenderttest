#include "TexAtlas.h"

using namespace TexBlender;

__device__
int MostCommonElement(int* arr, int num)
{
	int elem = -1;
	int cur_count = 0;
	int max_count = 0;

	int i, j;

	for (i = 0; i < num; i++)
	{
		cur_count = 0;

		for (j = 0; j < num;j++)
		{
			if (arr[j] == arr[i])
				++cur_count;
		}

		if(cur_count >= max_count)
		{
			max_count = cur_count;
			elem = arr[i];
		}
	}

	return elem;
}


__global__
void CompareArea(const int face_num, const int vid, const float* p_area, int* bsv, float* dst, float* in_uv, float* out_uv)
{
	const int face_id = blockIdx.x * blockDim.x + threadIdx.x;

	if (face_id >= face_num)
		return;

	if (p_area[face_id] < 0.0)
	{
		return;
	}

	if (p_area[face_id] > dst[face_id])
	{
		dst[face_id] = p_area[face_id];

		bsv[face_id * 3] = vid;
		bsv[face_id * 3 + 1] = vid;
		bsv[face_id * 3 + 2] = vid;

		out_uv[face_id * 3 * 2 + 0] = in_uv[face_id * 3 * 2 + 0];
		out_uv[face_id * 3 * 2 + 1] = in_uv[face_id * 3 * 2 + 1];

		out_uv[face_id * 3 * 2 + 2] = in_uv[face_id * 3 * 2 + 2];
		out_uv[face_id * 3 * 2 + 3] = in_uv[face_id * 3 * 2 + 3];

		out_uv[face_id * 3 * 2 + 4] = in_uv[face_id * 3 * 2 + 4];
		out_uv[face_id * 3 * 2 + 5] = in_uv[face_id * 3 * 2 + 5];
	}
}

void CompareProjectedArea(const int face_num, const int vid, const float* p_area, int* bsv, float* dst, float* in_uv, float* out_uv)
{
	dim3 block(256, 1);
	dim3 grid((face_num + block.x - 1) / block.x, 1);

	CompareArea << <grid, block >> > (face_num, vid, p_area, bsv, dst, in_uv, out_uv);

	CUDA_CHECK_ERROR();
}


__global__
void FaceLabelKernel(const int face_num, const int vid, const int W, const int H, const int* sel_view, const float* puv, const int* label_image, int* outlabel)
{
	const int face_id = blockIdx.x * blockDim.x + threadIdx.x;

	if (face_id >= face_num)
		return;

	if (sel_view[face_id * 3] != vid)
	{
		return;
	}

	// do bilateral interpolation in future

	float x1 = puv[face_id * MULT_TEXC + 0];
	float y1 = puv[face_id * MULT_TEXC + 1];

	float x2 = puv[face_id * MULT_TEXC + 2];
	float y2 = puv[face_id * MULT_TEXC + 3];

	float x3 = puv[face_id * MULT_TEXC + 4];
	float y3 = puv[face_id * MULT_TEXC + 5];

	int x = int((x1 + x2 + x3) / 3.0);
	int y = int((y1 + y2 + y3) / 3.0);

	if (x < 0 || x >= W || y < 0 || y >= H)
	{
		outlabel[face_id] = -1;
		return;
	}

	int lb = label_image[y * W + x];

	if (lb > 0)
	{
		outlabel[face_id] = lb;
		return;
	}
	else
	{
		for (int i = -1; i <= 1; ++i) {
			for (int j = -1; j <= 1; ++j) {

				if (x + i >= 0 && x + i < W && y + j >= 0 && y + j < H)
				{
					int lb = label_image[(y + j) * W + x + i];

					if (lb > 0)
					{
						outlabel[face_id] = lb;
						return;
					}
				}

			}
		}
	}

	outlabel[face_id] = -1;
}

__global__
void FillSelViewFaceFaceTopologyKernel(int nface, int* selview, int* faceface_topology, int topy_list_len)
{
	unsigned int face_id = threadIdx.x + blockIdx.x * blockDim.x;

	if (face_id >= nface)
		return;

	if (selview[face_id * 3] > 0)
	{
		return;
	}

	int iter = 0;

	while (iter < 100)
	{
		for (int i = 0; i < topy_list_len; ++i)
		{
			int nbfid = faceface_topology[face_id * topy_list_len + i];

			if (nbfid < 0 || nbfid >= nface)
				break;

			if (selview[nbfid * 3] > 0)
			{
				selview[face_id * 3] = selview[nbfid * 3];
				selview[face_id * 3 + 1] = selview[nbfid * 3];
				selview[face_id * 3 + 2] = selview[nbfid * 3];
				break;
			}
		}

		if (selview[face_id * 3] > 0)
			break;

		iter++;
	}

}

__global__
void CleanSelViewUVKernel(int nface, int* selview, int* faceface_topology, int topy_list_len)
{
	unsigned int face_id = threadIdx.x + blockIdx.x * blockDim.x;

	if (face_id >= nface)
		return;

	int iter = 0;

	while (iter < 3)
	{
		int nbcount = 0;
		int nbselviewlist[TOPOLOGY_LIST_LEN];

		for (int i = 0; i < topy_list_len; ++i)
		{
			int nbfid = faceface_topology[face_id * topy_list_len + i];

			if (nbfid < 0 || nbfid >= nface)
				break;

			if (selview[nbfid * 3] >= 0)
				nbselviewlist[nbcount++] = selview[nbfid * 3];
		}

		int most_com_nbview = MostCommonElement(nbselviewlist, nbcount);

		if(most_com_nbview>=0)
		{
			selview[face_id * 3] = most_com_nbview;
			selview[face_id * 3 + 1] = most_com_nbview;
			selview[face_id * 3 + 2] = most_com_nbview;
		}

		iter++;

		__syncthreads(); // not strictly needed 
	}
}

__global__
void FixNoViewFaceUVKernel(int nface, int vid, const int* selview, const float* proj_uv, float* orgin_uv)
{
	const int face_id = blockIdx.x * blockDim.x + threadIdx.x;

	if (face_id >= nface)
		return;

	if (orgin_uv[face_id * 3 * 2 + 0] > 0.0 || 
		orgin_uv[face_id * 3 * 2 + 1] > 0.0 ||
		orgin_uv[face_id * 3 * 2 + 2] > 0.0 ||
		orgin_uv[face_id * 3 * 2 + 3] > 0.0 ||
		orgin_uv[face_id * 3 * 2 + 4] > 0.0 ||
		orgin_uv[face_id * 3 * 2 + 5] > 0.0 )
	{
		//return;
	}

	if (selview[3 * face_id] == vid)
	{
		orgin_uv[face_id * 3 * 2 + 0] = proj_uv[face_id * 3 * 2 + 0];
		orgin_uv[face_id * 3 * 2 + 1] = proj_uv[face_id * 3 * 2 + 1];

		orgin_uv[face_id * 3 * 2 + 2] = proj_uv[face_id * 3 * 2 + 2];
		orgin_uv[face_id * 3 * 2 + 3] = proj_uv[face_id * 3 * 2 + 3];

		orgin_uv[face_id * 3 * 2 + 4] = proj_uv[face_id * 3 * 2 + 4];
		orgin_uv[face_id * 3 * 2 + 5] = proj_uv[face_id * 3 * 2 + 5];
	}
}

void GetFaceLabel(const int vid, const int img_W, const int img_H, const int face_num, const int* sel_view, const float* puv, const int* label_image, int* outlabel)
{
	dim3 block(256, 1);
	dim3 grid((face_num + block.x - 1) / block.x, 1);

	FaceLabelKernel << <grid, block >> > (face_num, vid, img_W, img_H, sel_view, puv, label_image, outlabel);

	CUDA_CHECK_ERROR();
}

void FillNoViewFaceFromFaceFaceTopology(int nface, int* selview, int* faceface_topology, int topy_list_len)
{
	dim3 block(128, 1);
	dim3 grid((nface + block.x - 1) / block.x, 1);

	FillSelViewFaceFaceTopologyKernel << <grid, block >> > (nface, selview, faceface_topology, topy_list_len);

	CUDA_CHECK_ERROR();
}

void FixNoViewFaceUV(int nface, int vid,  const int* selview, const float* proj_uv, float* orgin_uv)
{
	dim3 block(128, 1);
	dim3 grid((nface + block.x - 1) / block.x, 1);

	FixNoViewFaceUVKernel << <grid, block >> > (nface, vid, selview, proj_uv, orgin_uv);

	CUDA_CHECK_ERROR();

}


void CleanSelViewFromFaceFaceTopology(int nface, int* selview, int* faceface_topology, int topy_list_len)
{
	dim3 block(128, 1);
	dim3 grid((nface + block.x - 1) / block.x, 1);

	int iter = 0;

	while (iter < 3)
	{
		CleanSelViewUVKernel << <grid, block >> > (nface, selview, faceface_topology, topy_list_len);
		iter++;
	}

	CUDA_CHECK_ERROR();
}