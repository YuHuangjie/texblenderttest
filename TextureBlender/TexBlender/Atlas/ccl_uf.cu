#include <stdio.h>  
#include <stdlib.h>
#include <assert.h>
#include <cuda.h>  

#include "TexAtlas.h"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/generate.h>
#include <thrust/sequence.h>
#include <thrust/sort.h>

#define UF_BLOCK_SIZE_X 32
#define UF_BLOCK_SIZE_Y 16

struct Node {

	int parent;

	int left;
	int right;

	int Wb;
	int Hb;

	int ox;
	int oy;

	int BinId;

};

__device__	int		g_vid;
__device__	float	g_bins_area;
__device__	float	g_bins_len;

__device__  Bin2D	g_dev_bins[MAX_BINS];
__device__  int		g_dev_label2bin[MAX_BINS];

__device__	int		g_dev_bincount;

__device__	int		g_dev_labelcount;
__device__	int		g_child_count;
__device__  Node	g_child_list[MAX_CHILD_NUMBER];

texture<int, 2, cudaReadModeElementType> imgtex_uf;

namespace UnionFind {

	__device__ int push_bin(Bin2D bin)
	{
		int insert_pos = atomicAdd(&g_dev_bincount, 1);

		if (insert_pos < MAX_BINS)
		{
			g_dev_bins[insert_pos] = bin;
			return insert_pos;
		}
		else
		{
			atomicSub(&g_dev_bincount, 1);
			return -1;
		}
	}

	__device__ int find(int* buf, int x) {
		while (x != buf[x]) {
			x = buf[x];
		}
		return x;
	}

	__device__ void findAndUnion(int* buf, int g1, int g2)
	{
		bool done;
		do {

			g1 = find(buf, g1);
			g2 = find(buf, g2);

			// it should hold that g1 == buf[g1] and g2 == buf[g2] now

			if (g1 < g2) {
				int old = atomicMin(&buf[g2], g1);
				done = (old == g2);
				g2 = old;
			}
			else if (g2 < g1) {
				int old = atomicMin(&buf[g1], g2);
				done = (old == g1);
				g1 = old;
			}
			else {
				done = true;
			}

		} while (!done);
	}

	__global__ void UF_prev(int* label, int* label_clean, int* minx, int* miny, int* maxx, int* maxy, int w, int h)
	{
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;

		int global_index = x + y*w;

		bool in_limits = x < w && y < h;

		if (in_limits)
		{
			label[global_index] = -1;
			label_clean[global_index] = -1;

			minx[global_index] = w;
			miny[global_index] = h;

			maxx[global_index] = -1;
			maxy[global_index] = -1;
		}
	}

	__global__ void UF_local(int* label, int w, int h)
	{
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;
		int global_index = x + y*w;
		int block_index = UF_BLOCK_SIZE_X * threadIdx.y + threadIdx.x;

		__shared__ int s_buffer[UF_BLOCK_SIZE_X * UF_BLOCK_SIZE_Y];
		__shared__ int s_img[UF_BLOCK_SIZE_X * UF_BLOCK_SIZE_Y];

		bool in_limits = x < w && y < h;

		s_buffer[block_index] = block_index;
		s_img[block_index] = in_limits ? tex2D(imgtex_uf, x, y) : 0xFF;
		__syncthreads();

		int v = s_img[block_index];

		if (in_limits && threadIdx.x > 0 && s_img[block_index - 1] == v) {
			findAndUnion(s_buffer, block_index, block_index - 1);
		}

		__syncthreads();

		if (in_limits && threadIdx.y > 0 && s_img[block_index - UF_BLOCK_SIZE_X] == v) {
			findAndUnion(s_buffer, block_index, block_index - UF_BLOCK_SIZE_X);
		}

		__syncthreads();

		if (in_limits) {
			int f = find(s_buffer, block_index);
			int fx = f % UF_BLOCK_SIZE_X;
			int fy = f / UF_BLOCK_SIZE_X;
			label[global_index] = (blockIdx.y*UF_BLOCK_SIZE_Y + fy)*w +
				(blockIdx.x*blockDim.x + fx);
		}

	}

	__global__ void UF_global(int* label, int w, int h)
	{
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;
		int global_index = x + y*w;

		bool in_limits = x < w && y < h;
		int  v = (in_limits ? tex2D(imgtex_uf, x, y) : 0xFF);

		if (in_limits && y > 0 && threadIdx.y == 0 && tex2D(imgtex_uf, x, y - 1) == v) {
			findAndUnion(label, global_index, global_index - w);
		}

		if (in_limits && x > 0 && threadIdx.x == 0 && tex2D(imgtex_uf, x - 1, y) == v) {
			findAndUnion(label, global_index, global_index - 1);
		}

	}

	__global__ void UF_final(int* label, int w, int h)
	{
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;

		int global_index = x + y*w;

		bool in_limits = x < w && y < h;

		if (in_limits) {
			label[global_index] = find(label, global_index);
		}
	}

	__global__ void UF_MergeBins(int* label, int* minx, int* miny, int* maxx, int* maxy, int w, int h)
	{
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;

		int global_index = x + y*w;

		bool in_limits = x < w && y < h;

		if (!in_limits) return;
		if (label[global_index] <= 0) return;
		// don't extract holes, may used for clean up in the future, ignore holes for now
		if (tex2D(imgtex_uf, x, y) == 0) return;

		int lab = label[global_index];

		atomicMin(&minx[lab], x);
		atomicMin(&miny[lab], y);

		atomicMax(&maxx[lab], x);
		atomicMax(&maxy[lab], y);

	}

	__global__ void UF_CleanLabel(int* label_clean, int* minx, int* miny, int* maxx, int* maxy, int w, int h)
	{
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;

		int global_index = x + y*w;

		bool in_limits = x < w && y < h;

		if (!in_limits) return;

		if (maxx[global_index] >= 0 || maxy[global_index] >= 0)
		{
			int lc = atomicAdd(&g_dev_labelcount, 1);

			if (g_dev_labelcount > MAX_BINS)
				return;

			label_clean[global_index] = lc;
		}
	}

	__global__ void UF_ReplaceLabel(int* label, int* label_clean, int w, int h)
	{
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;

		int global_index = x + y*w;

		bool in_limits = x < w && y < h;

		if (!in_limits || label[global_index] < 0) return;

		label[global_index] = label_clean[label[global_index]];
	}

	__global__ void UF_ExtractBins(int* label_clean, int* minx, int* miny, int* maxx, int* maxy, int w, int h)
	{
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;

		int global_index = x + y*w;

		bool in_limits = x < w && y < h;

		if (!in_limits) return;

		if (maxx[global_index] >= 0 && maxy[global_index] >= 0)
		{
			Bin2D tbin;

			tbin.vid = g_vid;
			tbin.label = label_clean[global_index];

			tbin.im_x = minx[global_index] - TEX_PADDING / 2;
			tbin.im_y = miny[global_index] - TEX_PADDING / 2;
			tbin.W = maxx[global_index] - minx[global_index] + 1 + TEX_PADDING;
			tbin.H = maxy[global_index] - miny[global_index] + 1 + TEX_PADDING;
			tbin.tex_x = 0;
			tbin.tex_y = 0;

			atomicAdd(&g_bins_area, tbin.W * tbin.H);

			push_bin(tbin);
		}
	}

	CCL_UF::CCL_UF()
	{
		W = 0;
		H = 0;

		L = NULL;
		L_Clean = NULL;

		Minx = NULL;
		Miny = NULL;
		Maxx = NULL;
		Maxy = NULL;
	}

	CCL_UF::~CCL_UF()
	{
		if (L != NULL)	    CUDA_SAFE_CALL(cudaFree(L));
		if (L_Clean != NULL)	CUDA_SAFE_CALL(cudaFree(L_Clean));

		if (Minx != NULL)	CUDA_SAFE_CALL(cudaFree(Minx));
		if (Miny != NULL)	CUDA_SAFE_CALL(cudaFree(Miny));
		if (Maxx != NULL)	CUDA_SAFE_CALL(cudaFree(Maxx));
		if (Maxy != NULL)	CUDA_SAFE_CALL(cudaFree(Maxy));
	}

	void CCL_UF::Clear()
	{
		int zero = 0;
		int one = 1;
		float fzero = 0.0;

		CUDA_SAFE_CALL(cudaMemcpyToSymbol(g_dev_labelcount, &one, sizeof(int)));
		CUDA_SAFE_CALL(cudaMemcpyToSymbol(g_dev_bincount, &zero, sizeof(int)));
		CUDA_SAFE_CALL(cudaMemcpyToSymbol(g_bins_area, &fzero, sizeof(float)));
	}

	void CCL_UF::Label(cudaArray* ImgArry, int vid, int w_, int h_, void** label)
	{
		cudaError_t err;

		int  N = W * H;

		if (w_ != W || h_ != H)
		{
			W = w_; 	H = h_; 	N = W * H;

			if (L != NULL) CUDA_SAFE_CALL(cudaFree(L));

			if (L_Clean != NULL) CUDA_SAFE_CALL(cudaFree(L_Clean));

			if (Minx != NULL) CUDA_SAFE_CALL(cudaFree(Minx));
			if (Miny != NULL) CUDA_SAFE_CALL(cudaFree(Miny));
			if (Maxx != NULL) CUDA_SAFE_CALL(cudaFree(Maxx));
			if (Maxy != NULL) CUDA_SAFE_CALL(cudaFree(Maxy));

			CUDA_SAFE_CALL(cudaMalloc((void**)&L, sizeof(int) * N));

			CUDA_SAFE_CALL(cudaMalloc((void**)&L_Clean, sizeof(int) * N));

			CUDA_SAFE_CALL(cudaMalloc((void**)&Minx, sizeof(int) * N));
			CUDA_SAFE_CALL(cudaMalloc((void**)&Miny, sizeof(int) * N));
			CUDA_SAFE_CALL(cudaMalloc((void**)&Maxx, sizeof(int) * N));
			CUDA_SAFE_CALL(cudaMalloc((void**)&Maxy, sizeof(int) * N));
		}

		CUDA_SAFE_CALL(cudaMemcpyToSymbol(g_vid, &vid, sizeof(int)));

		cudaChannelFormatDesc intdesc = cudaCreateChannelDesc<int>();
		cudaBindTextureToArray(imgtex_uf, ImgArry, intdesc);

		dim3 block(UF_BLOCK_SIZE_X, UF_BLOCK_SIZE_Y);
		dim3 grid((W + UF_BLOCK_SIZE_X - 1) / UF_BLOCK_SIZE_X, (H + UF_BLOCK_SIZE_Y - 1) / UF_BLOCK_SIZE_Y);

		cudaThreadSetCacheConfig(cudaFuncCachePreferShared);

		UF_prev << <grid, block >> > (L, L_Clean, Minx, Miny, Maxx, Maxy, W, H);

		UF_local << <grid, block >> > (L, W, H);

		cudaThreadSetCacheConfig(cudaFuncCachePreferL1);

		UF_global << <grid, block >> > (L, W, H);

		UF_final << <grid, block >> > (L, W, H);

		// Find Bin2Ds
		UF_MergeBins << <grid, block >> > (L, Minx, Miny, Maxx, Maxy, W, H);

		UF_CleanLabel << <grid, block >> > (L_Clean, Minx, Miny, Maxx, Maxy, W, H);

		UF_ReplaceLabel << <grid, block >> > (L, L_Clean, W, H);

		UF_ExtractBins << <grid, block >> > (L_Clean, Minx, Miny, Maxx, Maxy, W, H);

		*label = L;

		CUDA_CHECK_ERROR();
	}

	void CCL_UF::GetBinsData(Bin2D* bins, int& bin_count, float& bin_area)
	{
		CUDA_SAFE_CALL(cudaMemcpyFromSymbol(bins, g_dev_bins, MAX_BINS * sizeof(Bin2D), 0, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpyFromSymbol(&bin_count, g_dev_bincount, sizeof(int), 0, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpyFromSymbol(&bin_area, g_bins_area, sizeof(float), 0, cudaMemcpyDeviceToHost));

		CUDA_CHECK_ERROR();
	}

	void CCL_UF::SetBinsData(Bin2D* bins, int bin_count, float bin_len)
	{
		CUDA_SAFE_CALL(cudaMemcpyToSymbol(g_dev_bins, bins, MAX_BINS * sizeof(Bin2D), 0, cudaMemcpyHostToDevice));
		CUDA_SAFE_CALL(cudaMemcpyToSymbol(g_bins_len, &bin_len, sizeof(float), 0, cudaMemcpyHostToDevice));

		CUDA_CHECK_ERROR();
	}
}


__global__ void Init()
{
	unsigned int Index = threadIdx.x + blockIdx.x * blockDim.x;

	if (Index < g_dev_bincount)
	{
		if (g_dev_bins[Index].label >= 0 && g_dev_bins[Index].label < MAX_BINS)
			g_dev_label2bin[g_dev_bins[Index].label] = Index;
	}

	if (Index < MAX_CHILD_NUMBER)
	{
		if (Index == 0) 
		{
			g_child_count = 1;
			g_bins_len = int(sqrt(g_bins_area) * LOOSE_PERCENT) + 1.0;

			g_child_list[Index].Wb = int(g_bins_len);
			g_child_list[Index].Hb = int(g_bins_len);
		}
		else 
		{
			g_child_list[Index].Wb = -1;
			g_child_list[Index].Hb = -1;
		}

		g_child_list[Index].BinId = -1;

		g_child_list[Index].ox = 0;
		g_child_list[Index].oy = 0;

		g_child_list[Index].parent = -1;
		g_child_list[Index].left = -1;
		g_child_list[Index].right = -1;
	}
}

__device__ void SplitCanvas(int cur, int bid, int lid, int rid)
{

	int diff_w = g_child_list[cur].Wb - g_dev_bins[bid].W;
	int diff_h = g_child_list[cur].Hb - g_dev_bins[bid].H;

	if (g_child_list[lid].left >= 0 ||
		g_child_list[lid].right >= 0 ||
		g_child_list[rid].left >= 0 ||
		g_child_list[rid].right >= 0)
	{
		// something is wrong
	}

	g_child_list[lid] = g_child_list[cur];
	g_child_list[rid] = g_child_list[cur];

	g_child_list[cur].left = lid;
	g_child_list[cur].right = rid;

	g_child_list[lid].BinId = -1;
	g_child_list[rid].BinId = -1;

	g_child_list[lid].parent = cur;
	g_child_list[rid].parent = cur;

	if (diff_w > diff_h)
	{
		g_child_list[lid].Wb  = g_dev_bins[bid].W;
		g_child_list[rid].Wb -= g_dev_bins[bid].W;

		g_child_list[rid].ox += g_dev_bins[bid].W;
	}
	else
	{
		g_child_list[lid].Hb  = g_dev_bins[bid].H;
		g_child_list[rid].Hb -= g_dev_bins[bid].H;

		g_child_list[rid].oy += g_dev_bins[bid].H;
	}

}

__global__ void PackingKernel()
{
	unsigned int index = threadIdx.x + blockIdx.x * blockDim.x;
	unsigned int stride = blockDim.x * gridDim.x;
	unsigned int offset = 0;

	bool newnode = true;
	bool leftdone = false;

	int  current;

	// insert block by block, reduce race rate
	while ((index + offset) < g_dev_bincount) {

		if (newnode)
		{
			current = 0;
			leftdone = false;
			newnode = false;
		}

		while (current >= 0)
		{
			// If left child is not traversed, find the leftmost child
			if (!leftdone)
			{
				while (g_child_list[current].left >= 0)
					current = g_child_list[current].left;
			}

			if (g_dev_bins[index + offset].W == g_child_list[current].Wb && g_dev_bins[index + offset].H == g_child_list[current].Hb)
			{
				// found, bin inserted
				g_child_list[current].BinId = index + offset;

				g_dev_bins[index + offset].tex_x = g_child_list[current].ox;
				g_dev_bins[index + offset].tex_y = g_child_list[current].oy;

				newnode = true;
				leftdone = false;

				offset += stride;
				current = -1;
				break;
			}

			if (g_child_list[current].left >= 0 || g_child_list[current].right >= 0 || g_dev_bins[index + offset].W > g_child_list[current].Wb || g_dev_bins[index + offset].H > g_child_list[current].Hb)
			{
				// not fit or has been filled, in order traverse
				leftdone = true;

				if (g_child_list[current].right >= 0) // Actually won't visit here
				{
					leftdone = false;
					current = g_child_list[current].right;
				}
				else if (g_child_list[current].parent >= 0) // Most time, here
				{
					// If this node is right child of its parent, visit parent's parent first
					while (g_child_list[current].parent &&
						current == g_child_list[g_child_list[current].parent].right)
						current = g_child_list[current].parent;

					if (g_child_list[current].parent < 0)
					{
						newnode = true;
						offset += stride;
						current = -1;

						break;
					}

					current = g_child_list[current].parent;
				}
				else
				{
					// has traversed whole tree, still no fit? have to break
					newnode = true;
					offset += stride;
					current = -1;

					break;
				}
			}
			else
			{
				// split space and fit in
				if (atomicCAS(&g_child_list[current].BinId, -1, -2) == -1) // mutex
				{
					int lch, rch, subl, subr;

					lch = atomicAdd(&g_child_count, 1);
					rch = atomicAdd(&g_child_count, 1);

					SplitCanvas(current, index + offset, lch, rch);

					subl = atomicAdd(&g_child_count, 1);
					subr = atomicAdd(&g_child_count, 1);

					SplitCanvas(lch, index + offset, subl, subr);

					// sub left cube should fit
					if (g_dev_bins[index + offset].W == g_child_list[subl].Wb && g_dev_bins[index + offset].H == g_child_list[subl].Hb)
					{
						g_child_list[subl].BinId = index + offset;

						g_dev_bins[index + offset].tex_x = g_child_list[subl].ox;
						g_dev_bins[index + offset].tex_y = g_child_list[subl].oy;

						newnode = true;
						offset += stride;
						current = -1;

						g_child_list[current].BinId = -1;

						break;
					}
					else {
						// shouldn't happen, something is wrong if we hit here

						newnode = true;
						offset += stride;
						current = -1;

						g_child_list[current].BinId = -1;
					}
				}
				else {
					newnode = true;
					break;
				}

			}
		}

		__syncthreads(); // not strictly needed 
	}

}

__device__ int Insert(int current, int bi)
{
	if (g_child_list[current].left >= 0 || g_child_list[current].right >= 0)
	{
		int res = Insert(g_child_list[current].left, bi);
		if (res == -1)
		{
			return Insert(g_child_list[current].right, bi);
		}
		return res;
	}
	else
	{
		if (g_child_list[current].BinId >= 0 || g_dev_bins[bi].W > g_child_list[current].Wb || g_dev_bins[bi].H > g_child_list[current].Hb)
		{
			return -1;
		}

		if (g_dev_bins[bi].W == g_child_list[current].Wb && g_dev_bins[bi].H == g_child_list[current].Hb)
		{
			if (atomicCAS(&g_child_list[current].BinId, -1, bi) == -1) // mutex
			{
				g_dev_bins[bi].tex_x = g_child_list[current].ox;
				g_dev_bins[bi].tex_y = g_child_list[current].oy;

				return 0;
			}
			else
			{
				return -2;
			}
		}

		if (atomicCAS(&g_child_list[current].BinId, -1, -2) == -1) // mutex
		{
			int lch = atomicAdd(&g_child_count, 1);
			int rch = atomicAdd(&g_child_count, 1);

			if (g_child_count > MAX_CHILD_NUMBER)
				return -1;

			SplitCanvas(current, bi, lch, rch);

			return Insert(lch, bi);
		}
		else {
			return -2;
		}
	}

}

__global__ void PackingRecursiveKernel()
{
	unsigned int index = threadIdx.x + blockIdx.x * blockDim.x;
	unsigned int stride = blockDim.x * gridDim.x;
	unsigned int offset = 0;

	int iter = 0;

	// insert block by block, reduce race rate
	while ((index + offset) < g_dev_bincount && iter < 1000) {

		int res = Insert(0, index + offset);

		if (res == 0) {
			// insert finish
			iter = 0;
			offset += stride;
		}
		else if (res == -2) {
			++iter;
		}
		else if (res == -1) {
			// fail to insert, break
			break;
		}
		else {
			// if we hit here, then we have a big problem
			break;
		}
		__syncthreads(); // not strictly needed 
	}

}


__global__ void InitLabelMapping()
{
	unsigned int Index = threadIdx.x + blockIdx.x * blockDim.x;

	if (Index < g_dev_bincount)
	{
		if (g_dev_bins[Index].label > 0 && g_dev_bins[Index].label < MAX_BINS)
			g_dev_label2bin[g_dev_bins[Index].label] = Index;
	}
}

__global__ void GetPackingUVKernel(int nface, const int* vid, const int* label, const float* puv, float* out_uv)
{
	unsigned int face_id = threadIdx.x + blockIdx.x * blockDim.x;

	if (face_id >= nface)
		return;

	int fl = label[face_id];

	if (fl > 0 && fl <= g_dev_bincount)
	{
		int tid = face_id * MULT_TEXC;
		int bid = g_dev_label2bin[fl];
		
		float offset_x = -g_dev_bins[bid].im_x + g_dev_bins[bid].tex_x;
		float offset_y = -g_dev_bins[bid].im_y + g_dev_bins[bid].tex_y;

		out_uv[tid + 0] = (puv[tid + 0] + offset_x) / g_bins_len;
		out_uv[tid + 1] = (puv[tid + 1] + offset_y) / g_bins_len;

		out_uv[tid + 2] = (puv[tid + 2] + offset_x) / g_bins_len;
		out_uv[tid + 3] = (puv[tid + 3] + offset_y) / g_bins_len;

		out_uv[tid + 4] = (puv[tid + 4] + offset_x) / g_bins_len;
		out_uv[tid + 5] = (puv[tid + 5] + offset_y) / g_bins_len;
	}
    else
    {
        int tid = face_id * MULT_TEXC;

        out_uv[tid + 0] = 1.0;
        out_uv[tid + 1] = 1.0;

        out_uv[tid + 2] = 1.0;
        out_uv[tid + 3] = 1.0;

        out_uv[tid + 4] = 1.0;
        out_uv[tid + 5] = 1.0;

    }
}

void TreeBasedBinPacking()
{
	dim3 block(256, 1);
	dim3 grid(MAX_CHILD_NUMBER / 256 / 2 + 1, 1);

	Init << <grid, block >> > ();

	PackingRecursiveKernel << <grid, block >> > ();

	CUDA_CHECK_ERROR();
}

void GetUVCoordinate(int nface, const int* vid, const int* label, const float* puv, float* out_uv)
{
	dim3 block_label(256, 1);
	dim3 grid_label(MAX_BINS / 256 + 1, 1);
	
	InitLabelMapping << <grid_label, block_label >> > ();

	dim3 block(256, 1);
	dim3 grid(nface / 256 + 1, 1);

	GetPackingUVKernel << <grid, block >> > (nface, vid, label, puv, out_uv);

	CUDA_CHECK_ERROR();
}

