#include  <numeric>
#include "FastPackingCPU.h"

TreeNode child_list[MAX_CHILD_NUMBER];

void FastPackingThreadRecursiveCPU::SplitCanvas_CPU(int cur, int bid, int lid, int rid, Bin2D* bins)
{
	int diff_w = child_list[cur].Wr - bins[bid].W;
	int diff_h = child_list[cur].Hr - bins[bid].H;

	if (child_list[lid].left_child >= 0 ||
		child_list[lid].right_child >= 0 ||
		child_list[rid].left_child >= 0 ||
		child_list[rid].right_child >= 0)
	{
		// shouldn't hit here, something is wrong
	}

	child_list[lid] = child_list[cur];
	child_list[rid] = child_list[cur];

	child_list[cur].left_child = lid;
	child_list[cur].right_child = rid;

	child_list[lid].BinId = -1;
	child_list[rid].BinId = -1;

	if (diff_w > diff_h)
	{
		child_list[lid].Wr = bins[bid].W;
		child_list[rid].Wr -= bins[bid].W;

		child_list[rid].x += bins[bid].W;
	}
	else
	{
		child_list[lid].Hr = bins[bid].H;
		child_list[rid].Hr -= bins[bid].H;

		child_list[rid].y += bins[bid].H;
	}
}

int FastPackingThreadRecursiveCPU::Insert_CPU(int current, int bi, Bin2D* bins)
{
	if (child_list[current].left_child >= 0 || child_list[current].right_child >= 0)
	{
		int res = Insert_CPU(child_list[current].left_child, bi, bins);

		if (res == -1)
		{
			return Insert_CPU(child_list[current].right_child, bi, bins);
		}
		return res;
	}
	else
	{
		if (child_list[current].BinId >= 0 || bins[bi].W > child_list[current].Wr || bins[bi].H > child_list[current].Hr)
		{
			return -1;
		}

		if (bins[bi].W == child_list[current].Wr && bins[bi].H == child_list[current].Hr)
		{

			{
				omp_set_lock(&insrt_lock);
				{
					if (child_list[current].BinId == -1) // mutex
					{
						child_list[current].BinId = bi;
						bins[bi].tex_x = child_list[current].x;
						bins[bi].tex_y = child_list[current].y;

						omp_unset_lock(&insrt_lock);

						return 0;
					}
					else
					{
						omp_unset_lock(&insrt_lock);

						return -2;
					}
				}

			}
		}

		{
			omp_set_lock(&split_lock);

			if (child_list[current].BinId == -1) // mutex
			{
				child_list[current].BinId = -2;

				int lch = child_count++;
				int rch = child_count++;

				if (child_count > MAX_CHILD_NUMBER)
					return -1;

				SplitCanvas_CPU(current, bi, lch, rch, bins);

				omp_unset_lock(&split_lock);

				return Insert_CPU(lch, bi, bins);
			}
			else
			{
				omp_unset_lock(&split_lock);
				return -2;
			}

		}
	}
}

void FastPackingThreadRecursiveCPU::PackingRecursive(int bin_count, int canvas_width, int canvas_height, Bin2D* bins)
{
	child_count = 1;

	omp_init_lock(&split_lock);
	omp_init_lock(&insrt_lock);

#pragma omp parallel for schedule(dynamic)  
	for (int i = 0; i < MAX_CHILD_NUMBER; ++i)
	{
		child_list[i].BinId = -1;

		if (i == 0)
		{
			child_list[i].Wr = canvas_width;
			child_list[i].Hr = canvas_height;
		}
		else
		{
			child_list[i].Wr = -1;
			child_list[i].Hr = -1;
		}

		child_list[i].x = 0;
		child_list[i].y = 0;

		child_list[i].left_child = -1;
		child_list[i].right_child = -1;
	}

#pragma omp parallel for schedule(dynamic)  
	for (int i = 0; i < bin_count; ++i)
	{
		int iter = 0;

		while (iter < 1000)
		{
			int res = Insert_CPU(0, i, bins);

			if (res == 0) {
				// insert finish
				iter = 0;
				break;
			}
			else if (res == -2) {
				++iter;
			}
			else if (res == -1) {
				// fail to insert, break
				break;
			}
			else {
				// not possible
				break;
			}
		}
	}

}
