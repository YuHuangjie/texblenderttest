#pragma once
#include <omp.h>

#include "Common/comm.h"

class FastPackingThreadRecursiveCPU {

private:

	int		   child_count;

	omp_lock_t split_lock;
	omp_lock_t insrt_lock;

public:

	int Insert_CPU(int current, int bi, Bin2D* bins);

	void SplitCanvas_CPU(int cur, int bid, int lid, int rid, Bin2D* bins);

	void PackingRecursive(int bin_count, int canvas_width, int canvas_height, Bin2D* bins);
};