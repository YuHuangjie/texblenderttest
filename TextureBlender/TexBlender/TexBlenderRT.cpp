#include "TexBlenderRT.h"

using namespace TexBlender;

bool TexBlender_RealTime::InitRender(void *ctx, OBJRender::RenderType type, int estimated_face,
	int im_w, int im_h, int tex_width, int tex_height)
{
	CUDA_SAFE_CALL(cudaSetDevice(m_cuda_devID));
	m_render = new OBJRender(m_cuda_devID, ctx, type, estimated_face, im_w, im_h, tex_width, tex_height);
	return true;
}

bool TexBlender_RealTime::AppendViews(int id, int w, int h, float* k, float* m, float* k_dev, float* m_dev, void* img)
{
	CUDA_SAFE_CALL(cudaSetDevice(m_cuda_devID));
	m_view_cuda[id] = new View_Cuda(id, w, h, k, m, k_dev, m_dev, img);
	return true;
}

bool TexBlender_RealTime::AppendViews(int id, int w, int h, float* k, float* m, void* img)
{
	CUDA_SAFE_CALL(cudaSetDevice(m_cuda_devID));

	m_view_cuda[id] = new View_Cuda(id, w, h, k, m, img);
	return true;
}

void TexBlender_RealTime::SetMeshAndTexRGB(CuMesh::TriMesh *mesh, std::map<int, void*>& map_texrgb)
{
	m_mesh = mesh;

	for (auto& vw : m_view_cuda)
	{
		vw.second->m_cpu_image = map_texrgb[vw.first];
	}
}

//bool TexBlender_RealTime::UpdateMesh(ITMMesh *mesh, int frome_gpuid)
//{
//	CUDA_SAFE_CALL(cudaSetDevice(m_cuda_devID));
//	m_mesh->Update(mesh, frome_gpuid);
//
//	return true;
//}


bool TexBlender_RealTime::BuildTexture(double packing_loose)
{
	CUDA_SAFE_CALL(cudaSetDevice(m_cuda_devID));

	m_render->MakeContextCurrent();

	if (m_mesh->FN == 0)  return false;

	m_render->TransferMeshToGL(m_mesh);
	m_atlas.UpdateBufferSize(m_mesh->FN, m_mesh->FN_Capacity);

	for (auto vw : m_view_cuda) 
	{
		CUDA_SAFE_CALL(cudaSetDevice(m_cuda_devID));
		vw.second->UpdateBufferSize(m_mesh->FN, m_mesh->FN_Capacity);

		m_render->SetWindow(vw.second->W(), vw.second->H(), TEX_WIDTH, TEX_HEIGHT);
		m_render->SetMatrices(vw.second->GetModelViewMatrix(), vw.second->GetProjectionMatrix());
		m_render->SetDepthRange(vw.second->m_mindepth, vw.second->m_maxdepth);

		m_render->RenderVisibility();

		m_render->TransferVisibilityToCuda(vw.second->m_dev_visibility);
		vw.second->ViewProjection(m_mesh);
		m_atlas.Compare(vw.second->id, vw.second->m_dev_facearea, vw.second->m_dev_faceuv);

		CUDA_CHECK_ERROR();
		
		// debug save visibility to ply
		//if(g_debug.SaveFile) Debug_VisibilityCheck(vw.second);
	}

	if(INTERPRETATE_MESH_TOPOLOGY)
		m_atlas.FillNoViewFace((int*)m_mesh->FaceFaceTopology(CuMesh::GPU) , TOPOLOGY_LIST_LEN);
	
	m_render->TransferSelectedViewsToGL(m_atlas.m_dev_viewid);
	m_atlas.AtlasPrepare();

	// debug save selected view to ply
	//if (g_debug.SaveFile) Debug_SelectedViewCheck();

	for (auto vw : m_view_cuda)
	{
		CUDA_SAFE_CALL(cudaSetDevice(m_cuda_devID));

		if (INTERPRETATE_MESH_TOPOLOGY)
			m_atlas.FillNoViewFaceUV(vw.second->id, vw.second->m_dev_faceuv);

		m_render->SetWindow(vw.second->W(), vw.second->H(), TEX_WIDTH, TEX_HEIGHT);
		m_render->SetMatrices(vw.second->GetModelViewMatrix(), vw.second->GetProjectionMatrix());
		m_render->SetDepthRange(vw.second->m_mindepth, vw.second->m_maxdepth);
	
		m_render->RenderSelectedView(vw.second->id);

		m_render->TransferSelectedViewTextureToCUDA(&m_atlas.m_dev_tex_array);
		m_atlas.  ExtractConnectedComponents(vw.second->id, vw.second->W(), vw.second->H());
		m_render->ReleaseSelectedViewTextureToGL();
	}

	CUDA_SAFE_CALL(cudaSetDevice(m_cuda_devID));
	//m_atlas.ExtractUVCoordinate(m_mesh.TexCoord(GPU));
	m_atlas.ExtractUVCoordinateCPU(m_mesh->TexCoord(CuMesh::GPU), packing_loose);
	m_mesh->HasSetTexCoord();
	
	// debug save original uv to ply
	//if (g_debug.SaveFile) Debug_OriginalUVCheck();

	m_render->SetType(OBJRender::RenderType::UVBLEND);
	//m_render->TransferMeshToGL(m_mesh);
	m_render->TransferUVCoordinateToGL(m_mesh);
	m_render->TransferSelectedViewsToGL(m_atlas.m_dev_viewid);
	m_manifold.InitDeviceMemory();

	for (auto vw : m_view_cuda)
	{
		CUDA_SAFE_CALL(cudaSetDevice(m_cuda_devID));
		m_render->SetWindow(vw.second->W(), vw.second->H(), TEX_WIDTH, TEX_HEIGHT);
		m_render->SetMatrices(vw.second->GetModelViewMatrix(), vw.second->GetProjectionMatrix());
		m_render->SetDepthRange(vw.second->m_mindepth, vw.second->m_maxdepth);
		
		m_render->SetTexture(vw.second->m_cpu_image, vw.second->W(), vw.second->H(), 3);
		m_render->TransferVisibilityToGL(vw.second->m_dev_visibility);

		m_render->RenderUV(vw.second->id);

		m_render->TransferUVFrequenciesViewTextureToCUDA(&m_manifold.m_dev_lowfreq_array, &m_manifold.m_dev_higfreq_array);
		m_manifold.BlendView();
		m_render->ReleaseUVFrequenciesViewTextureToGL();
	}

	//m_manifold.SaveFreqsToFile();

	m_manifold.Finalize();
	m_manifold.GetTexture(m_mesh->Texture_Raw(CuMesh::GPU));
	
	//m_manifold.SaveToFile();

	CUDA_CHECK_ERROR();

	m_render->RestorePreviousContext();

	return true;
}

TexBlender_RealTime::~TexBlender_RealTime() {


}

void TexBlender_RealTime::Debug_VisibilityCheck(View_Cuda* vw) {

	//debug visibility rendering results

	int m_face_useful = m_mesh->FN;

	int* cpu_vis = new int[m_mesh->FN * 3];
	float* cpu_vert = new float[m_mesh->FN * 3 * 3];
	CUDA_SAFE_CALL(cudaMemcpy(cpu_vis, vw->m_dev_visibility, m_face_useful * MULT_VISB * sizeof(TYPE_VISB), cudaMemcpyDeviceToHost));
	CUDA_SAFE_CALL(cudaMemcpy(cpu_vert, m_mesh->Vert(CuMesh::GPU), m_face_useful * MULT_VERT * sizeof(TYPE_VERT), cudaMemcpyDeviceToHost));

	std::stringstream debug_outputstream;

	debug_outputstream << "ply\nformat ascii 1.0\n";
	debug_outputstream << "element vertex " << m_face_useful * 3 << "\n";
	debug_outputstream << "property float x\nproperty float y\nproperty float z\nproperty uchar red\nproperty uchar green\nproperty uchar blue" << std::endl;
	debug_outputstream << "element face " << m_face_useful << std::endl;
	debug_outputstream << "property list uchar int vertex_indices" << std::endl;
	debug_outputstream << "end_header" << std::endl;
	debug_outputstream << std::endl;

	for (int i = 0; i < m_face_useful; ++i)
	{
		for (int j = 0; j < 3; ++j) {
			debug_outputstream << cpu_vert[i * 3 * 3 + j * 3] << " " << cpu_vert[i * 3 * 3 + j * 3 + 1] << " " << cpu_vert[i * 3 * 3 + j * 3 + 2] << " ";

			if (cpu_vis[i * 3] > 0 || cpu_vis[i * 3 + 1] > 0 || cpu_vis[i * 3 + 2] > 0)
				debug_outputstream << 255 << " " << 0 << " " << 0 << std::endl;
			else
				debug_outputstream << 0 << " " << 0 << " " << 255 << std::endl;
		}
	}

	for (int i = 0; i < m_face_useful; ++i)
		debug_outputstream << 3 << " " << i * 3 + 0 << " " << i * 3 + 1 << " " << i * 3 + 2 << std::endl;

	//std::string debug_outputfile = g_debug.Folder + "\\" + "debug_visibility" + std::to_string(vw->id) +"f"+ std::to_string(g_debug.FrameNumber) + ".ply";
	//std::ofstream ofstr(debug_outputfile.c_str(), std::ios_base::trunc | std::ios_base::out);

	//ofstr.write(debug_outputstream.str().c_str(), debug_outputstream.str().length());
	//ofstr.close();
}

void TexBlender_RealTime::Debug_SelectedViewCheck() {

	int view_num = m_view_cuda.size();

	std::vector<unsigned char> rcolor(3 * view_num, 0);

	srand(time(NULL));
	
	int count = 0;

	for (auto vw: m_view_cuda)
	{
		rcolor[count + 0] = (count / 3 + 1) * 30; // rand() % 255 + 1;
		rcolor[count + 1] = (count / 3 + 1) * 30; // rand() % 255 + 1;
		rcolor[count + 2] = (count / 3 + 1) * 30; // rand() % 255 + 1;

		count += 3;
	}
	
	int m_face_useful = m_mesh->FN;

	int* cpu_selv = new int[m_mesh->FN * 3];
	float* cpu_vert = new float[m_mesh->FN * 3 * 3];
	CUDA_SAFE_CALL(cudaMemcpy(cpu_selv, m_atlas.m_dev_viewid, m_face_useful * MULT_VISB * sizeof(TYPE_VISB), cudaMemcpyDeviceToHost));
	CUDA_SAFE_CALL(cudaMemcpy(cpu_vert, m_mesh->Vert(CuMesh::GPU), m_face_useful * MULT_VERT * sizeof(TYPE_VERT), cudaMemcpyDeviceToHost));

	std::stringstream debug_outputstream;

	debug_outputstream << "ply\nformat ascii 1.0\n";
	debug_outputstream << "element vertex " << m_face_useful * 3 << "\n";
	debug_outputstream << "property float x\nproperty float y\nproperty float z\nproperty uchar red\nproperty uchar green\nproperty uchar blue" << std::endl;
	debug_outputstream << "element face " << m_face_useful << std::endl;
	debug_outputstream << "property list uchar int vertex_indices" << std::endl;
	debug_outputstream << "end_header" << std::endl;
	debug_outputstream << std::endl;

	for (int i = 0; i < m_face_useful; ++i)
	{
		for (int j = 0; j < 3; ++j) {
			debug_outputstream << cpu_vert[i * 3 * 3 + j * 3] << " " << cpu_vert[i * 3 * 3 + j * 3 + 1] << " " << cpu_vert[i * 3 * 3 + j * 3 + 2] << " ";
			debug_outputstream << cpu_selv[i * 3] * 30 << " " << cpu_selv[i * 3 + 1] * 30 << " " << cpu_selv[i * 3 + 2] * 30 << std::endl;
		}
	}

	for (int i = 0; i < m_face_useful; ++i)
		debug_outputstream << 3 << " " << i * 3 + 0 << " " << i * 3 + 1 << " " << i * 3 + 2 << std::endl;

	//std::string debug_outputfile = g_debug.Folder + "\\" + "debug_selectedview" + std::to_string(g_debug.FrameNumber) + ".ply";
	//std::ofstream ofstr(debug_outputfile.c_str(), std::ios_base::trunc | std::ios_base::out);

	//ofstr.write(debug_outputstream.str().c_str(), debug_outputstream.str().length());
	//ofstr.close();
}

void TexBlender_RealTime::Debug_VisRenderPassCheck(View_Cuda* vw) {

	cv::Mat vis(vw->H(), vw->W(), CV_32S);
	m_render->GetTextureData(m_render->renderedTexture, vis.data, 0);

	cv::Mat out_mat;
	vis.convertTo(out_mat, CV_8UC1, 1);
	cv::imwrite("visb" + std::to_string(vw->id) + ".jpg", out_mat);
}

void TexBlender_RealTime::Debug_OriginalUVCheck()
{
	float* cpu_uv = new float[m_mesh->FN * MULT_TEXC];
	CUDA_SAFE_CALL(cudaMemcpy(cpu_uv, m_atlas.m_dev_orgin_uv, m_mesh->FN * MULT_TEXC * sizeof(TYPE_TEXC), cudaMemcpyDeviceToHost));

	int m_face_useful = m_mesh->FN;

	int* cpu_selv = new int[m_mesh->FN * 3];
	float* cpu_vert = new float[m_mesh->FN * 3 * 3];
	CUDA_SAFE_CALL(cudaMemcpy(cpu_selv, m_atlas.m_dev_viewid, m_face_useful * MULT_VISB * sizeof(TYPE_VISB), cudaMemcpyDeviceToHost));
	CUDA_SAFE_CALL(cudaMemcpy(cpu_vert, m_mesh->Vert(CuMesh::GPU), m_face_useful * MULT_VERT * sizeof(TYPE_VERT), cudaMemcpyDeviceToHost));

	std::stringstream debug_outputstream;

	debug_outputstream << "ply\nformat ascii 1.0\n";
	debug_outputstream << "element vertex " << m_face_useful * 3 << "\n";
	debug_outputstream << "property float x\nproperty float y\nproperty float z" << std::endl;
	debug_outputstream << "element face " << m_face_useful << std::endl;
	debug_outputstream << "property list uchar int vertex_indices" << std::endl;
	debug_outputstream << "property list uchar float texcoord" << std::endl;
	debug_outputstream << "end_header" << std::endl;
	debug_outputstream << std::endl;

	for (int i = 0; i < m_face_useful; ++i)
	{
		for (int j = 0; j < 3; ++j) {
			debug_outputstream << cpu_vert[i * 3 * 3 + j * 3] << " " << cpu_vert[i * 3 * 3 + j * 3 + 1] << " " << cpu_vert[i * 3 * 3 + j * 3 + 2] << std::endl;
		}
	}

	for (int i = 0; i < m_face_useful; ++i) {
		debug_outputstream << 3 << " " << i * 3 + 0 << " " << i * 3 + 1 << " " << i * 3 + 2;

		debug_outputstream << " " << 6 << " "
			<< cpu_uv[i * 6] << ' '
			<< cpu_uv[i * 6 + 1] << ' '
			<< cpu_uv[i * 6 + 2] << ' '
			<< cpu_uv[i * 6 + 3] << ' '
			<< cpu_uv[i * 6 + 4] << ' '
			<< cpu_uv[i * 6 + 5] << std::endl;
	}

	//std::string debug_outputfile = g_debug.Folder + "\\" + "debug_originaluv" + std::to_string(g_debug.FrameNumber) + ".ply";
	//std::ofstream ofstr(debug_outputfile.c_str(), std::ios_base::trunc | std::ios_base::out);

	//ofstr.write(debug_outputstream.str().c_str(), debug_outputstream.str().length());
	//ofstr.close();

	return;
}

void TexBlender_RealTime::Debug_PureUVCheck()
{
	float* cpu_uv = new float[m_mesh->FN * MULT_TEXC];
	CUDA_SAFE_CALL(cudaMemcpy(cpu_uv, m_mesh->TexCoord(CuMesh::GPU), m_mesh->FN * MULT_TEXC * sizeof(TYPE_TEXC), cudaMemcpyDeviceToHost));

	std::stringstream debug_outputstream;
	debug_outputstream << "Bins = [";

	for (int i = 0; i < m_mesh->FN * 3; ++i) {
		debug_outputstream << cpu_uv[i * 2 + 0] << "," << cpu_uv[i * 2 + 1] << ";" << std::endl;
	}

	debug_outputstream << "];";
	//std::string debug_outputfile = g_debug.Folder + "\\" + "debug_packuv" + std::to_string(g_debug.FrameNumber) + ".m";
	//std::ofstream ofstr(debug_outputfile.c_str(), std::ios_base::trunc | std::ios_base::out);

	//ofstr.write(debug_outputstream.str().c_str(), debug_outputstream.str().length());
	//ofstr.close();

	//m_mesh.SaveToPly("uv.ply", false);
	
	return;
}