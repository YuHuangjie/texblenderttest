#pragma once

#include "Common/comm.h"
#include "Common/MeshDef.h"
#include "../Views/View_Cuda.h"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

namespace TexBlender {

	class TexManifold {

	public:

		cudaArray *m_dev_lowfreq_array;
		cudaArray *m_dev_higfreq_array;

		void* m_dev_lowfreq;
		void* m_dev_higfreq;

		void* m_dev_wl;
		void* m_dev_wh;

		void* m_dev_texture_final;

		std::vector<float*> m_dev_mipmaps;
		std::vector<float*> m_dev_mipmaps_alpha;
		std::vector<int>   m_dev_mipmaps_width;
		std::vector<int>   m_dev_mipmaps_height;

		void InitDeviceMemory();

		void BlendView();
		void Finalize();
		void SaveToFile();
		void SaveFreqsToFile();
		void GetTexture(void* tex);

		TexManifold();
		~TexManifold();
	};
}