#include "TexManifold.h"

#define HIGHFREQ_MULTIPLIER 1.00

using namespace TexBlender;

texture<uchar4, 2, cudaReadModeElementType> imgtex_lowfreq;
texture<uchar4, 2, cudaReadModeElementType> imgtex_higfreq;

__global__
void AccumulateFrequencies(const int W, const int H, float* dev_low_rgb, float* dev_hig_rgb, float* dev_low_alpha, float* dev_hig_alpha)
{
	const int x = blockIdx.x * blockDim.x + threadIdx.x;
	const int y = blockIdx.y * blockDim.y + threadIdx.y;

	if (x >= W || y >= H || x < 0 || y < 0)
		return;

	const int index = y * W + x;

	uchar4 ulowcolor = tex2D(imgtex_lowfreq, x, y);
	uchar4 uhigcolor = tex2D(imgtex_higfreq, x, y);

	float wl = (float)ulowcolor.w / 255.0;
	float wh = (float)uhigcolor.w / 255.0;

	dev_low_rgb[3 * index]	   += (float)ulowcolor.x * wl;
	dev_low_rgb[3 * index + 1] += (float)ulowcolor.y * wl;
	dev_low_rgb[3 * index + 2] += (float)ulowcolor.z * wl;

	dev_hig_rgb[3 * index]     += ((float)uhigcolor.x * 2.0 - 255.0) * wh;
	dev_hig_rgb[3 * index + 1] += ((float)uhigcolor.y * 2.0 - 255.0) * wh;
	dev_hig_rgb[3 * index + 2] += ((float)uhigcolor.z * 2.0 - 255.0) * wh;

	dev_low_alpha[index] += wl;
	dev_hig_alpha[index] += wh;
}

__global__
void CombineFrequencies(const int W, const int H, float* dev_low_rgb, float* dev_hig_rgb, float* dev_low_alpha, float* dev_hig_alpha)
{
	const int x = blockIdx.x * blockDim.x + threadIdx.x;
	const int y = blockIdx.y * blockDim.y + threadIdx.y;

	if (x >= W || y >= H || x < 0 || y < 0)
		return;

	const int index = y * W + x;

	if (dev_low_alpha[index] < 0.00001 || dev_hig_alpha[index] < 0.00001)
	{
		dev_low_rgb[3 * index]     = 0;
		dev_low_rgb[3 * index + 1] = 0;
		dev_low_rgb[3 * index + 2] = 0;
	}
	else
	{
		dev_low_rgb[3 * index] = dev_low_rgb[3 * index] / dev_low_alpha[index] + dev_hig_rgb[3 * index] / dev_hig_alpha[index] * HIGHFREQ_MULTIPLIER;

		dev_low_rgb[3 * index + 1] = dev_low_rgb[3 * index + 1] / dev_low_alpha[index] + dev_hig_rgb[3 * index + 1] / dev_hig_alpha[index] * HIGHFREQ_MULTIPLIER;

		dev_low_rgb[3 * index + 2] = dev_low_rgb[3 * index + 2] / dev_low_alpha[index] + dev_hig_rgb[3 * index + 2] / dev_hig_alpha[index] * HIGHFREQ_MULTIPLIER;
	}

	if (dev_low_rgb[3 * index] != 0 || dev_low_rgb[3 * index + 1] != 0 || dev_low_rgb[3 * index + 2] != 0)
		dev_low_alpha[index] = 1.0;
	else
		dev_low_alpha[index] = 0.0;

	dev_hig_rgb[3 * index]     = 0.0;
	dev_hig_rgb[3 * index + 1] = 0.0;
	dev_hig_rgb[3 * index + 2] = 0.0;

	dev_hig_alpha[index] = 0.0;
}

__global__ void PyrDownGaussKernel(float* src, float* dst, int sw, int sh, int dw, int dh, int dc)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;

	if (x >= dw || y >= dh)
		return;

	const int D = 5;

	int x_mi = max(0,  2 * x - D / 2) - 2 * x;
	int y_mi = max(0,  2 * y - D / 2) - 2 * y;

	int x_ma = min(sw, 2 * x - D / 2 + D) - 2 * x;
	int y_ma = min(sh, 2 * y - D / 2 + D) - 2 * y;

	float weights[] = { 0.375f, 0.25f, 0.0625f };

	for (int ch = 0; ch < dc; ++ch) 
	{
		float sum = 0;
		float wall = 0;

		for (int yi = y_mi; yi < y_ma; ++yi)
			for (int xi = x_mi; xi < x_ma; ++xi)
			{
				float val = src[((2 * y + yi) * sw + (2 * x + xi)) * dc + ch];

				sum  += val * weights[abs(xi)] * weights[abs(yi)];
				wall += weights[abs(xi)] * weights[abs(yi)];
			}

		dst[(y * dw + x) * dc + ch] = sum / wall;
	}
}

template<bool islevel0>
__global__ void PyrUpKernel(float* src_color, float* src_alpha, float* dst_color, float* dst_alpha, int sw, int sh, int dw, int dh)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;

	if (x >= sw || y >= sh)
		return;

	if (src_alpha[y * sw + x] >= 0.00001)
		return;

	int mx = x / 2;
	int my = y / 2;

	if (mx >= dw) mx = dw - 1;
	if (my >= dh) my = dh - 1;
	if (mx <  0)  mx = 0;
	if (my <  0)  my = 0;

	for (int c = 0; c < 3; ++c)
		src_color[(y * sw + x) * 3 + c] = dst_color[(my * dw + mx) * 3 + c] / dst_alpha[my * dw + mx];

	src_alpha[y * sw + x] = 1.0;

	//src_alpha[y * sw + x] += dst_alpha[my * dw + mx];

	//if (islevel0)
	//{
	//	for (int c = 0; c < 3; ++c)
	//		src_color[(y * sw + x) * 3 + c] /= src_alpha[y * sw + x];
	//}
}

__global__
void DevFloatTexToDevCharTexKernel(float* dev_float_tex, unsigned char* dev_char_tex, int w, int h)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;

	if (x >= w || y >= h)
		return;

	for (int c = 0; c < 3; ++c)
	{
		int color = int(dev_float_tex[(y * w + x) * 3 - c + 2]);
        
        //int color = int(dev_float_tex[(y * w + x) * 3 + c]);

		if (color < 0)   color = 0;
		if (color > 255) color = 255;

		dev_char_tex[(y * w + x) * 3 + c] = color;
	}
}


void MultiBandSumation(cudaArray* lf, cudaArray* hf, float* dev_low_rgb, float* dev_hig_rgb, float* dev_low_alpha, float* dev_hig_alpha)
{
	cudaChannelFormatDesc descUChar = cudaCreateChannelDesc<uchar4>();
	cudaBindTextureToArray(imgtex_lowfreq, lf, descUChar);
	cudaBindTextureToArray(imgtex_higfreq, hf, descUChar);

	CUDA_CHECK_ERROR();

	dim3 block(16, 16);
	dim3 grid((TEX_WIDTH + block.x - 1)/block.x, (TEX_HEIGHT + block.y - 1) / block.y);
	
	AccumulateFrequencies << <grid, block>> > (TEX_WIDTH, TEX_HEIGHT, dev_low_rgb, dev_hig_rgb, dev_low_alpha, dev_hig_alpha);

	CUDA_CHECK_ERROR();
}

void MultiBandDvision(float* dev_low_rgb, float* dev_hig_rgb, float* dev_low_alpha, float* dev_hig_alpha)
{
	dim3 block(16, 16);
	dim3 grid((TEX_WIDTH + block.x - 1) / block.x, (TEX_HEIGHT + block.y - 1) / block.y);

	CombineFrequencies << <grid, block >> > (TEX_WIDTH, TEX_HEIGHT, dev_low_rgb, dev_hig_rgb, dev_low_alpha, dev_hig_alpha);

	CUDA_CHECK_ERROR();
}

void PushPullMip(float* dev_from_rgb, float* dev_from_alpha, float* dev_to_rgb, float* dev_to_alpha, int from_width, int from_height, int to_width, int to_height)
{
	dim3 block(16, 16);
	dim3 grid((to_width + block.x - 1) / block.x, (to_height + block.y - 1) / block.y);

	PyrDownGaussKernel << <grid, block >> > (dev_from_rgb,   dev_to_rgb,   from_width, from_height, to_width, to_height, 3);
	PyrDownGaussKernel << <grid, block >> > (dev_from_alpha, dev_to_alpha, from_width, from_height, to_width, to_height, 1);

	CUDA_CHECK_ERROR();
}

void PushPullFill(const bool islevel0, float* dev_src_rgb, float* dev_src_alpha, float* dev_mip_rgb, float* dev_mip_alpha, int src_width, int src_height, int mip_width, int mip_height)
{
	dim3 block(16, 16);
	dim3 grid((src_width + block.x - 1) / block.x, (src_height + block.y - 1) / block.y);

	if(islevel0)
		PyrUpKernel <true>  << <grid, block >> > (dev_src_rgb, dev_src_alpha, dev_mip_rgb, dev_mip_alpha, src_width, src_height, mip_width, mip_height);
	else
		PyrUpKernel <false> << <grid, block >> > (dev_src_rgb, dev_src_alpha, dev_mip_rgb, dev_mip_alpha, src_width, src_height, mip_width, mip_height);

	CUDA_CHECK_ERROR();
}

void FloatTexDevToCharTexDev(float* dev_float_tex, unsigned char* dev_char_tex, int width, int height)
{
	dim3 block(24, 24);
	dim3 grid((width + block.x - 1) / block.x, (height + block.y - 1) / block.y);

	DevFloatTexToDevCharTexKernel << <grid, block >> > (dev_float_tex, dev_char_tex, width, height);

	CUDA_CHECK_ERROR();
}