#include "TexManifold.h"
#include "TexManifold.h"

using namespace TexBlender;

void MultiBandSumation(cudaArray* lf, cudaArray* hf, float* dev_low_rgb, float* dev_hig_rgb, float* dev_low_alpha, float* dev_hig_alpha);
void MultiBandDvision (float* dev_low_rgb, float* dev_hig_rgb, float* dev_low_alpha, float* dev_hig_alpha);
void PushPullMip (float* dev_from_rgb, float* dev_from_alpha, float* dev_to_rgb, float* dev_to_alpha, int from_width, int from_height, int to_width, int to_height);

void PushPullFill(const bool islevel0, float* dev_src_rgb, float* dev_src_alpha, float* dev_mip_rgb, float* dev_mip_alpha, int src_width, int src_height, int mip_width, int mip_height);

void FloatTexDevToCharTexDev(float* dev_float_tex, unsigned char* dev_char_tex, int width, int height);

TexManifold::TexManifold() {

	m_dev_lowfreq_array = NULL;
	m_dev_higfreq_array = NULL;

	m_dev_lowfreq = NULL;
	m_dev_higfreq = NULL;

	m_dev_wl = NULL;
	m_dev_wh = NULL;
	
	m_dev_texture_final = NULL;
}

TexManifold::~TexManifold() {

	if (m_dev_lowfreq != NULL)
		CUDA_SAFE_CALL(cudaFree(m_dev_lowfreq));

	if (m_dev_higfreq != NULL)
		CUDA_SAFE_CALL(cudaFree(m_dev_higfreq));

	if (m_dev_wl != NULL)
		CUDA_SAFE_CALL(cudaFree(m_dev_wl));

	if (m_dev_wh != NULL)
		CUDA_SAFE_CALL(cudaFree(m_dev_wh));

	if (m_dev_texture_final != NULL)
		CUDA_SAFE_CALL(cudaFree(m_dev_texture_final));

	CUDA_CHECK_ERROR();

}

void TexManifold::InitDeviceMemory()
{
	if (m_dev_lowfreq == NULL || m_dev_higfreq == NULL || m_dev_wl == NULL || m_dev_wh == NULL)
	{
		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_lowfreq, TEX_WIDTH * TEX_HEIGHT * 3 * sizeof(float))
		);

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_higfreq, TEX_WIDTH * TEX_HEIGHT * 3 * sizeof(float))
		);

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_wl, TEX_WIDTH * TEX_HEIGHT * sizeof(float))
		);

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_wh, TEX_WIDTH * TEX_HEIGHT * sizeof(float))
		);

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_texture_final, TEX_WIDTH * TEX_HEIGHT * 3 * sizeof(unsigned char))
		);

		int w = TEX_WIDTH, h = TEX_HEIGHT;

		m_dev_mipmaps_width.push_back(w);
		m_dev_mipmaps_height.push_back(h);

		m_dev_mipmaps.push_back((float*)m_dev_lowfreq);
		m_dev_mipmaps_alpha.push_back((float*)m_dev_wl);

		float* pmm = (float*)m_dev_higfreq;
		float* pmm_alpha = (float*)m_dev_wh;

		while (w > 2 && h > 2)
		{
			w = w / 2;
			h = h / 2;

			m_dev_mipmaps_width.push_back(w);
			m_dev_mipmaps_height.push_back(h);

			m_dev_mipmaps.push_back(pmm);
			m_dev_mipmaps_alpha.push_back(pmm_alpha);

			pmm += w * h * 3;
			pmm_alpha += w * h;
		}
	}

	CUDA_SAFE_CALL(
		cudaMemset(m_dev_lowfreq, 0, TEX_WIDTH * TEX_HEIGHT * 3 * sizeof(float))
	);

	CUDA_SAFE_CALL(
		cudaMemset(m_dev_higfreq, 0, TEX_WIDTH * TEX_HEIGHT * 3 * sizeof(float))
	);

	CUDA_SAFE_CALL(
		cudaMemset(m_dev_wl, 0, TEX_WIDTH * TEX_HEIGHT * sizeof(float))
	);

	CUDA_SAFE_CALL(
		cudaMemset(m_dev_wh, 0, TEX_WIDTH * TEX_HEIGHT * sizeof(float))
	);


}

void TexManifold::BlendView()
{
	MultiBandSumation(m_dev_lowfreq_array, m_dev_higfreq_array, (float*)m_dev_lowfreq, (float*)m_dev_higfreq, (float*)m_dev_wl, (float*)m_dev_wh);
}

void TexManifold::Finalize()
{
	MultiBandDvision((float*)m_dev_lowfreq, (float*)m_dev_higfreq, (float*)m_dev_wl, (float*)m_dev_wh);

	for (int lev = 0; lev < m_dev_mipmaps.size() - 1; ++lev)
	{
		PushPullMip(m_dev_mipmaps[lev],
			m_dev_mipmaps_alpha[lev],
			m_dev_mipmaps[lev + 1],
			m_dev_mipmaps_alpha[lev + 1],
			m_dev_mipmaps_width[lev],
			m_dev_mipmaps_height[lev],
			m_dev_mipmaps_width[lev + 1],
			m_dev_mipmaps_height[lev + 1]);
	}

	for (int lev = m_dev_mipmaps.size() - 2; lev >= 1; --lev)
	{
		PushPullFill(false,
			m_dev_mipmaps[lev],
			m_dev_mipmaps_alpha[lev],
			m_dev_mipmaps[lev + 1],
			m_dev_mipmaps_alpha[lev + 1],
			m_dev_mipmaps_width[lev],
			m_dev_mipmaps_height[lev],
			m_dev_mipmaps_width[lev + 1],
			m_dev_mipmaps_height[lev + 1]);
	}

	int lev = 0;
	{
		PushPullFill(true,
			m_dev_mipmaps[lev],
			m_dev_mipmaps_alpha[lev],
			m_dev_mipmaps[lev + 1],
			m_dev_mipmaps_alpha[lev + 1],
			m_dev_mipmaps_width[lev],
			m_dev_mipmaps_height[lev],
			m_dev_mipmaps_width[lev + 1],
			m_dev_mipmaps_height[lev + 1]);
	}

	FloatTexDevToCharTexDev((float*)m_dev_lowfreq, (unsigned char*)m_dev_texture_final, TEX_WIDTH, TEX_HEIGHT);
}

void TexManifold::GetTexture(void* tex)
{
	CUDA_SAFE_CALL(
		cudaMemcpy(tex,
			m_dev_texture_final,
			TEX_WIDTH * TEX_HEIGHT * 3 * sizeof(unsigned char),
			cudaMemcpyDeviceToDevice));

	CUDA_CHECK_ERROR();
}

void TexManifold::SaveToFile() {

	for (int i = 0; i < m_dev_mipmaps.size(); ++i) {

		cv::Mat mip(m_dev_mipmaps_height[i], m_dev_mipmaps_width[i], CV_32FC3);

		CUDA_SAFE_CALL(
			cudaMemcpy(reinterpret_cast<void*>(mip.data), reinterpret_cast<void*>(m_dev_mipmaps[i]), m_dev_mipmaps_width[i] * m_dev_mipmaps_height[i] * 3 * sizeof(float), cudaMemcpyDeviceToHost)
		);

		cv::Mat outmip_mat;
		mip.convertTo(outmip_mat, CV_8UC3, 1);

		std::string mipfilename = "mip_lev" + std::to_string(i) + ".png";
		cv::imwrite(mipfilename, outmip_mat);

		cv::Mat mip_alpha(m_dev_mipmaps_height[i], m_dev_mipmaps_width[i], CV_32F);

		CUDA_SAFE_CALL(
			cudaMemcpy(reinterpret_cast<void*>(mip_alpha.data), reinterpret_cast<void*>(m_dev_mipmaps_alpha[i]), m_dev_mipmaps_width[i] * m_dev_mipmaps_height[i] * sizeof(float), cudaMemcpyDeviceToHost)
		);

		cv::Mat outalpha_mat;
		mip_alpha.convertTo(outalpha_mat, CV_8U, 100);

		std::string alphafilename = "mipalpha_lev" + std::to_string(i) + ".png";
		cv::imwrite(alphafilename, outalpha_mat);
	}

}

void TexManifold::SaveFreqsToFile() 
{
	cv::Mat lowfreq(TEX_HEIGHT, TEX_WIDTH, CV_32FC3);

	CUDA_SAFE_CALL(
		cudaMemcpy(reinterpret_cast<void*>(lowfreq.data), m_dev_lowfreq, TEX_HEIGHT * TEX_WIDTH * 3 * sizeof(float), cudaMemcpyDeviceToHost)
	);

	cv::Mat outlowfreq;
	lowfreq.convertTo(outlowfreq, CV_8UC3, 1.0);

	std::string lowfreqname = "low_freq.png";
	cv::imwrite(lowfreqname, outlowfreq);

	cv::Mat higfreq(TEX_HEIGHT, TEX_WIDTH, CV_32FC3);

	CUDA_SAFE_CALL(
		cudaMemcpy(reinterpret_cast<void*>(higfreq.data), m_dev_higfreq, TEX_HEIGHT * TEX_WIDTH * 3 * sizeof(float), cudaMemcpyDeviceToHost)
	);

	cv::Mat outhigfreq;
	higfreq.convertTo(outhigfreq, CV_8UC3, 1.0);

	std::string higfreqname = "hig_freq.png";
	cv::imwrite(higfreqname, outhigfreq);
}