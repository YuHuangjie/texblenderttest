#include <windows.h>

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <fstream>

#include <GL/glew.h>
#include <gl/GL.h>
#include <GLFW/glfw3.h>	// Window & keyboard, contains OpenGL

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "OBJRender.h"

const std::string gVertexShaderCode =
#include ".\shader\OBJRender.vs"
;

const std::string gFragmentShaderCode =
#include ".\shader\OBJRender.frag"
;

const std::string gUVPassVertShaderCode =
#include ".\shader\UVPass.vs"
;

const std::string gUVPassFragShaderCode =
#include ".\shader\UVPass.frag"
;

using namespace glm;


OBJRender::OBJRender(int gpuid, void *ctx, RenderType _type, int estimated_face,
	int im_w, int im_h, int tex_width, int tex_height)
{
	m_type = _type;

	tarW = im_w;
	tarH = im_h;

	texW = tex_width;
	texH = tex_height;

	std::string err;

	window = static_cast<GLFWwindow*>(ctx);
	glfwMakeContextCurrent(window);

	glewExperimental = true;        // Needed in core profile
	if (glewInit() != GLEW_OK) {
		throw std::runtime_error("glew init failed");
	}

	GLFWwindow* cur = glfwGetCurrentContext();

	CUDA_CHECK_ERROR(cudaGLSetGLDevice(gpuid));

	const unsigned char* glver = glGetString(GL_VERSION);
	std::cout << "OpenGL Version: " << glver << " ";

	unsigned int glDeviceCount = 16;
	int glDevices[16];

	cudaGLGetDevices(&glDeviceCount, glDevices, glDeviceCount, cudaGLDeviceListAll);

	printf("OpenGL is using CUDA device(s): ");
	for (unsigned int i = 0; i < glDeviceCount; ++i) {
		printf("%s%d", i == 0 ? "" : ", ", glDevices[i]);
	}
	printf("\n");

	CUDA_CHECK_ERROR();

	AllocateBufferCuda(estimated_face);

	programID = LoadShaders("OBJRender.vs", "OBJRender.frag");
	programID_UVPass = LoadUVPassShaders("UVPass.vs", "UVPass.frag");

	RenderTypeID = glGetUniformLocation(programID, "RenderType");
	CameraViewID = glGetUniformLocation(programID, "CurrentViewID");

	MatrixID = glGetUniformLocation(programID, "MVP");
	MVID = glGetUniformLocation(programID, "MV");

	TarWID = glGetUniformLocation(programID, "TarW");
	TarHID = glGetUniformLocation(programID, "TarH");
	TarDID = glGetUniformLocation(programID, "TarD");

	//
	UVRenderTypeID = glGetUniformLocation(programID_UVPass, "RenderType");
	UVCameraViewID = glGetUniformLocation(programID_UVPass, "CurrentViewID");

	UVMatrixID = glGetUniformLocation(programID_UVPass, "MVP");
	UVMVID = glGetUniformLocation(programID_UVPass, "MV");

	UVTarWID = glGetUniformLocation(programID_UVPass, "TarW");
	UVTarHID = glGetUniformLocation(programID_UVPass, "TarH");
	UVTarDID = glGetUniformLocation(programID_UVPass, "TarD");

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
	glEnable(GL_DEPTH_TEST);

	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT);

	glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
	glClampColor(GL_CLAMP_VERTEX_COLOR, GL_FALSE);
	glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);

	ModelViewMatrix = glm::mat4(1);
	ProjectionMatrix = glm::mat4(1);

	// frame buffer for visibility and selected view

	glGenFramebuffers(1, &Framebuffer_ImageSize);
	glGenTextures(1, &renderedTexture);
	glGenTextures(1, &renderedTextureAttached1);

	glGenRenderbuffers(1, &depthrenderbuffer);
	glGenRenderbuffers(1, &renderedNorms);

	glBindFramebuffer(GL_FRAMEBUFFER, Framebuffer_ImageSize);

	glBindTexture(GL_TEXTURE_2D, renderedTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, tarW, tarH, 0, GL_RED_INTEGER, GL_INT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);

	CUDA_SAFE_CALL(cudaGraphicsGLRegisterImage(&m_cudares_vistex, renderedTexture, GL_TEXTURE_2D, cudaGraphicsMapFlagsNone));

	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, tarW, tarH);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	glBindTexture(GL_TEXTURE_2D, renderedTextureAttached1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tarW, tarH, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, renderedTextureAttached1, 0);

	// frame buffer for uv blend

	glGenFramebuffers(1, &Framebuffer_TextureSize);
	glGenTextures(1, &renderedlowfreq);
	glGenTextures(1, &renderedhighfreq);

	glBindFramebuffer(GL_FRAMEBUFFER, Framebuffer_TextureSize);

	glBindTexture(GL_TEXTURE_2D, renderedlowfreq);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texW, texH, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedlowfreq, 0);

	CUDA_SAFE_CALL(cudaGraphicsGLRegisterImage(&m_cudares_lowfreqtex, renderedlowfreq, GL_TEXTURE_2D, cudaGraphicsMapFlagsNone));

	glBindTexture(GL_TEXTURE_2D, renderedhighfreq);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texW, texH, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, renderedhighfreq, 0);

	CUDA_SAFE_CALL(cudaGraphicsGLRegisterImage(&m_cudares_higfreqtex, renderedhighfreq, GL_TEXTURE_2D, cudaGraphicsMapFlagsNone));
}

void OBJRender::AllocateBufferCuda(int estimated_face_num)
{
	glGenTextures(1, &OBJTexture);

	glBindTexture(GL_TEXTURE_2D, OBJTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 0, 0, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

	glGenerateMipmap(GL_TEXTURE_2D);

	m_face_capacity = estimated_face_num;
	m_face_useful = 0;

	glGenVertexArrays(1, &m_VertexArray);
	glGenBuffers(1, &m_vertexbuffers);
	glGenBuffers(1, &m_elementbuffers);
	glGenBuffers(1, &m_normalbuffers);
	glGenBuffers(1, &m_uvs);
	glGenBuffers(1, &m_visbuffer);
	glGenBuffers(1, &m_selvbuffer);
	glGenBuffers(1, &m_vid_buffer);
	glBindVertexArray(m_VertexArray);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffers);
	glBufferData(GL_ARRAY_BUFFER, estimated_face_num * MULT_VERT * sizeof(TYPE_VERT), NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementbuffers);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, estimated_face_num * MULT_FACE * sizeof(TYPE_FACE), NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_vid_buffer);
	glBufferData(GL_ARRAY_BUFFER, estimated_face_num * MULT_FACE * sizeof(TYPE_FACE), NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_normalbuffers);
	glBufferData(GL_ARRAY_BUFFER, estimated_face_num * MULT_NORM * sizeof(TYPE_NORM), NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_uvs);
	glBufferData(GL_ARRAY_BUFFER, estimated_face_num * MULT_TEXC * sizeof(TYPE_TEXC), NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_visbuffer);
	glBufferData(GL_ARRAY_BUFFER, estimated_face_num * MULT_VISB * sizeof(TYPE_VISB), NULL, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_selvbuffer);
	glBufferData(GL_ARRAY_BUFFER, estimated_face_num * MULT_VISB * sizeof(TYPE_VISB), NULL, GL_STATIC_DRAW);

	glGenBuffers(1, &m_ssbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, estimated_face_num * MULT_VISB * sizeof(TYPE_VISB), NULL, GL_DYNAMIC_READ);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, m_ssbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_vert, m_vertexbuffers, cudaGraphicsRegisterFlagsNone));
	CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_norm, m_normalbuffers, cudaGraphicsRegisterFlagsNone));
	CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_uv, m_uvs, cudaGraphicsRegisterFlagsNone));
	CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_elem, m_elementbuffers, cudaGraphicsRegisterFlagsNone));
	CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_vis, m_visbuffer, cudaGraphicsRegisterFlagsNone));
	CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_ssbo, m_ssbo, cudaGraphicsRegisterFlagsNone));
	CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_vid, m_vid_buffer, cudaGraphicsRegisterFlagsNone));
	CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_selv, m_selvbuffer, cudaGraphicsRegisterFlagsNone));
}

void OBJRender::SetType(RenderType type) {

	m_type = type;

}


void OBJRender::MakeContextCurrent()
{
	window_prev = glfwGetCurrentContext();
	glfwMakeContextCurrent(window);
}

void OBJRender::RestorePreviousContext()
{
	glfwMakeContextCurrent(window_prev);
}


void OBJRender::TransferMeshToGL(CuMesh::TriMesh* mesh)
{
	m_face_useful = mesh->FN;

	if (m_face_capacity < mesh->FN_Capacity)
	{
		m_face_capacity = mesh->FN_Capacity;

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_vert));
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffers);
		glBufferData(GL_ARRAY_BUFFER, m_face_capacity * MULT_VERT * sizeof(TYPE_VERT), NULL, GL_STATIC_DRAW);
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_vert, m_vertexbuffers, cudaGraphicsRegisterFlagsNone));

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_elem));
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementbuffers);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_face_capacity * MULT_FACE * sizeof(TYPE_FACE), NULL, GL_STATIC_DRAW);
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_elem, m_elementbuffers, cudaGraphicsRegisterFlagsNone));

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_vid));
		glBindBuffer(GL_ARRAY_BUFFER, m_vid_buffer);
		glBufferData(GL_ARRAY_BUFFER, m_face_capacity * MULT_FACE * sizeof(TYPE_FACE), NULL, GL_STATIC_DRAW);
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_vid, m_vid_buffer, cudaGraphicsRegisterFlagsNone));

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_norm));
		glBindBuffer(GL_ARRAY_BUFFER, m_normalbuffers);
		glBufferData(GL_ARRAY_BUFFER, m_face_capacity * MULT_NORM * sizeof(TYPE_NORM), NULL, GL_STATIC_DRAW);
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_norm, m_normalbuffers, cudaGraphicsRegisterFlagsNone));

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_uv));
		glBindBuffer(GL_ARRAY_BUFFER, m_uvs);
		glBufferData(GL_ARRAY_BUFFER, m_face_capacity * MULT_TEXC * sizeof(TYPE_TEXC), NULL, GL_STATIC_DRAW);
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_uv, m_uvs, cudaGraphicsRegisterFlagsNone));

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_vis));
		glBindBuffer(GL_ARRAY_BUFFER, m_visbuffer);
		glBufferData(GL_ARRAY_BUFFER, m_face_capacity * MULT_VISB * sizeof(TYPE_VISB), NULL, GL_STATIC_DRAW);
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_vis, m_visbuffer, cudaGraphicsRegisterFlagsNone));

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_selv));
		glBindBuffer(GL_ARRAY_BUFFER, m_selvbuffer);
		glBufferData(GL_ARRAY_BUFFER, m_face_capacity * MULT_VISB * sizeof(TYPE_VISB), NULL, GL_STATIC_DRAW);
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_selv, m_selvbuffer, cudaGraphicsRegisterFlagsNone));

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_ssbo));
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssbo);
		glBufferData(GL_SHADER_STORAGE_BUFFER, m_face_capacity * MULT_VISB * sizeof(TYPE_VISB), NULL, GL_DYNAMIC_READ);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, m_ssbo);
		glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(&m_cudares_ssbo, m_ssbo, cudaGraphicsRegisterFlagsNone));
	}

	CuMesh::Device from_dev = mesh->m_dev;

	if (from_dev == CuMesh::Device::CPU)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffers);
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_face_useful * MULT_VERT * sizeof(TYPE_VERT), mesh->Vert(from_dev));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementbuffers);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, m_face_useful * MULT_FACE * sizeof(TYPE_FACE), mesh->Face(from_dev));

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vid_buffer);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, m_face_useful * MULT_FACE * sizeof(TYPE_FACE), mesh->Face(from_dev));

		if (mesh->HasNormal() && m_type == UVBLEND)
		{
			glBindBuffer(GL_ARRAY_BUFFER, m_normalbuffers);
			glBufferSubData(GL_ARRAY_BUFFER, 0, m_face_useful * MULT_NORM * sizeof(TYPE_NORM), mesh->Normal(from_dev));
		}

		if (mesh->HasTexCoord() && m_type == UVBLEND)
		{
			glBindBuffer(GL_ARRAY_BUFFER, m_uvs);
			glBufferSubData(GL_ARRAY_BUFFER, 0, m_face_useful * MULT_TEXC * sizeof(TYPE_TEXC), mesh->TexCoord(from_dev));
		}
	}
	else
	{
		void * p_vert;
		void * p_elem;
		void * p_vid;
		void * p_norm;
		void * p_uv;

		size_t n_verts = 0, n_norms = 0, n_elems = 0, n_vid = 0, n_uvs = 0;

		CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_vert, 0));
		CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer((void **)&p_vert, &n_verts, m_cudares_vert));
		CUDA_SAFE_CALL(cudaMemcpy(p_vert, mesh->Vert(from_dev), m_face_useful * MULT_VERT * sizeof(TYPE_VERT), cudaMemcpyDeviceToDevice));

		/*
		float* t_vert = new float[m_face_useful * MULT_VERT];
		CUDA_SAFE_CALL(cudaMemcpy(t_vert, p_vert, m_face_useful * MULT_VERT * sizeof(TYPE_VERT), cudaMemcpyDeviceToHost));

		for (int i = 0; i < 10; ++i)
			std::cout << "P" << i << ": " << t_vert[i * 3 + 0] << "," << t_vert[i * 3 + 1] << "," << t_vert[i * 3 + 2] << std::endl;
			*/

		CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_elem, 0));
		CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer((void **)&p_elem, &n_elems, m_cudares_elem));
		CUDA_SAFE_CALL(cudaMemcpy(p_elem, mesh->Face(from_dev), m_face_useful * MULT_FACE * sizeof(TYPE_FACE), cudaMemcpyDeviceToDevice));

		/*
		int* t_elem = new int[m_face_useful * MULT_FACE];
		CUDA_SAFE_CALL(cudaMemcpy(t_elem, p_elem, m_face_useful * MULT_FACE * sizeof(TYPE_FACE), cudaMemcpyDeviceToHost));
		*/

		CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_vid, 0));
		CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer((void **)&p_vid, &n_vid, m_cudares_vid));
		CUDA_SAFE_CALL(cudaMemcpy(p_vid, mesh->Face(from_dev), m_face_useful * MULT_FACE * sizeof(TYPE_FACE), cudaMemcpyDeviceToDevice));

		//for (int i = 0; i < 10; ++i)
		//	std::cout << "F" << i << ": " << t_elem[i * 3 + 0] << "," << t_elem[i * 3 + 1] << "," << t_elem[i * 3 + 2] << std::endl;

		if (mesh->HasNormal() && m_type == UVBLEND && USE_NORMAL_WEIGHTING)
		{
			//CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_norm, 0));
			//CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer((void **)&p_norm, &n_norms, m_cudares_norm));
			//CUDA_SAFE_CALL(cudaMemcpy(p_norm, mesh.Normal(from_dev), m_face_useful * MULT_NORM * sizeof(TYPE_NORM), cudaMemcpyDeviceToDevice));
			//cudaGraphicsUnmapResources(1, &m_cudares_norm, 0);
		}

		if (mesh->HasTexCoord() && m_type == UVBLEND)
		{
			CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_uv, 0));
			CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer((void **)&p_uv, &n_uvs, m_cudares_uv));
			CUDA_SAFE_CALL(cudaMemcpy(p_uv, mesh->TexCoord(from_dev), m_face_useful * MULT_TEXC * sizeof(TYPE_TEXC), cudaMemcpyDeviceToDevice));

			//float* t_uv = new float[m_face_useful * MULT_TEXC];
			//CUDA_SAFE_CALL(cudaMemcpy(t_uv, p_uv, m_face_useful * MULT_TEXC * sizeof(TYPE_TEXC), cudaMemcpyDeviceToHost));
			//for (int i = 0; i < 100; ++i)
			//	std::cout << "UV" << i << ": " << t_uv[i * 2 + 0] << "," << t_uv[i * 2 + 1] << std::endl;

			cudaGraphicsUnmapResources(1, &m_cudares_uv, 0);
		}

		cudaGraphicsUnmapResources(1, &m_cudares_vert, 0);
		cudaGraphicsUnmapResources(1, &m_cudares_elem, 0);
		cudaGraphicsUnmapResources(1, &m_cudares_vid, 0);

		CUDA_CHECK_ERROR();
	}
}

void OBJRender::TransferUVCoordinateToGL(CuMesh::TriMesh* mesh)
{
	if (mesh->HasTexCoord() && m_type == UVBLEND)
	{
		CuMesh::Device from_dev = mesh->m_dev;

		if (from_dev == CuMesh::Device::CPU)
		{
			if (mesh->HasTexCoord() && m_type == UVBLEND)
			{
				glBindBuffer(GL_ARRAY_BUFFER, m_uvs);
				glBufferSubData(GL_ARRAY_BUFFER, 0, m_face_useful * MULT_TEXC * sizeof(TYPE_TEXC), mesh->TexCoord(from_dev));
			}
		}
		else
		{
			size_t n_uvs;
			void * p_uv;

			CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_uv, 0));
			CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer((void **)&p_uv, &n_uvs, m_cudares_uv));
			CUDA_SAFE_CALL(cudaMemcpy(p_uv, mesh->TexCoord(from_dev), m_face_useful * MULT_TEXC * sizeof(TYPE_TEXC), cudaMemcpyDeviceToDevice));

			//float* t_uv = new float[m_face_useful * MULT_TEXC];
			//CUDA_SAFE_CALL(cudaMemcpy(t_uv, p_uv, m_face_useful * MULT_TEXC * sizeof(TYPE_TEXC), cudaMemcpyDeviceToHost));
			//for (int i = 0; i < 100; ++i)
			//	std::cout << "UV" << i << ": " << t_uv[i * 2 + 0] << "," << t_uv[i * 2 + 1] << std::endl;

			cudaGraphicsUnmapResources(1, &m_cudares_uv, 0);
		}
	}

	CUDA_CHECK_ERROR();
}

void OBJRender::TransferVisibilityToCuda(void* p_dev_buffer)
{
	void * p_vis;
	size_t n_vis;

	CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_ssbo, 0));
	CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer((void **)&p_vis, &n_vis, m_cudares_ssbo));

	CUDA_SAFE_CALL(cudaMemcpy(p_dev_buffer, p_vis, m_face_useful * MULT_VISB * sizeof(TYPE_VISB), cudaMemcpyDeviceToDevice));

	cudaGraphicsUnmapResources(1, &m_cudares_ssbo, 0);

	CUDA_CHECK_ERROR();
}

void OBJRender::TransferVisibilityToGL(void* p_dev_buffer)
{
	void * p_vis;
	size_t n_vis;

	CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_vis, 0));
	CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer((void **)&p_vis, &n_vis, m_cudares_vis));

	CUDA_SAFE_CALL(cudaMemcpy(p_vis, p_dev_buffer, m_face_useful * MULT_VISB * sizeof(TYPE_VISB), cudaMemcpyDeviceToDevice));

	//int* t_vis = new int[m_face_useful * MULT_VISB];
	//CUDA_SAFE_CALL(cudaMemcpy(t_vis, p_vis, m_face_useful * MULT_VISB * sizeof(TYPE_VISB), cudaMemcpyDeviceToHost));
	//for (int i = 0; i < 1000; ++i)
	//	std::cout << t_vis[i * 3 + 0] <<  t_vis[i * 3 + 1] << t_vis[i * 3 + 2] << std::endl;

	cudaGraphicsUnmapResources(1, &m_cudares_vis, 0);

	CUDA_CHECK_ERROR();
}

void OBJRender::TransferSelectedViewTextureToCUDA(cudaArray** p_cuda_array)
{
	CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_vistex, 0));
	cudaGraphicsSubResourceGetMappedArray(p_cuda_array, m_cudares_vistex, 0, 0);
	//cudaGraphicsUnmapResources(1, &m_cudares_vistex, 0);
	CUDA_CHECK_ERROR();
}

void OBJRender::ReleaseSelectedViewTextureToGL()
{
	cudaGraphicsUnmapResources(1, &m_cudares_vistex, 0);
}

void OBJRender::TransferUVFrequenciesViewTextureToCUDA(cudaArray** p_cuda_array_lowfreq, cudaArray** p_cuda_array_higfreq)
{
	CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_lowfreqtex, 0));
	cudaGraphicsSubResourceGetMappedArray(p_cuda_array_lowfreq, m_cudares_lowfreqtex, 0, 0);

	CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_higfreqtex, 0));
	cudaGraphicsSubResourceGetMappedArray(p_cuda_array_higfreq, m_cudares_higfreqtex, 0, 0);

	CUDA_CHECK_ERROR();
}

void OBJRender::ReleaseUVFrequenciesViewTextureToGL()
{
	cudaGraphicsUnmapResources(1, &m_cudares_lowfreqtex, 0);
	cudaGraphicsUnmapResources(1, &m_cudares_higfreqtex, 0);
}

void OBJRender::TransferSelectedViewTextureToCPU(int& in_tex_size, void** p_cpu_buffer)
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(0);

	glBindTexture(GL_TEXTURE_2D, renderedTexture);

	int W, H;

	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &W);
	glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &H);

	if (in_tex_size < W * H * sizeof(int))
	{
		if (*p_cpu_buffer != NULL) delete[] (*p_cpu_buffer);

		*p_cpu_buffer = new int[W * H];
		in_tex_size = W * H * sizeof(int);
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, GL_INT, *p_cpu_buffer);


	/*void * p_tex;
	size_t n_tex;

	CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_vistex, 0));
	CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer((void **)&p_tex, &n_tex, m_cudares_vistex));

	CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_vistex, 0));

	cudaGraphicsSubResourceGetMappedArray(&array, resource, 0, 0);

	if (in_tex_size < n_tex)
	{
		if (p_dev_buffer != NULL) CUDA_SAFE_CALL(cudaFree(p_dev_buffer));
		CUDA_SAFE_CALL(cudaMalloc(&p_dev_buffer, n_tex));

		in_tex_size = n_tex;
	}

	std::cout << "ntex:" << n_tex << std::endl;

	CUDA_SAFE_CALL(cudaMemcpy(p_dev_buffer, p_tex, n_tex, cudaMemcpyDeviceToDevice));

	cudaGraphicsUnmapResources(1, &m_cudares_vistex, 0);

	CUDA_CHECK_ERROR(); */
}

void OBJRender::TransferSelectedViewsToGL(void* p_dev_buffer)
{
	void * p_vis;
	size_t n_vis;

	CUDA_SAFE_CALL(cudaGraphicsMapResources(1, &m_cudares_selv, 0));
	CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer((void **)&p_vis, &n_vis, m_cudares_selv));

	CUDA_SAFE_CALL(cudaMemcpy(p_vis, p_dev_buffer, m_face_useful * MULT_VISB * sizeof(TYPE_VISB), cudaMemcpyDeviceToDevice));

	cudaGraphicsUnmapResources(1, &m_cudares_selv, 0);

	CUDA_CHECK_ERROR();
}

int OBJRender::RenderVisibility()
{
	glViewport(0, 0, tarW, tarH);

	m_type = VISIBILITY_DEPTH_PASS;

	glUseProgram(programID);
	glm::mat4 Model = glm::mat4(1.0f);  // Changes for each model !

	glm::mat4 MVP = ProjectionMatrix * ModelViewMatrix * Model;
	glm::mat4 MV = ModelViewMatrix * Model;

	glUniform1i(RenderTypeID, m_type);

	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(MVID, 1, GL_FALSE, &MV[0][0]);

	glUniform1f(TarWID, (float)tarW);
	glUniform1f(TarHID, (float)tarH);
	glUniform1f(TarDID, (float)tarD);

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	//glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

	glBindFramebuffer(GL_FRAMEBUFFER, Framebuffer_ImageSize);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glBindVertexArray(m_VertexArray);

	glEnableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glDisableVertexAttribArray(5);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffers);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, m_vid_buffer);
	glVertexAttribIPointer(
		4,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		1,                  // size
		GL_UNSIGNED_INT,    // type
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementbuffers);

	glDrawElements(
		GL_TRIANGLES,      // mode
		m_face_useful * 3, // count
		GL_UNSIGNED_INT,   // type
		(void*)0           // element array buffer offset
	);

	glFlush();

	m_type = VISIBILITY;
	glUniform1i(RenderTypeID, m_type);

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	glDepthFunc(GL_LEQUAL);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	glBindVertexArray(m_VertexArray);

	glEnableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glEnableVertexAttribArray(4);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffers);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, m_vid_buffer);
	glVertexAttribIPointer(
		4,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		1,                  // size
		GL_UNSIGNED_INT,    // type
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glDrawArrays(
		GL_POINTS,      // mode
		0,
		m_face_useful * 3
	); 

	glFinish();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(0);

	return 0;
}


int OBJRender::RenderSelectedView(int vid)
{
	glViewport(0, 0, tarW, tarH);
	glUseProgram(programID);

	m_type = SELECTED_VIEW;

	glDisable(GL_DEPTH_TEST);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glClearColor(0, 0, 0, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, Framebuffer_ImageSize);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 Model = glm::mat4(1.0f);  // Changes for each model !
	glm::mat4 MVP = ProjectionMatrix * ModelViewMatrix * Model; // Remember, matrix multiplication is the other way around
	glm::mat4 MV = ModelViewMatrix * Model; // Remember, matrix multiplication is the other way around

	glUniform1i(RenderTypeID, m_type);
	glUniform1i(CameraViewID, vid);

	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(MVID, 1, GL_FALSE, &MV[0][0]);

	glUniform1f(TarWID, (float)tarW);
	glUniform1f(TarHID, (float)tarH);
	glUniform1f(TarDID, (float)tarD);

	glBindVertexArray(m_VertexArray);

	glEnableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	glEnableVertexAttribArray(5);

	GLenum attach_bufs[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, attach_bufs);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffers);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, m_selvbuffer);
	glVertexAttribIPointer(
		5,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		1,                  // size
		GL_INT,           // type
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementbuffers);

	glDrawElements(
		GL_TRIANGLES,      // mode
		m_face_useful * 3,    // count
		GL_UNSIGNED_INT,   // type
		(void*)0           // element array buffer offset
	);

	glFlush();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(0);

	return 0;
}

int OBJRender::RenderUV(int vid)
{
	glViewport(0, 0, texW, texH);

	glUseProgram(programID_UVPass);

	glEnable(GL_TEXTURE);
	glDisable(GL_DEPTH_TEST);

	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glClearColor(0, 0, 0, 0);

	glDisable(GL_BLEND);

	m_type = UVBLEND;

	glBindFramebuffer(GL_FRAMEBUFFER, Framebuffer_TextureSize);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 Model = glm::mat4(1.0f);
	glm::mat4 MVP = ProjectionMatrix * ModelViewMatrix * Model;
	glm::mat4 MV = ModelViewMatrix * Model;

	glUniform1i(UVRenderTypeID, m_type);
	glUniform1i(UVCameraViewID, vid);

	glUniformMatrix4fv(UVMatrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(UVMVID, 1, GL_FALSE, &MV[0][0]);

	glUniform1f(UVTarWID, (float)tarW);
	glUniform1f(UVTarHID, (float)tarH);
	glUniform1f(UVTarDID, (float)tarD);

	glUniform1i(glGetUniformLocation(programID_UVPass, "IMG"), 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, OBJTexture);

	glBindVertexArray(m_VertexArray);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	glEnableVertexAttribArray(5);

	GLenum attach_bufs[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, attach_bufs);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffers);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, m_uvs);
	glVertexAttribPointer(
		1,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		2,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, m_visbuffer);
	glVertexAttribIPointer(
		2,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		1,                  // size
		GL_INT,           // type
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, m_selvbuffer);
	glVertexAttribIPointer(
		5,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		1,                  // size
		GL_INT,           // type
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementbuffers);

	glDrawElements(
		GL_TRIANGLES,      // mode
		m_face_useful * 3,    // count
		GL_UNSIGNED_INT,   // type
		(void*)0           // element array buffer offset
	);

	glFlush();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(0);

	return 0;
}

OBJRender::~OBJRender()
{
	glDeleteBuffers(1, &m_vertexbuffers);
	glDeleteBuffers(1, &m_normalbuffers);
	glDeleteBuffers(1, &m_uvs);
	glDeleteBuffers(1, &m_elementbuffers);
	glDeleteBuffers(1, &m_visbuffer);
	glDeleteBuffers(1, &m_vid_buffer);
	glDeleteBuffers(1, &m_selvbuffer);

	//if(m_hdc)
	//	ReleaseDC(m_hwnd, m_hdc);

	//if (m_hrc)
	//	wglDeleteContext(m_hrc);

	glfwTerminate();
}

void OBJRender::SetTexture(cv::Mat& vimg) {

	glBindTexture(GL_TEXTURE_2D, OBJTexture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	GLenum plane = (vimg.channels() == 4) ? GL_RGBA : GL_RGB;
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, vimg.cols, vimg.rows, 0, plane, GL_UNSIGNED_BYTE, vimg.data);

}

void OBJRender::SetTexture(void* img, int w, int h, int ch) {

	glBindTexture(GL_TEXTURE_2D, OBJTexture);

	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	if (ch == 3)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, img);
	else if (ch == 4)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, img);

	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
}

void OBJRender::SetVisibility(int n, std::vector<int> _visibility) {

	int numVis = _visibility.size();

	int   * Visibility = NULL;
	Visibility = new int[numVis];

	for (int i = 0; i < _visibility.size(); ++i)
		Visibility[i] = _visibility[i];

	glBindBuffer(GL_ARRAY_BUFFER, m_visbuffer);
	glBufferData(GL_ARRAY_BUFFER, numVis * sizeof(int), Visibility, GL_STATIC_DRAW);

	delete[]Visibility;
}

void OBJRender::SetWindow(int _tarw, int _tarh, int _texw, int _texh) {

	if (tarW != _tarw || tarH != _tarh)
	{
		tarW = _tarw; tarH = _tarh;

		glBindFramebuffer(GL_FRAMEBUFFER, Framebuffer_ImageSize);

		glDeleteTextures(1, &renderedTexture);
		glGenTextures(1, &renderedTexture);

		glBindTexture(GL_TEXTURE_2D, renderedTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, tarW, tarH, 0, GL_RED_INTEGER, GL_INT, 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_vistex));
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterImage(&m_cudares_vistex, renderedTexture, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsNone));

		glDeleteRenderbuffers(1, &depthrenderbuffer);
		glGenRenderbuffers(1, &depthrenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
		//glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, texW, texH);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, tarW, tarH);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

		//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture[i], 0);

		// --------------------------------------------------------------------------------------------
		glDeleteTextures(1, &renderedTextureAttached1);
		glGenTextures(1, &renderedTextureAttached1);

		glBindTexture(GL_TEXTURE_2D, renderedTextureAttached1);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tarW, tarH, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, renderedTextureAttached1, 0);
	}

	if (texW != _texw || texH != _texh)
	{
		texW = _texw; texH = _texh;

		glBindFramebuffer(GL_FRAMEBUFFER, Framebuffer_TextureSize);

		glDeleteTextures(1, &renderedlowfreq);
		glGenTextures(1, &renderedlowfreq);

		glBindTexture(GL_TEXTURE_2D, renderedlowfreq);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texW, texH, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedlowfreq, 0);

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_lowfreqtex));
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterImage(&m_cudares_lowfreqtex, renderedlowfreq, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsNone));

		glDeleteTextures(1, &renderedhighfreq);
		glGenTextures(1, &renderedhighfreq);

		glBindTexture(GL_TEXTURE_2D, renderedhighfreq);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texW, texH, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, renderedhighfreq, 0);

		CUDA_SAFE_CALL(cudaGraphicsUnregisterResource(m_cudares_higfreqtex));
		CUDA_SAFE_CALL(cudaGraphicsGLRegisterImage(&m_cudares_higfreqtex, renderedhighfreq, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsNone));
	}

}


void OBJRender::SetDepthRange(float minz, float maxz) {
	tarD = maxz;
}

void OBJRender::SetMatrices(glm::mat4 _ModelViewMatrix, glm::mat4 _ProjectionMatrix)
{
	ModelViewMatrix = _ModelViewMatrix;
	ProjectionMatrix = _ProjectionMatrix;
}

void OBJRender::GetTextureData(GLuint tex, unsigned char * dst, int type)
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(0);

	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	if (type == UVBLEND)
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, dst);
	else
		glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, GL_INT, dst);
}

GLuint OBJRender::LoadShaders(const char * vertex_file_path, const char * fragment_file_path)
{
	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;

	VertexShaderCode = gVertexShaderCode;

	std::string FragmentShaderCode;

	FragmentShaderCode = gFragmentShaderCode;

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	//printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
	glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
	fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);

	// Compile Fragment Shader
	//printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
	glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
	fprintf(stdout, "    %s\n", &FragmentShaderErrorMessage[0]);

	// Link the program
	//fprintf(stdout, "Linking programn");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);

	glBindFragDataLocation(ProgramID, 0, "zDepth");
	glBindFragDataLocation(ProgramID, 1, "texcolor");

	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> ProgramErrorMessage(max(InfoLogLength, int(1)));
	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	//fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}

GLuint OBJRender::LoadUVPassShaders(const char * vertex_file_path, const char * fragment_file_path)
{
	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;

	VertexShaderCode = gUVPassVertShaderCode;

	std::string FragmentShaderCode;

	FragmentShaderCode = gUVPassFragShaderCode;

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	//printf("Compiling shader : %s\n", vertex_file_path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
	glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
	fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);

	// Compile Fragment Shader
	//printf("Compiling shader : %s\n", fragment_file_path);
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
	glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
	fprintf(stdout, "    %s\n", &FragmentShaderErrorMessage[0]);

	// Link the program
	//fprintf(stdout, "Linking programn");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);

	glBindFragDataLocation(ProgramID, 0, "lowfreq");
	glBindFragDataLocation(ProgramID, 1, "highfreq");

	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> ProgramErrorMessage(max(InfoLogLength, int(1)));
	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	//fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}

void OBJRender::Release() {
}

