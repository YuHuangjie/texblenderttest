R"(

#version 440 core

layout(early_fragment_tests) in;

layout(std430,binding=6) buffer visibleBuffer {
  int visibles[];
};

uniform int RenderType;

out int    zDepth;
out vec4   texcolor;
out vec4   lowfreq;
out vec4   highfreq;

uniform sampler2D IMG;

in  vec2  out_texcoord;
in  float my_depth;
in  float weight;
in  vec3  out_Normal;
in  float out_vis;

flat in int out_vert_id;

void main(){

	if(RenderType == 0)
	{
		int iDepth = floatBitsToInt(my_depth);
		zDepth = iDepth;
	}
	else if(RenderType == 1) // uv path
	{
		float mipmap_lev = textureQueryLod(IMG, out_texcoord).x;

		lowfreq          = textureLod(IMG, out_texcoord, mipmap_lev + 1);

		lowfreq          = vec4(1.0, 0.0, 0.0, 1.0);

		if( out_vis >= 0.5 )
		{
			highfreq   = textureLod(IMG, out_texcoord, mipmap_lev) - lowfreq;
			highfreq   = (highfreq + 1.0)/2.0;
			highfreq.w = 1.0;
		}
		else
			highfreq = vec4(0.0, 0.0, 0.0, 0.0);

		highfreq = vec4(0.0, 1.0, 0.0, 1.0);

	}
	else if(RenderType == 2) //Depth Pass
	{
		//zDepth =  floatBitsToInt(my_depth); //floatBitsToInt(float(out_vert_id));
		zDepth = 1;
	}
	else  if(RenderType == 3) //Visibility Pass
	{
		visibles[out_vert_id] = 1;
	}
	else  if(RenderType == 4) //Selected View
	{
		zDepth = 1;
	}

}

)"