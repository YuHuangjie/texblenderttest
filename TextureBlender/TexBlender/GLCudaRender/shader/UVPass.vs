R"(

#version 440 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 in_TexCoord ;
layout(location = 2) in int  in_Visible ;
layout(location = 3) in vec3 in_Normal ;
layout(location = 4) in uint in_VertID ;
layout(location = 5) in int  in_SelView ;

layout(std430,binding=6) buffer visibleBuffer {
  int visibles[];
};

uniform int RenderType;

uniform int CurrentViewID;

uniform sampler2D DEPTH;

uniform mat4 MVP;
uniform mat4 MV;

uniform float TarW;
uniform float TarH;
uniform float TarD;

out vec2  out_texcoord;
out float my_depth;
out float weight;
out vec3  out_Normal;
out float out_vis;
flat out int out_vert_id;

void main()
{
	my_depth = 0.0;

	out_texcoord = in_TexCoord;

	if(RenderType == 0)
	{
		vec4 v = vec4(vertexPosition_modelspace, 1.0);
  
		gl_Position = MVP * v;

		gl_Position.x = gl_Position.x / gl_Position.z / TarW * 2.0 - 1.0;
		gl_Position.y = gl_Position.y / gl_Position.z / TarH * 2.0 - 1.0;
		gl_Position.z = gl_Position.z / gl_Position.w / TarD;

		gl_Position.w = 1.0;

		vec4 tmp = MV*v;
		tmp     /= tmp.w;

		my_depth = tmp.z;
	}
	else if(RenderType == 1)
	{
		vec4 vt  = MVP * vec4(vertexPosition_modelspace, 1.0);
		vec4 tmp = MV  * vec4(vertexPosition_modelspace, 1.0);
		vec4 cop = inverse(MV)  * vec4(0.0, 0.0, 0.0, 1.0);

		vt.x = vt.x / vt.z / TarW;
		vt.y = vt.y / vt.z / TarH;
		vt.z = vt.z / vt.w / TarD;
		vt.w = 1.0;
		
		gl_Position.x = in_TexCoord.x * 2.0 - 1.0;
		gl_Position.y = 1.0 - in_TexCoord.y * 2.0; 

		out_texcoord.x = vt.x;
		out_texcoord.y = vt.y;

		//float boundary_weight = 1.0 / (1.0 + exp( -100 * (0.425 - max( abs(out_texcoord.x - 0.5), abs(out_texcoord.y - 0.5))) ));

		//out_texcoord.x = (gl_Position.x + 1.0)/2.0;
		//out_texcoord.y = (gl_Position.y + 1.0)/2.0;

		//float zmin = texture(DEPTH, out_texcoord).r;
		
		//tmp     /= tmp.w;
		//my_depth = tmp.z;
		
		if((vt.x < 0.0 || vt.x > 1.0 || vt.y < 0.0  || vt.y > 1.0 || in_Visible <= 0) && (in_SelView != CurrentViewID))
			gl_Position.z = 2.0;
		else
			gl_Position.z = vt.z;
		
		gl_Position.w = 1.0;

		//vec3 viewdir = vec3(cop.x, cop.y, cop.z) - vertexPosition_modelspace;
		//weight = abs(dot(normalize(in_Normal), normalize(viewdir))) * boundary_weight;

		weight = abs(in_Visible) / 65536.0;

		if(in_SelView == CurrentViewID)
			out_vis = 1.0;
		else
			out_vis = 0.0; 
	}
	else if(RenderType == 2) //Depth Pass
	{
		vec4 v = vec4(vertexPosition_modelspace, 1.0);
  
		gl_Position = MVP * v;

		gl_Position.x = gl_Position.x / gl_Position.z / TarW * 2.0 - 1.0;
		gl_Position.y = gl_Position.y / gl_Position.z / TarH * 2.0 - 1.0;
		gl_Position.z = gl_Position.z / gl_Position.w / TarD ;

		gl_Position.w = 1.0;

		vec4 tmp = MV*v;
		tmp     /= tmp.w;
		my_depth = tmp.z;

		out_vert_id = int(in_VertID);
		visibles[out_vert_id] = 0;
	}
	else  if(RenderType == 3) //Visibility Pass
	{
		vec4 v = vec4(vertexPosition_modelspace, 1.0);
  
		gl_Position = MVP * v;

		gl_Position.x = gl_Position.x / gl_Position.z / TarW * 2.0 - 1.0;
		gl_Position.y = gl_Position.y / gl_Position.z / TarH * 2.0 - 1.0;
		gl_Position.z = gl_Position.z / gl_Position.w / TarD ;

		gl_Position.z = gl_Position.z - TarD / 5000.0;

		gl_Position.w = 1.0;

		out_vert_id = int(in_VertID);
	}
	else  if(RenderType == 4) //Selected View
	{
		vec4 v = vec4(vertexPosition_modelspace, 1.0);
  
		gl_Position = MVP * v;

		gl_Position.x = gl_Position.x / gl_Position.z / TarW * 2.0 - 1.0;
		gl_Position.y = gl_Position.y / gl_Position.z / TarH * 2.0 - 1.0;
		gl_Position.z = gl_Position.z / gl_Position.w / TarD ;

		gl_Position.w = 1.0;

		if(in_SelView != CurrentViewID)
		{
			gl_Position.y = -2.0;
			gl_Position.x = -2.0;
		}
	}

}

)"