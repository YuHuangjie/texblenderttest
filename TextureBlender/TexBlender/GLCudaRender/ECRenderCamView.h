#pragma once
#include "Util.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class RenderCamView
{
public:
	RenderCamView() {};
	RenderCamView() restrict(amp);

	void setParameter(TexBlender::Vector3 &_location, TexBlender::Vector3 &_look, TexBlender::Vector3 &_up, float _FovW, float _FovH, int _imgW, int _imgH);

	glm::mat4 GetModelViewMatrix(float lambda = 1);
	glm::mat4 GetProjectionMatrix();

	TexBlender::Vector3 location;
	TexBlender::Vector3 lookat;
	TexBlender::Vector3 up;
	TexBlender::Vector3 K;
	float FovW, FovH;
	int imgW, imgH;
	TexBlender::Vector3 dw, dh;
};

