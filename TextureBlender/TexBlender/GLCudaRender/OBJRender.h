
#pragma once

#include "Util.h"
#include "tinyobj.h"
#include <vector>
#include <string>
#include <thread>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Common/comm.h"

#include <cuda_gl_interop.h>

#include "Mesh/Mesh.h"

class Camera;

class OBJRender
{
public:

	enum RenderType {
		DEPTH = 0,
		UVBLEND = 1,
		VISIBILITY_DEPTH_PASS = 2,
		VISIBILITY = 3,
		SELECTED_VIEW = 4
	};

    static unsigned int m_render_instance;

	RenderType m_type;

	HWND  m_hwnd;
	HDC   m_hdc;
	HGLRC m_hrc;

	int m_face_useful;
	int m_face_capacity;

	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	GLuint m_VertexArray;
	GLuint m_vertexbuffers;
	GLuint m_normalbuffers;
	GLuint m_uvs;
	GLuint m_elementbuffers;
	GLuint m_visbuffer;
	GLuint m_selvbuffer;
	GLuint m_vid_buffer;
	GLuint OBJTexture;
	//GLuint DepthTexture;

	cudaGraphicsResource_t m_cudares_vert;
	cudaGraphicsResource_t m_cudares_norm;
	cudaGraphicsResource_t m_cudares_uv;
	cudaGraphicsResource_t m_cudares_elem;
	cudaGraphicsResource_t m_cudares_vis;
	cudaGraphicsResource_t m_cudares_selv;
	cudaGraphicsResource_t m_cudares_ssbo;
	cudaGraphicsResource_t m_cudares_vid;

	cudaGraphicsResource_t m_cudares_uvtex;
	cudaGraphicsResource_t m_cudares_vistex;

	cudaGraphicsResource_t m_cudares_lowfreqtex;
	cudaGraphicsResource_t m_cudares_higfreqtex;

	GLFWwindow* window;
	GLFWwindow* window_prev;
	GLuint programID;
	GLuint programID_UVPass;

	GLuint RenderTypeID;
	GLuint CameraViewID;
	GLuint MatrixID;
	GLuint MVID;
	GLuint TarWID;
	GLuint TarHID;
	GLuint TarDID;

	GLuint UVRenderTypeID;
	GLuint UVCameraViewID;
	GLuint UVMatrixID;
	GLuint UVMVID;
	GLuint UVTarWID;
	GLuint UVTarHID;
	GLuint UVTarDID;

	glm::mat4 ModelViewMatrix;
	glm::mat4 ProjectionMatrix;

	GLuint Framebuffer_ImageSize;
	GLuint Framebuffer_TextureSize;

	GLuint renderedTexture;
	GLuint renderedNorms;
	GLuint depthrenderbuffer;
	GLuint renderedTextureAttached1;
	
	//GLuint depthrenderbuffer_texsize;
	GLuint renderedlowfreq;
	GLuint renderedhighfreq;

	GLuint depthComponentAsTexture;

	GLuint m_ssbo;
	int* m_visibility_ssbo_data;

	int nScene;
	std::vector<int> nTriangle;
	int tarW, tarH;
	float tarD;

	int texW, texH;
    std::stringstream debug_outputstream;
	//OBJRender(std::string filename, int w = 1500, int h = 1000);
	//OBJRender(CMeshO& cm, RenderType _type, int w = 1500, int h = 1000, int texsize = 2048, cv::Mat* vimg = NULL);

	void AllocateBufferCuda(int estimated_vertexnum);
	void TransferMeshToGL( CuMesh::TriMesh* mesh );
	void TransferUVCoordinateToGL(CuMesh::TriMesh* mesh);

	void TransferVisibilityToCuda(void* p_dev_buffer);
	void TransferVisibilityToGL(void* p_dev_buffer);

	void TransferSelectedViewTextureToCPU(int &in_tex_size, void** p_cpu_buffer);
	void TransferSelectedViewTextureToCUDA(cudaArray** p_cuda_array);
	void ReleaseSelectedViewTextureToGL();

	void TransferUVFrequenciesViewTextureToCUDA(cudaArray** p_cuda_array_lowfreq, cudaArray** p_cuda_array_higfreq);
	void ReleaseUVFrequenciesViewTextureToGL();

	void TransferSelectedViewsToGL(void* p_dev_buffer);

	~OBJRender();

	int RenderUV(int vid);
	int RenderVisibility();
	int RenderSelectedView(int vid);

	void SetWindow(int _tarw, int _tarh, int _texw, int _texh);
	void SetMatrices(glm::mat4 _ModelViewMatrix, glm::mat4 _ProjectionMatrix);

	void GetTextureData(GLuint tex, unsigned char * depthmap, int type);
	void SetTexture(cv::Mat& vimg);
	void SetTexture(void* img, int w, int h, int ch);
	void SetDepthRange(float minz, float maxz);
	
	void SetType(RenderType type);
	void SetVisibility(int n, std::vector<int> _visibility);

	void MakeContextCurrent();
	void RestorePreviousContext();

	static GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);
	static GLuint LoadUVPassShaders(const char * vertex_file_path, const char * fragment_file_path);
	void Release();


	OBJRender(int gpuid, void *ctx, RenderType _type = VISIBILITY_DEPTH_PASS, int estimated_face = 1E5, int im_w = 2560, int im_h = 1920, int tex_width = 1024, int tex_height = 1024);
};


//static OBJRender *objRender;
//static std::thread * glThread;