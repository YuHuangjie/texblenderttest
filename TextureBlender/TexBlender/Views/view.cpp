#include "view.h"

using namespace TexBlender;

View::View(void) {
	static int g_id = 0;
	id = ++g_id;
};

View::View(Image& img, Camera& cam):
	Image(img),
	Camera(cam){

	m_mindepth = 0.0;
	m_maxdepth = 10.0;
};

glm::mat4 View::GetModelViewMatrix() {

	cv::Mat RT;
	cv::hconcat(R, T, RT);

	//std::cout << "Model View Matrix:\n";
	glm::mat4 res(0.0f);
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 4; ++j) {
			res[j][i] = RT.at<double>(i, j);
			//std::cout << res[j][i] << ",";
		}
		//std::cout << "\n";
	}

	res[3][3] = 1.0;
	
	return res;
};

glm::mat4 View::GetProjectionMatrix() {

	// Set translation of the new projection matrix to 0
	// Don't know why

	//std::cout << "Project View Matrix:\n";
	glm::mat4 res(0.0f);
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			res[j][i] = K.at<double>(i, j);
			//std::cout << res[j][i] << ",";
		}
		//std::cout << "\n";
	}

	res[3][3] = 1.0;

	return res;
};

cv::Point2d View::Project(cv::Point3d src) {

	std::vector<cv::Point3d> vsrc(1, src);
	std::vector<cv::Point2d> out;

	cv::projectPoints(vsrc, RVec, T, K, D, out);

	return out[0];
}

cv::Point3d View::TransformToCameraView(cv::Point3d src) {

	cv::Point3d out;

	cv::Mat V = R * cv::Mat(src, false) + T;
	V.copyTo(cv::Mat(out, false));

	return out;
}

std::vector<cv::Point2d> View::Project(std::vector<cv::Point3d> src) {
	std::vector<cv::Point2d> out;

	cv::projectPoints(src, R, T, K, D, out);
	return out;
}

cv::Point3d	View::GetViewPoint() {

	cv::Point3d out;
	cv::Mat temp = - R.t() * T;
	temp.copyTo(cv::Mat(out, false));

	return out;
}


void View::GlobalRTAdjust(cv::Mat Rs, cv::Mat Ts, double Ss) {

	T = Ss * T;
	
	T = R * Ts + T;
	R = R * Rs;

	cv::Rodrigues(R, RVec);
}

cv::Point2d	View::GetZMinMax(cv::Point3d minbox, cv::Point3d maxbox){
	
	double minz = std::numeric_limits<double>::max();
	double maxz = std::numeric_limits<double>::min();

	std::vector<cv::Point3d> vbox{ minbox , maxbox };

	double x, y, z;
	for (int i = 0; i < 2; ++i) {
		x = vbox[i].x;
		for (int j = 0; j < 2; ++j) {
			y = vbox[j].y;
			for (int k = 0; k < 2; ++k){
				z = vbox[k].z;
				cv::Point3d mv = TransformToCameraView(cv::Point3d(x, y, z));

				if (mv.z > maxz)
					maxz = mv.z;

				if (mv.z < minz)
					minz = mv.z;
			}
		}
	}


	m_mindepth = minz;
	m_maxdepth = maxz;

	return cv::Point2d(minz, maxz);
}


void View::Undistortion() {
	cv::Mat tmp;

	if ( Ch() != 4 && !mask.empty() ) {
		std::vector<cv::Mat> chs;

		cv::split(image, chs);
		chs.push_back(mask);

		cv::merge(chs, image);
	}

	cv::undistort(image, tmp, GetK(), GetD() );
	
	image = tmp;
}

void View::SetMask(std::string file) {

	mask = cv::imread(file, CV_LOAD_IMAGE_GRAYSCALE);

	cv::Mat ker = getStructuringElement(cv::MORPH_ELLIPSE,
		cv::Size(2 * 3 + 1, 2 * 3 + 1),
		cv::Point(2, 2));

	cv::erode(mask, mask, ker);
}