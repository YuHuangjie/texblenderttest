#pragma once

#include <memory>

#include "Common/comm.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../../Mesh/Mesh.h"
#include "Image/image.h"
#include "Camera/camera.h"

namespace TexBlender {

	class View_Cuda {

	public:
		View_Cuda(void);
		View_Cuda(int id, int _w, int _h, float* _k, float* _m, float* _k_dev, float* _m_dev, void* img);
		View_Cuda(int id, int _w, int _h, float* _k, float* _m, void* img);
		~View_Cuda(void);

		int m_Width ;
		int m_Height;

		float* K_dev;
		float* M_dev;

		float* K;
		float* M;

		void* m_cpu_image;

		int id;

		int GetId() { return id; }
		int SetId(int _id) { id = _id; }

		int W() { return m_Width; }
		int H() { return m_Height; }

		float m_mindepth;
		float m_maxdepth;

		//std::vector<int> m_visibility;
		//std::vector<int> m_vert_visibility;

		int   m_face_useful;
		int   m_face_capacity;

		void* m_dev_visibility;
		void* m_dev_facearea;
		void* m_dev_faceuv;

		void* m_dev_freq_low;
		void* m_dev_freq_hig;

		//void* m_dev_weight_low;
		void* m_dev_weight_hig;

		//cv::Point2d					GetZMinMax(cv::Point3d minbox, cv::Point3d maxbox);
		//void						GlobalRTAdjust(cv::Mat Rs, cv::Mat Ts, double Ss);
		//void						SetDepth(cv::Mat d) { depth = d; }
		//void						SetVisibilityList(std::vector<int> _vlist) { m_visibility = _vlist; }
		//cv::Mat&					GetDepth() { return depth; }
		//void						SetMask(std::string file);

		//void						Undistortion();

		void UpdateBufferSize(int face_size, int face_capacity);
		void ViewProjection(CuMesh::TriMesh* mesh);

		glm::mat4 GetModelViewMatrix();
		glm::mat4 GetProjectionMatrix();

		cv::Mat		depth;
		cv::Mat		mask;
	};


	typedef std::map< int, View_Cuda* > Views_Cuda;

}

