#ifndef VEC_H
#define VEC_H

#include <opencv2/opencv.hpp>
#include "rect.h"

//template<typename _Tp, int n> class cv::Vec : public cv::Matx<_Tp, n, 1> { ... };

typedef cv::Vec<uchar, 2> Vec2B;
typedef cv::Vec<uchar, 3> Vec3B;
typedef cv::Vec<uchar, 4> Vec4B;

typedef cv::Vec<short, 2> Vec2S;
typedef cv::Vec<short, 3> Vec3S;
typedef cv::Vec<short, 4> Vec4S;

typedef cv::Vec<int, 2> Vec2I;
typedef cv::Vec<int, 3> Vec3I;
typedef cv::Vec<int, 4> Vec4I;

typedef cv::Vec<float, 2> Vec2F;
typedef cv::Vec<float, 3> Vec3F;
typedef cv::Vec<float, 4> Vec4F;
typedef cv::Vec<float, 6> Vec6F;

typedef cv::Vec<double, 2> Vec2D;
typedef cv::Vec<double, 3> Vec3D;
typedef cv::Vec<double, 4> Vec4D;
typedef cv::Vec<double, 6> Vec6D;


typedef TexBlender::Rect<int>    Rect2I;
typedef TexBlender::Rect<float>  Rect2F;
typedef TexBlender::Rect<double> Rect2D;

typedef cv::Mat2f Matrix2F;

//typedef Vec<uchar, 2> Vec2b;
//typedef Vec<uchar, 3> Vec3b;
//typedef Vec<uchar, 4> Vec4b;
//
//typedef Vec<short, 2> Vec2s;
//typedef Vec<short, 3> Vec3s;
//typedef Vec<short, 4> Vec4s;
//
//typedef Vec<int, 2> Vec2i;
//typedef Vec<int, 3> Vec3i;
//typedef Vec<int, 4> Vec4i;
//
//typedef Vec<float, 2> Vec2F;
//typedef Vec<float, 3> Vec3f;
//typedef Vec<float, 4> Vec4f;
//typedef Vec<float, 6> Vec6f;
//
//typedef Vec<double, 2> Vec2d;
//typedef Vec<double, 3> Vec3d;
//typedef Vec<double, 4> Vec4d;
//typedef Vec<double, 6> Vec6d;

#endif

