#ifndef IMAGE_H
#define IMAGE_H

#include <opencv2/opencv.hpp>

namespace TexBlender {

	class Image
	{
		enum PixelType {
			U8 = 0,
			U16 = 1,
			S8 = 2,
			S16 = 3,
			S32 = 4,
			F32 = 5
		};

	public:
		/** Allocating constructor. */
		cv::Mat		image;

		Image(void);
		Image(int w, int h, int ch, PixelType type = U8);
		Image(Image& img);

		std::string file;

		/** Smart pointer image constructor. */
		static Image CreateImage(void);
		/** Allocating smart pointer image constructor. */
		static Image CreateImage(int width, int height, int channels, PixelType type = U8);


		/** Duplicates the image. */
		//Image& Clone(void) const;

		int W() { return  width; }
		int H() { return  height; }
		int Ch() { return  image.channels(); }

		/** Load image file. */
		void SetFile(std::string filepath) { file = filepath; };

		bool Load(std::string filepath);
		bool Load();

		void Release();

		cv::Mat& GetImage() { return image; };

		/** Fills every pixel of the image with the given color. */
		template <typename T> void SetTo(T const var);

		/** Linear indexing of image data. */
		template <typename T> T const& at(int index) const;
		/** Linear indexing of channel data. */
		template <typename T> T const& at(int index, int channel) const;
		/** 2D indexing of image data, more expensive. */
		template <typename T> T const& at(int x, int y, int channel) const;

		/** Linear indexing of image data. */
		template <typename T> T& at(int index);
		/** Linear indexing of channel data. */
		template <typename T> T& at(int index, int channel);
		/** 2D indexing of image data, more expensive. */
		template <typename T> T& at(int x, int y, int channel);

		/** Linear interpolation (more expensive) for a single color channel. */
		template <typename T> T linear_at(float x, float y) const;

		/**
		* Linear interpolation (more expensive) for all color channels.
		* The method generates one value for each color channel and places
		* the result in the buffer provided by 'px'.
		*/
		template <typename T> T& linear_at(float x, float y) const;

		// TODO operators for data access (operators & refptr?)
		template <typename T> T& operator[] (int index);
		template <typename T> T const& operator[] (int index) const;

		template <typename T> T const& operator() (int index) const;
		template <typename T> T const& operator() (int index, int channel) const;
		template <typename T> T const& operator() (int x, int y, int channel) const;
		template <typename T> T& operator() (int index);
		template <typename T> T& operator() (int index, int channel);
		template <typename T> T& operator() (int x, int y, int channel);

	private:
		int width;
		int height;
		int channels;


		PixelType	type;


	};



}



#endif

