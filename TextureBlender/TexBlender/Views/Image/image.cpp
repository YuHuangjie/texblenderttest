#include "image.h"

using namespace TexBlender;

Image::Image(void) {
	width  = 0;
	height = 0;

	image = NULL;
}

Image::Image(Image& img) {
	image = img.GetImage();

	width  = img.W();
	height = img.H();
}

Image::Image(int w, int h, int ch, Image::PixelType type) {

	int cv_type = -1;

	switch(type) {
		case U8:
			cv_type = CV_8U;
			break;
		case U16:
			cv_type = CV_16U;
			break;
		case S8:
			cv_type = CV_8S;
			break;
		case S16:
			cv_type = CV_16S;
			break;
		case S32:
			cv_type = CV_32S;
			break;
		case F32:
			cv_type = CV_32F;
			break;
		default:
			// wrong type
			image = NULL;
			break;
	}

	if ( cv_type == -1 || ch <= 0 || ch > 256 ) {
		std::cerr << "Wrong Image Format!" << std::endl;
		return;
	}

	image = cv::Mat(h, w, CV_MAKETYPE(cv_type, ch));

	width  = w;
	height = h;
}

bool Image::Load() {

	image = cv::imread(file);

	if (!image.data)
	{
		std::cerr << "Could not open or find the image: " << file.c_str() << std::endl;
		return false;
	}

	width    = image.cols;
	height   = image.rows;

	channels = image.channels();

};

bool Image::Load(std::string tfile) {

	file = tfile;

	image = cv::imread(file);

	if (!image.data)                             
	{
		std::cerr << "Could not open or find the image: " << file.c_str() << std::endl;
		return false;
	}

	width = image.cols;
	height = image.rows;

	channels = image.channels();
};


void Image::Release() {
	
	if ( !image.empty() )
		image.release();

	image == NULL;
	
	width  = 0;
	height = 0;

	channels = 0;
};

template <typename T> 
void Image::SetTo(T const var) {

	assert(!image.empty());
	image.setTo(var);

};