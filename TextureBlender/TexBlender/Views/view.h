#ifndef VIEWS_H
#define VIEWS_H

#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Image/image.h"
#include "Camera/camera.h"

namespace TexBlender {

	class View : public Image, public Camera {

	public:
		View(void);
		View(Image& img, Camera& cam);

		int id;

		int GetId() { return id; }
		int SetId(int _id) { id = _id; }

		float m_mindepth;
		float m_maxdepth;

		std::vector<int> m_visibility;
		std::vector<int> m_vert_visibility;

		cv::Point2d					Project(cv::Point3d src);
		std::vector<cv::Point2d>	Project(std::vector<cv::Point3d> src);
		cv::Point3d					TransformToCameraView(cv::Point3d src);
		cv::Point3d					GetViewPoint();

		cv::Point2d					GetZMinMax(cv::Point3d minbox, cv::Point3d maxbox);
		void						GlobalRTAdjust(cv::Mat Rs, cv::Mat Ts, double Ss);
		void						SetDepth(cv::Mat d) { depth = d; }
		void						SetVisibilityList(std::vector<int> _vlist) { m_visibility = _vlist; }
		cv::Mat&					GetDepth() { return depth; }
		void						SetMask(std::string file);

		void						Undistortion();

		glm::mat4 GetModelViewMatrix();
		glm::mat4 GetProjectionMatrix();

		cv::Mat		depth;
		cv::Mat		mask;
	};


typedef std::vector< View > Views;

}

#endif // VIEWS_H
