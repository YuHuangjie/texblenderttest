#ifndef CAMERA_H
#define CAMERA_H
#include "opencv2/opencv.hpp"

namespace TexBlender {

	class Camera {

	public:
		Camera(void);
		Camera(Camera& cam);

		std::string label;

		std::string & Label() { return label; }

		cv::Mat GetD() { return D; }
		cv::Mat GetK() { return K; }

		cv::Mat GetR() { return R; }
		cv::Mat GetT() { return T; }

		cv::Mat GetRVec() { return RVec; }

		bool LoadIntrinsic(std::string file);
		bool LoadExtrinsic(std::string file);

		cv::Mat D;
		cv::Mat K;

		cv::Mat R;
		cv::Mat T;

		cv::Mat RVec;

	private:

	};

}

#endif