#include "camera.h"

using namespace TexBlender;

Camera::Camera() {
}

Camera::Camera(Camera& cam) {
	label = cam.Label();

	K = cam.GetK();
	D = cam.GetD();

	R = cam.GetR();
	T = cam.GetT();

	RVec = cam.GetRVec();
}

bool Camera::LoadIntrinsic(std::string file) {

	cv::FileStorage fs_i(file, cv::FileStorage::READ);
	if (!fs_i.isOpened()) {
		std::cerr << "Can not open the intrinsics file: " << file.c_str() << std::endl;
		return false;
	}
	else {
		fs_i["M"] >> K;
		fs_i["D"] >> D;
		fs_i.release();
	}
}

bool Camera::LoadExtrinsic(std::string file) {

	cv::FileStorage fs_i(file, cv::FileStorage::READ);
	if (!fs_i.isOpened()) {
		std::cerr << "Can not open the extrinsic file: " << file.c_str() << std::endl;
		return false;
	}
	else {
		fs_i["R"] >> R;
		fs_i["T"] >> T;
		fs_i.release();
	}
}



