#include "View_Cuda.h"

#define INT_VISBILITY_MULTPLIER 65536

__device__
inline float3 GetCoP(const float* M)
{
	float3 ans;

	ans.x = -M[0] * M[3] - M[4] * M[7] - M[8] * M[11];
	ans.y = -M[1] * M[3] - M[5] * M[7] - M[9] * M[11];
	ans.z = -M[2] * M[3] - M[6] * M[7] - M[10] * M[11];

	return ans;
}

__device__
inline float3 GetFaceCenter(const float* v1, const float* v2, const float* v3)
{
	float3 ans;

	ans.x = (v1[0] + v2[0] + v3[0]) / 3.0;
	ans.y = (v1[1] + v2[1] + v3[1]) / 3.0;
	ans.z = (v1[2] + v2[2] + v3[2]) / 3.0;

	return ans;
}

__device__
inline float3 Mat4TansVector(const float* M, const float* v)
{
	float3 ans;

	ans.x = M[0] * v[0] + M[1] * v[1] + M[2] * v[2] + M[3];
	ans.y = M[4] * v[0] + M[5] * v[1] + M[6] * v[2] + M[7];
	ans.z = M[8] * v[0] + M[9] * v[1] + M[10] * v[2] + M[11];

	return ans;
}

__device__
inline float2 Project3Vector(const float* K, float3 v)
{
	float2 ans;

	double w = K[6] * v.x + K[7] * v.y + K[8] * v.z;

	if (fabs(w) > 0.000001) {
		ans.x = (K[0] * v.x + K[1] * v.y + K[2] * v.z) / w;
		ans.y = (K[3] * v.x + K[4] * v.y + K[5] * v.z) / w;
	}
	else {
		ans.x = 0;
		ans.y = 0;
	}

	return ans;
}

__device__
inline float CalculateArea(float2 v1, float2 v2, float2 v3)
{
	return (0.5 * fabsf(v1.x * (v2.y - v3.y) + v2.x * (v3.y - v1.y) + v3.x * (v1.y - v2.y)));
}

__global__
void ProjectFace(const int face_num, int* face_vis, const unsigned int* face_ids, const float* vert_pos, const int width, const int height, const float* K, const float* M, float* dst, float* uv)
{
	const int face_id = blockIdx.x * blockDim.x + threadIdx.x;

	if (face_id >= face_num)
		return;

	bool bfacevis = true;

	if (face_vis[face_id * 3] == 0 || face_vis[face_id * 3 + 1] == 0 || face_vis[face_id * 3 + 2] == 0)
	{
		dst[face_id] = -1.0;
		bfacevis = false;
	}

	unsigned int v1 = face_ids[face_id * 3];
	unsigned int v2 = face_ids[face_id * 3 + 1];
	unsigned int v3 = face_ids[face_id * 3 + 2];

	float3 mv1 = Mat4TansVector(M, vert_pos + 3 * v1);
	float3 mv2 = Mat4TansVector(M, vert_pos + 3 * v2);
	float3 mv3 = Mat4TansVector(M, vert_pos + 3 * v3);

	float2 pv1 = Project3Vector(K, mv1);
	float2 pv2 = Project3Vector(K, mv2);
	float2 pv3 = Project3Vector(K, mv3);

	if (bfacevis)
	{
		dst[face_id] = CalculateArea(pv1, pv2, pv3);

		face_vis[face_id * 3] = INT_VISBILITY_MULTPLIER;
		face_vis[face_id * 3 + 1] = INT_VISBILITY_MULTPLIER;
		face_vis[face_id * 3 + 2] = INT_VISBILITY_MULTPLIER;
	}

	uv[face_id * MULT_TEXC + 0] = pv1.x;
	uv[face_id * MULT_TEXC + 1] = pv1.y;

	uv[face_id * MULT_TEXC + 2] = pv2.x;
	uv[face_id * MULT_TEXC + 3] = pv2.y;

	uv[face_id * MULT_TEXC + 4] = pv3.x;
	uv[face_id * MULT_TEXC + 5] = pv3.y;


}

__global__
void ProjectFaceWithNormal(const int face_num, int* face_vis, const unsigned int* face_ids, const float* vert_pos, const float* normals, const int width, const int height, const float* K, const float* M, float* dst, float* uv)
{
	const int face_id = blockIdx.x * blockDim.x + threadIdx.x;

	if (face_id >= face_num)
		return;

	bool bfacevis = true;

	if (face_vis[face_id * 3] == 0 || face_vis[face_id * 3 + 1] == 0 || face_vis[face_id * 3 + 2] == 0)
	{
		dst[face_id] = -1.0;
		bfacevis = false;
	}

	unsigned int v1 = face_ids[face_id * 3];
	unsigned int v2 = face_ids[face_id * 3 + 1];
	unsigned int v3 = face_ids[face_id * 3 + 2];

	float3 mv1 = Mat4TansVector(M, vert_pos + 3 * v1);
	float3 mv2 = Mat4TansVector(M, vert_pos + 3 * v2);
	float3 mv3 = Mat4TansVector(M, vert_pos + 3 * v3);

	float2 pv1 = Project3Vector(K, mv1);
	float2 pv2 = Project3Vector(K, mv2);
	float2 pv3 = Project3Vector(K, mv3);


	float area = CalculateArea(pv1, pv2, pv3);

	// normal weighting

	float3 cop = GetCoP(M);
	float3 fct = GetFaceCenter(vert_pos + 3 * v1, vert_pos + 3 * v2, vert_pos + 3 * v3);

	float3 n1 = make_float3(normals[3 * v1 + 0], normals[3 * v1 + 1], normals[3 * v1 + 2]);
	float3 n2 = make_float3(normals[3 * v2 + 0], normals[3 * v2 + 1], normals[3 * v2 + 2]);
	float3 n3 = make_float3(normals[3 * v3 + 0], normals[3 * v3 + 1], normals[3 * v3 + 2]);

	float3 nml = make_float3((n1.x + n2.x + n3.x) / 3.0, (n1.y + n2.y + n3.y) / 3.0, (n1.z + n2.z + n3.z) / 3.0);
	float3 vdr = make_float3(cop.x - fct.x, cop.y - fct.y, cop.z - fct.z);

	float  vdl = sqrtf(vdr.x * vdr.x + vdr.y * vdr.y + vdr.z * vdr.z);
	float  ndl = sqrtf(nml.x * nml.x + nml.y * nml.y + nml.z * nml.z);

	float wn = fabsf((nml.x * vdr.x + nml.y * vdr.y + nml.z * vdr.z) / (vdl * ndl));

	if (bfacevis)
	{
		dst[face_id] = (area + 1.0)* wn;

		face_vis[face_id * 3] = int(wn * wn * INT_VISBILITY_MULTPLIER);
		face_vis[face_id * 3 + 1] = int(wn * wn * INT_VISBILITY_MULTPLIER);
		face_vis[face_id * 3 + 2] = int(wn * wn * INT_VISBILITY_MULTPLIER);
	}
	else
	{
		face_vis[face_id * 3] = -int(wn * wn * INT_VISBILITY_MULTPLIER);
		face_vis[face_id * 3 + 1] = -int(wn * wn * INT_VISBILITY_MULTPLIER);
		face_vis[face_id * 3 + 2] = -int(wn * wn * INT_VISBILITY_MULTPLIER);
	}

	uv[face_id * MULT_TEXC + 0] = pv1.x;
	uv[face_id * MULT_TEXC + 1] = pv1.y;

	uv[face_id * MULT_TEXC + 2] = pv2.x;
	uv[face_id * MULT_TEXC + 3] = pv2.y;

	uv[face_id * MULT_TEXC + 4] = pv3.x;
	uv[face_id * MULT_TEXC + 5] = pv3.y;
}

void FaceAreaProjection(const int face_num, int* face_vis, const unsigned int* face_ids, const float* vert_pos, const int width, const int height, const float* K, const float* M, float* dst, float* uv)
{
	dim3 block(64, 1);
	dim3 grid((face_num + block.x - 1) / block.x, 1);

	ProjectFace << <grid, block >> > (face_num, face_vis, face_ids, vert_pos, width, height, K, M, dst, uv);

	CUDA_CHECK_ERROR();
}


void FaceAreaProjectionNormalWeighting(const int face_num, int* face_vis, const unsigned int* face_ids, const float* vert_pos, const float* normals, const int width, const int height, const float* K, const float* M, float* dst, float* uv)
{
	dim3 block(64, 1);
	dim3 grid((face_num + block.x - 1) / block.x, 1);

	ProjectFaceWithNormal << <grid, block >> > (face_num, face_vis, face_ids, vert_pos, normals, width, height, K, M, dst, uv);

	CUDA_CHECK_ERROR();
}