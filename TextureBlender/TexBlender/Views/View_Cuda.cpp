#include "View_Cuda.h"

using namespace TexBlender;
using namespace CuMesh;

void FaceAreaProjection(const int face_num, 
	int* face_vis, 
	const unsigned int* face_ids, 
	const float* vert_pos, 
	const int width, 
	const int height, 
	const float* K, 
	const float* M, 
	float* dst, 
	float* uv);

void FaceAreaProjectionNormalWeighting(const int face_num, 
	int* face_vis, 
	const unsigned int* face_ids, 
	const float* vert_pos, 
	const float* normals,
	const int width, 
	const int height, 
	const float* K, 
	const float* M, 
	float* dst, 
	float* uv);

View_Cuda::View_Cuda(void) 
{
	m_face_useful = 0;
	m_face_capacity = 0;

	m_dev_visibility = NULL;
	m_dev_facearea = NULL;
	m_dev_faceuv = NULL;

	m_dev_freq_low = NULL;
	m_dev_freq_hig = NULL;

	m_dev_weight_hig = NULL;
}

View_Cuda::~View_Cuda(void)
{
	m_face_useful = 0;
	m_face_capacity = 0;

	if (K_dev != NULL) CUDA_SAFE_CALL(cudaFree(K_dev));
	if (M_dev != NULL) CUDA_SAFE_CALL(cudaFree(M_dev));

	if (m_dev_visibility != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_visibility));
	if (m_dev_facearea != NULL)	  CUDA_SAFE_CALL(cudaFree(m_dev_facearea));
	if (m_dev_faceuv != NULL)     CUDA_SAFE_CALL(cudaFree(m_dev_faceuv));
	if (m_dev_freq_low != NULL)   CUDA_SAFE_CALL(cudaFree(m_dev_freq_low));
	if (m_dev_freq_hig != NULL)   CUDA_SAFE_CALL(cudaFree(m_dev_freq_hig));
	if (m_dev_weight_hig != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_weight_hig));
}

View_Cuda::View_Cuda(int id, int _w, int _h, float* _k, float* _m, float* _k_dev, float* _m_dev, void* img)
{
	this->id = id;
	
	m_Width  = _w;
	m_Height = _h;

	K = _k;
	M = _m;

	K_dev = _k_dev;
	M_dev = _m_dev;

	m_mindepth = 0.5;
	m_maxdepth = 6.0;

	m_face_useful = 0;
	m_face_capacity = 0;

	m_dev_visibility = NULL;
	m_dev_facearea = NULL;
	m_dev_faceuv = NULL;

	m_dev_freq_low = NULL;
	m_dev_freq_hig = NULL;

	m_dev_weight_hig = NULL;

	m_cpu_image = img;
}

View_Cuda::View_Cuda(int id, int _w, int _h, float* _k, float* _m, void* img)
{
	this->id = id;

	m_Width = _w;
	m_Height = _h;

	K = _k;
	M = _m;

	m_mindepth = 0.5;
	m_maxdepth = 6.0;

	m_face_useful = 0;
	m_face_capacity = 0;

	m_dev_visibility = NULL;
	m_dev_facearea = NULL;
	m_dev_faceuv = NULL;

	m_dev_freq_low = NULL;
	m_dev_freq_hig = NULL;

	m_dev_weight_hig = NULL;

	m_cpu_image = img;

	CUDA_SAFE_CALL(cudaMalloc(&K_dev, 9 * sizeof(float)));
	CUDA_SAFE_CALL(cudaMalloc(&M_dev, 16 * sizeof(float)));

	CUDA_SAFE_CALL(cudaMemcpy(K_dev, K, 9 * sizeof(float), cudaMemcpyHostToDevice));
	CUDA_SAFE_CALL(cudaMemcpy(M_dev, M, 16 * sizeof(float), cudaMemcpyHostToDevice));
}

glm::mat4 View_Cuda::GetModelViewMatrix(void) 
{
	//std::cout << "Model View Matrix:\n";
	glm::mat4 res(0.0f);
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			res[j][i] = M[i * 4 + j];
			//std::cout << res[j][i] << ",";
		}
		//std::cout << "\n";
	}

	res[3][3] = 1.0;

	return res;

}

glm::mat4 View_Cuda::GetProjectionMatrix(void)
{
	// Set translation of the new projection matrix to 0
	// Don't know why

	//std::cout << "Project View Matrix:\n";
	glm::mat4 res(0.0f);
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j) {
			res[j][i] = K[i * 3 + j];
			//std::cout << res[j][i] << ",";
		}
		//std::cout << "\n";
	}

	res[3][3] = 1.0;

	return res;
}

void View_Cuda::UpdateBufferSize(int face_size, int face_capacity)
{
	if (m_face_capacity < face_capacity)
	{
		m_face_capacity = face_capacity;

		if (m_dev_visibility != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_visibility));

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_visibility, m_face_capacity * MULT_VISB * sizeof(TYPE_VISB))
		);

		if (m_dev_facearea != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_facearea));

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_facearea, m_face_capacity * sizeof(float))
		);

		if (m_dev_faceuv != NULL) CUDA_SAFE_CALL(cudaFree(m_dev_faceuv));

		CUDA_SAFE_CALL(
			cudaMalloc(&m_dev_faceuv, m_face_capacity * MULT_TEXC * sizeof(TYPE_TEXC))
		);
	}

	m_face_useful = face_size;
}

void View_Cuda::ViewProjection(TriMesh* mesh)
{
	if (mesh->HasNormal() && USE_NORMAL_WEIGHTING)
	{
		FaceAreaProjectionNormalWeighting(mesh->FN,
			(int*)m_dev_visibility,
			(unsigned int*)mesh->Face(Device::GPU),
			(float*)mesh->Vert(Device::GPU),
			(float*)mesh->Normal(Device::GPU),
			m_Width,
			m_Height,
			K_dev,
			M_dev,
			(float*)m_dev_facearea,
			(float*)m_dev_faceuv);
	}
	else
	{
		FaceAreaProjection(mesh->FN,
			(int*)m_dev_visibility,
			(unsigned int*)mesh->Face(Device::GPU),
			(float*)mesh->Vert(Device::GPU),
			m_Width,
			m_Height,
			K_dev,
			M_dev,
			(float*)m_dev_facearea,
			(float*)m_dev_faceuv);
	}
}
