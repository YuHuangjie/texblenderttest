#pragma once

#include "opencv2/opencv.hpp"
#include "GLCudaRender/OBJRender.h"

#include "Views/View_Cuda.h"
#include "../Common/MeshDef.h"
#include "Atlas/TexAtlas.h"
#include "Manifold/TexManifold.h"

#include "../Common/comm.h"

namespace TexBlender {

	class TexBlender_RealTime {

	public:
		
		int m_cuda_devID;

		bool m_init_done;

		OBJRender	*m_render;
		CuMesh::TriMesh* m_mesh;
		TexAtlas     m_atlas;
		TexManifold  m_manifold;
		Views_Cuda	 m_view_cuda;

		bool InitRender(void *ctx, OBJRender::RenderType type, int estimated_face = 1E5, int im_w = 640, int im_h = 480, int tex_width = 1024, int tex_height = 1024);
		bool AppendViews(int id, int w, int h, float* k, float* m, float* k_dev, float* m_dev, void* img);
		bool AppendViews(int id, int w, int h, float* k, float* m, void* img);
		void SetMeshAndTexRGB(CuMesh::TriMesh *mesh, std::map<int, void*>& map_texrgb);

		//bool UpdateMesh(ITMLib::ITMMesh *mesh, int frome_gpuid);
		void Debug_VisibilityCheck(View_Cuda* vw);
		void Debug_VisRenderPassCheck(View_Cuda* vw);
		void Debug_PureUVCheck();
		void Debug_SelectedViewCheck();
		void Debug_OriginalUVCheck();

		bool BuildTexture(double packing_loose);

		TexBlender_RealTime() {
			m_cuda_devID = 0; 
			m_init_done = false;
		}

		~TexBlender_RealTime();
	};

}
