#ifndef TEXTURE_BLENDER_H
#define TEXTURE_BLENDER_H

#include <memory>

class TextureBlenderImpl;
class Stream;

class __declspec(dllexport) TextureBlender
{
public:
	TextureBlender();
	/* Construct empty texture blender with a fixed number of cameras */
	explicit TextureBlender(const size_t NCAM);
	~TextureBlender();

	/* Is texture blender ready to run? */
	bool IsConfigured() const;
	/* Get size of output texture (in bytes)*/
	size_t GetTextureSize() const;

	/* Set camera resolution and parameters. *id* should range from 0 to 
	 * NCAM - 1. */
	bool SetCamera(const size_t id, const int32_t w, const int32_t h,
		const float intrin[9], const float extrin[16]);

	/* Inform TextureBlender that all cameras are properly set. It will
	 * start a sub thread for texturing processing.
	 */
	bool HaveSetCameras();

	/* Freeze current thread until there is space to perform next frame 
	 * texturing. It has to be called before ANY Update*() functions.
	 */
	void WaitFree();

	/* Update rgb images.  *id* should range from 0 to NCAM - 1. */
	bool UpdateImage(const size_t id, const uint8_t *const img,
		const size_t szImg);

	/* Update mesh. Vertices are packed such that 12 floats form a triangle. 
	 * *GPU* indicates whether *v* is in device.
	 */
	bool UpdateMesh(const float *v, const size_t szV, bool GPU);

	/* Perform texturing in sub thread. */
	void Run();

	/* TextureBlender can write to the stream every time when a texture is 
	 * built 
	 */
	void RegisterStream(std::weak_ptr<Stream>);

private:
	std::shared_ptr<TextureBlenderImpl> _pImpl;
};

#endif /* TEXTURE_BLENDER_H */
