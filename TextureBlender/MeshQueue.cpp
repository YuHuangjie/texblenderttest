#include "MeshQueue.h"
#include "Mesh/Mesh.h"


MeshQueue::MeshQueue(int dev_id, int size, const size_t nCams, float voxelsize)
{
	fix_size = size;
	gpu_id = dev_id;

	frame_process = 0;
	frame_texture = 0;

	mesh_queue.resize(fix_size);
	teximage_queue.resize(fix_size);

	for (int i = 0; i < fix_size; ++i)
	{
		mesh_queue[i] = new CuMesh::TriMesh();
		mesh_queue[i]->m_gpuid = dev_id;
		mesh_queue[i]->m_voxel_param.voxel_scale = voxelsize;

		for (size_t j = 0; j < nCams; ++j)
			teximage_queue[i][j] = NULL;
	}
}

MeshQueue::~MeshQueue() {

	for (int i = 0; i < fix_size; ++i)
	{
		delete mesh_queue[i];

		for (auto& teximage : teximage_queue[i]) {
			if (teximage.second != NULL)
			{
				delete teximage.second;
				teximage.second = NULL;
			}
		}
	}
}

bool MeshQueue::HasMeshToTexture()
{
	return (frame_process > frame_texture);
}

bool MeshQueue::HasSpaceToProcess()
{
	return ((frame_process % fix_size != frame_texture % fix_size) || frame_texture == frame_process);
}

CuMesh::TriMesh* MeshQueue::MeshToProcess()
{
	return mesh_queue[frame_process % fix_size];
}

CuMesh::TriMesh* MeshQueue::MeshToTexture()
{
	return mesh_queue[frame_texture % fix_size];
}

std::map< int, void* >& MeshQueue::TexImagesToProcess()
{
	return teximage_queue[frame_process % fix_size];
}

std::map< int, void* >& MeshQueue::TexImagesToTexture()
{
	return teximage_queue[frame_texture % fix_size];
}

void MeshQueue::AllocateTexImageToProcess(int camid, int size)
{
	teximage_queue[frame_process % fix_size][camid] = new unsigned char[size];
}

void MeshQueue::FinishProcess()
{
	++frame_process;
}

void MeshQueue::FinishTexture()
{
	++frame_texture;
}