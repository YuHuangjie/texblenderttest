#ifndef VISUALIZER_H
#define VISUALIZER_H

#include <cstdint>
#include <memory>

class VisualizerImpl;

class __declspec(dllexport) Visualizer
{
public:
	Visualizer(void *ctx);
	~Visualizer();

	bool UpdateMesh(const float *v, const size_t szV,
		const float *vt, const size_t szVt);
	bool UpdateTexture(const uint8_t *tex, const uint32_t w,
		const uint32_t h);

	bool Render();

	bool SetModelMatrix(const float R[9], const float modelCenter[3]);
	void SetScale(float scale);

private:
	std::shared_ptr<VisualizerImpl> _pImpl;
};

#endif /* VISUALIZER_H */
