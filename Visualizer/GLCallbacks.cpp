#include <GLFW/glfw3.h>
#include <memory>
#include <mutex>

#include "GLCallbacks.h"
#include "ArcBall.h"

static std::shared_ptr<ArcBallT> pArcball;

static Matrix3fT sRot;
static std::mutex sMtxRot;

static float scale = 1.f;

static Point2fT GetMouse(GLFWwindow *wd)
{
	double x, y;
	Point2fT mouse;

	glfwGetCursorPos(wd, &x, &y);
	mouse.s.X = x;
	mouse.s.Y = y;
	return mouse;
}

void SetArcball(float w, float h)
{
	pArcball.reset(new ArcBallT(w, h));
	sRot = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };
}

void MouseCallback(GLFWwindow * wd, int btn, int act, int mod)
{
	Point2fT mouse = GetMouse(wd);

	if (btn == GLFW_MOUSE_BUTTON_LEFT) {
		if (act == GLFW_PRESS) {
			if (!pArcball->isDragging) {
				pArcball->isDragging = true;
				pArcball->click(&mouse);
			}
		}
		else if (act == GLFW_RELEASE) {
			pArcball->isDragging = false;
			std::lock_guard<std::mutex> lck(sMtxRot);
			sRot = Matrix3fT({ 1, 0, 0, 0, 1, 0, 0, 0, 1 });
		}
	}
}

void CursorCallback(GLFWwindow * wd, double x, double y)
{
	Point2fT mouse = GetMouse(wd);

	if (pArcball->isDragging) {
		Quat4fT quat;

		pArcball->drag(&mouse, &quat);

		{
			std::lock_guard<std::mutex> lck(sMtxRot);
			Matrix3fSetRotationFromQuat4f(&sRot, &quat);
		}

		pArcball->click(&mouse);
	}
}

void ScrollCallback(GLFWwindow *window, double xpos, double ypos)
{
	scale += ypos * 0.1f;
}

void GetArcRotation(float M[9])
{
	std::lock_guard<std::mutex> lck(sMtxRot);
	std::memcpy(M, sRot.M, sizeof(sRot.M));
}

float GetScale()
{
	return scale;
}
