#pragma once

#include <sstream>
#include <stdexcept>
#include <cuda_runtime.h>

#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 1024

#define MAX_VERTEX 3000000

#define CUDA_SAFE_CALL(err) _CUDA_SAFE_CALL(err, __FILE__, __LINE__)
#define CUDA_CHECK_ERROR()    __cudaCheckError( __FILE__, __LINE__ )

inline void _CUDA_SAFE_CALL(cudaError err, const char *file, const int line)
{
	if (cudaSuccess != err) {
		std::ostringstream oss;
		oss << file << "(" << line << ")" << " : cudaSafeCall() Runtime API error : "
			<< cudaGetErrorString(err) << std::endl;
		throw std::runtime_error(oss.str());
	}
};

inline void __cudaCheckError(const char *file, const int line)
{
	cudaError err = cudaGetLastError();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "cudaCheckError() failed at %s:%i : %s\n",
			file, line, cudaGetErrorString(err));
		//exit(-1);
	}

	// More careful checking. However, this will affect performance.
	// Comment away if needed.
	err = cudaDeviceSynchronize();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "cudaCheckError() with sync failed at %s:%i : %s\n",
			file, line, cudaGetErrorString(err));
		//exit(-1);
	}
};
