#include "Visualizer.h"
#include "VisualizerImpl.h"

Visualizer::Visualizer(void *ctx)
	: _pImpl(new VisualizerImpl(ctx))
{
}

Visualizer::~Visualizer()
{
	_pImpl = nullptr;
}

bool Visualizer::UpdateMesh(const float * v, const size_t szV, 
	const float * vt, const size_t szVt)
{
	return _pImpl->UpdateMesh(v, szV, vt, szVt);
}

bool Visualizer::UpdateTexture(const uint8_t * tex, 
	const uint32_t w, const uint32_t h)
{
	return _pImpl->UpdateTexture(tex, w, h);
}

bool Visualizer::Render()
{
	return _pImpl->Render();
}

bool Visualizer::SetModelMatrix(const float R[9], const float modelCenter[3])
{
	return _pImpl->SetModelMatrix(R, modelCenter);
}

void Visualizer::SetScale(float scale)
{
	_pImpl->SetScale(scale);
}
