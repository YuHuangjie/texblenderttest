#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

#include "VisualizerImpl.h"
#include "comm.h"
#include "camera/Camera.hpp"

VisualizerImpl::VisualizerImpl(void *ctx)
	: _wd(static_cast<GLFWwindow*>(ctx)),
	_wdPrev(nullptr),
	_VPLocation(-1),
	_model(1.f),
	_wld2Cam(1.f),
	_proj(1.f),
	_programID(-1),
	_nVerts(0),
	_VAO(-1),
	_VBO(-1),
	_texCBO(-1),
	_PBO(-1),
	_texture(-1),
	_cudaVBO(nullptr),
	_cudaTexCBO(nullptr),
	_cudaPBO(nullptr),
	_near(0.1f),
	_far(1000.f),
	_aspect(1.f),
	_fov(45.0f)
{
	/* Assume GL Context is created */
	glfwMakeContextCurrent(_wd);

	glewExperimental = true;
	if (glewInit() != GLEW_OK) {
		throw std::runtime_error("glew init failed");
	}

	/* shader program init */
	_programID = LoadShaders();
	_VPLocation = glGetUniformLocation(_programID, "VP");

	/* view/proj matrices */
	int width, height;
	glfwGetWindowSize(_wd, &width, &height);
	_aspect = static_cast<float>(width) / height;
	_wld2Cam = glm::lookAt(glm::vec3(0, 0, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	_proj = glm::perspective<float>(glm::radians(_fov), _aspect, _near, _far);

	/* Create vertex buffer and register cuda resource with it */
	glGenVertexArrays(1, &_VAO);
	glGenBuffers(1, &_VBO);
	glGenBuffers(1, &_texCBO);

	glBindVertexArray(_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, _VBO);
	glBufferData(GL_ARRAY_BUFFER, MAX_VERTEX * sizeof(float) * 3, 0, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, _texCBO);
	glBufferData(GL_ARRAY_BUFFER, MAX_VERTEX * sizeof(float) * 2, 0, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);

	glBindVertexArray(0);

	CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(
		&_cudaVBO, _VBO, cudaGraphicsMapFlagsWriteDiscard));
	CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(
		&_cudaTexCBO, _texCBO, cudaGraphicsMapFlagsWriteDiscard));
}

VisualizerImpl::~VisualizerImpl()
{
	MakeContextCurrent();
	glDeleteProgram(_programID);
	glDeleteVertexArrays(1, &_VAO);
	glDeleteBuffers(1, &_VBO);
	glDeleteBuffers(1, &_texCBO);
	glDeleteBuffers(1, &_PBO);
	glDeleteTextures(1, &_texture);
	RestorePreviousContext();
}

void VisualizerImpl::MakeContextCurrent()
{
	_wdPrev = glfwGetCurrentContext();
	glfwMakeContextCurrent(_wd);
}

void VisualizerImpl::RestorePreviousContext()
{
	glfwMakeContextCurrent(_wdPrev);
}

bool VisualizerImpl::UpdateMesh(const float * v, const size_t szV, const float * vt, const size_t szVt)
{
	MakeContextCurrent();

	size_t num_bytes = 0;
	float* cuda_data = nullptr; 
	
	if (v) {
		// map vertex positions to opengl
		CUDA_SAFE_CALL(cudaGraphicsMapResources(
			1, &_cudaVBO, 0));
		CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer(
			(void **)&cuda_data, &num_bytes, _cudaVBO));
		CUDA_SAFE_CALL(cudaMemcpy(
			cuda_data, v, szV, cudaMemcpyDeviceToDevice));
		CUDA_SAFE_CALL(cudaGraphicsUnmapResources(1, &_cudaVBO, 0));

		_nVerts = szV / (sizeof(float) * 3);
	}

	if (vt) {
		// map texture coordinates to opengl
		CUDA_SAFE_CALL(cudaGraphicsMapResources(
			1, &_cudaTexCBO, 0));
		CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer(
			(void **)&cuda_data, &num_bytes, _cudaTexCBO));
		CUDA_SAFE_CALL(cudaMemcpy(
			cuda_data, vt, szVt, cudaMemcpyDeviceToDevice));
		CUDA_SAFE_CALL(cudaGraphicsUnmapResources(
			1, &_cudaTexCBO, 0));
	}

	RestorePreviousContext();
	return true;
}

bool VisualizerImpl::UpdateTexture(const uint8_t * tex, const uint32_t w, const uint32_t h)
{
	MakeContextCurrent();

	size_t num_bytes = 0;
	float* cuda_data = nullptr;

	if (tex) {
		if (_PBO == -1) {
			/* Create texture and register cuda with it */
			glGenBuffers(1, &_PBO);
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER, _PBO);
			glBufferData(GL_PIXEL_UNPACK_BUFFER, w * h * 3, NULL, GL_DYNAMIC_COPY);

			glGenTextures(1, &_texture);
			glBindTexture(GL_TEXTURE_2D, _texture);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			CUDA_SAFE_CALL(cudaGraphicsGLRegisterBuffer(
				&_cudaPBO, _PBO, cudaGraphicsMapFlagsWriteDiscard));
		}

		/* Copy texture data */
		CUDA_SAFE_CALL(cudaGraphicsMapResources(
			1, &_cudaPBO, 0));
		CUDA_SAFE_CALL(cudaGraphicsResourceGetMappedPointer(
			(void **)&cuda_data, &num_bytes, _cudaPBO));
		CUDA_SAFE_CALL(cudaMemcpy(
			cuda_data, tex, w*h*3, cudaMemcpyDeviceToDevice));
		CUDA_SAFE_CALL(cudaGraphicsUnmapResources(
			1, &_cudaPBO, 0));

		glBindBuffer(GL_PIXEL_UNPACK_BUFFER, _PBO);
		glBindTexture(GL_TEXTURE_2D, _texture);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h,
			GL_RGB, GL_UNSIGNED_BYTE, NULL);	}

	RestorePreviousContext();
	return true;
}

bool VisualizerImpl::Render()
{
	MakeContextCurrent();
	// prepare rendering state
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glClearColor(0.f, 0.f, 0.f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(_programID);

	// send MVP matrix
	glm::mat4 MVP = _proj * _wld2Cam * _model;
	glUniformMatrix4fv(_VPLocation, 1, GL_FALSE, &MVP[0][0]);

	// perform rendering
	glBindVertexArray(_VAO);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, _texture);
	glDrawArrays(GL_TRIANGLES, 0, _nVerts);

	glBindVertexArray(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(0);

	// 
	CUDA_SAFE_CALL(cudaStreamSynchronize(0));

	RestorePreviousContext();
	return true;
}

bool VisualizerImpl::SetModelMatrix(const float R[9], const float modelCenter[3])
{
	glm::mat3 rot(glm::vec3(R[0], R[1], R[2]),
		glm::vec3(R[3], R[4], R[5]),
		glm::vec3(R[6], R[7], R[8]));
	glm::vec3 trans(modelCenter[0], modelCenter[1], modelCenter[2]);
	glm::mat3 newRot = rot * glm::mat3(_model);

	_model = glm::mat4(newRot);
	_model[3] = glm::vec4(-newRot * trans, 1.f);
	return true;
}

void VisualizerImpl::SetScale(float scale)
{
	_fov = 45.f / scale;
	_proj = glm::perspective<float>(glm::radians(_fov), _aspect, _near, _far);
}

GLuint VisualizerImpl::LoadShaders(const char * vertex_file_path, const char * fragment_file_path)
{
	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;

	if (vertex_file_path) {
		std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
		if (VertexShaderStream.is_open())
		{
			std::string Line = "";
			while (getline(VertexShaderStream, Line))
				VertexShaderCode += "\n" + Line;
			VertexShaderStream.close();
		}
		else
		{
			printf("FILE NOT OPEN\n");
		}
	}
	else {
		VertexShaderCode = "#version 330 core \n"
			"layout(location = 0) in vec3 v;  \n"
			"layout(location = 1) in vec2 vt; \n"
			"uniform mat4 VP;                \n"
			"out vec2 fVt;                    \n"
			"void main()\n"
			"{\n"
			"	gl_Position = VP * vec4(v.xyz, 1.0);\n"
			"   fVt = vt;						     \n"
			"}";
	}


	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	if (fragment_file_path) {
		std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
		if (FragmentShaderStream.is_open()) {
			std::string Line = "";
			while (getline(FragmentShaderStream, Line))
				FragmentShaderCode += "\n" + Line;
			FragmentShaderStream.close();
		}
	}
	else {
		FragmentShaderCode = "#version 330 core\n"
			"out vec4 color;                   \n"
			"in  vec2 fVt;                     \n"
			"uniform sampler2D IMG;            \n"
			"void main() {                     \n"
			"	color = texture(IMG, vec2(fVt.x, 1-fVt.y));     \n"
			"}";
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	//printf("Compiling shader\n");
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
	glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
	fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);

	// Compile Fragment Shader
	//printf("Compiling shader \n");
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
	glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
	fprintf(stdout, "%s\n", &FragmentShaderErrorMessage[0]);

	// Link the program
	//fprintf(stdout, "Linking programn");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> ProgramErrorMessage(std::max(InfoLogLength, int(1)));
	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}