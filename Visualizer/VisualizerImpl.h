#ifndef VISUALIZER_IMPL_H
#define VISUALIZER_IMPL_H

#include <cstdint>
#include <GL/glew.h>
#include <cuda_gl_interop.h>
#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

class VisualizerImpl
{
public:
	VisualizerImpl(void *ctx);
	~VisualizerImpl();

	bool UpdateMesh(const float *v, const size_t szV,
		const float *vt, const size_t szVt);
	bool UpdateTexture(const uint8_t *tex, const uint32_t w,
		const uint32_t h);

	bool Render();

	bool SetModelMatrix(const float R[9], const float modelCenter[3]);
	void SetScale(float scale);

private:
	GLuint LoadShaders(const char * vs = nullptr, const char * fs = nullptr);

	void MakeContextCurrent();
	void RestorePreviousContext();

private:
	GLFWwindow *_wd;
	GLFWwindow *_wdPrev;

	GLuint _VPLocation;
	glm::mat4 _model;
	glm::mat4 _wld2Cam;
	glm::mat4 _proj;

	GLuint _programID;		/* shader program */

	size_t _nVerts;
	GLuint _VAO;
	GLuint _VBO;
	GLuint _texCBO;
	GLuint _PBO;
	GLuint _texture;
	struct cudaGraphicsResource* _cudaVBO;
	struct cudaGraphicsResource* _cudaTexCBO;
	struct cudaGraphicsResource* _cudaPBO;;

	const float _near, _far;
	float _aspect;
	float _fov;
};

#endif /* VISUALIZER_IMPL_H */
