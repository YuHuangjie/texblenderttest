#ifndef GLCALLBACKS_H
#define GLCALLBACKS_H

struct GLFWwindow; 

#define EXPORT __declspec(dllexport)

void EXPORT SetArcball(float w, float h);
void EXPORT MouseCallback(GLFWwindow *wd, int btn, int act, int mod);
void EXPORT CursorCallback(GLFWwindow *wd, double x, double y);
void EXPORT ScrollCallback(GLFWwindow *window, double x, double y);

void EXPORT GetArcRotation(float M[9]);
float EXPORT GetScale();

#endif /* GLCALLBACKS_H */
