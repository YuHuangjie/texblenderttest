#ifndef COMMON_TYPE_H
#define COMMON_TYPE_H

#include <cstdint>

struct Intrinsic
{
	int32_t width;
	int32_t height;
	float	intrinsic[3*3];	// row major as in OpenCV
	float	distortion[12];
};

struct Extrinsic
{
	float	world2cam[4*4];	// row major as in OpenCV
};

struct Mesh
{
	size_t cntV;	// vertices count
	size_t cntF;	// indices count
	size_t szV;		// vertex buffer size
	size_t szF;		// index buffer size
	float *bufV;	// vertex buffer
	int *bufF;		// index buffer
};

#endif /* COMMON_TYPE_H */
