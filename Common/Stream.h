#ifndef STREAM_H
#define STREAM_H

class Stream
{
public:
	virtual ~Stream() {};

	virtual bool CanWrite() const = 0;
	virtual int Write(const void *buf, const size_t sz) = 0;
	virtual void OnCompleted() = 0;
};

#endif /* STREAM_H */
