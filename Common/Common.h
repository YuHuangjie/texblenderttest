#ifndef COMMON_COMMON_H
#define COMMON_COMMON_H

#include <stdexcept>
#include <sstream>

#define ASSERT(a) do { \
	if (!a) { \
		std::ostringstream oss;	\
		oss << __FILE__ << " : " << __LINE__ << " return false";	\
		throw std::runtime_error(oss.str());	\
	}\
} while (0)

#endif /* COMMON_COMMON_H */
