#include <fstream>
#include <iostream>

#include "Geometry.h"
#include "tinyply.h"

using namespace std;

Geometry Geometry::FromFile(const std::string &filename)
{
	std::string ext = filename.substr(filename.length() - 3, 3);

	if (ext == "ply" || ext == "PLY") {
		return Geometry::FromPly(filename);
	}
	else {
		cerr << "[ERROR] Geometry: Unknown format: " << filename << endl;
	}
	return Geometry();
}

Geometry Geometry::FromPly(const std::string &filename)
{
	ifstream ss(filename, ios::binary);
	Geometry geometry;
	tinyply::PlyFile file;

	if (ss.fail()) throw runtime_error("[ERROR] Geometry: failed to open " + filename);

	file.parse_header(ss);

	shared_ptr<tinyply::PlyData> vertices, colors, normals, faces, texcoords;

	try { vertices = file.request_properties_from_element("vertex", { "x", "y", "z" }); }
	catch (const std::exception & e) { cout << e.what() << endl; }

	try { normals = file.request_properties_from_element("vertex", { "nx", "ny", "nz" }); }
	catch (const std::exception & e) { std::cout << e.what() << endl; }

	try { colors = file.request_properties_from_element("vertex", { "red", "green", "blue" }); }
	catch (const std::exception & e) { std::cout << e.what() << endl; }

	try { faces = file.request_properties_from_element("face", { "vertex_indices" }, 3); }
	catch (const std::exception & e) { std::cout << e.what() << endl; }

	try { texcoords = file.request_properties_from_element("vertex", { "u", "v" }); }
	catch (const std::exception & e) { std::cout << e.what() << endl; }

	file.read(ss);

	if (vertices) {
		const size_t numVerticesBytes = vertices->buffer.size_bytes();
		geometry.vertices = vector < float >(vertices->count * 3);
		std::memcpy(geometry.vertices.data(), vertices->buffer.get(), numVerticesBytes);
	}

	if (normals) {
		const size_t numNormalsBytes = normals->buffer.size_bytes();
		geometry.normals = vector<float>(vertices->count * 3);
		std::memcpy(geometry.normals.data(), vertices->buffer.get(), numNormalsBytes);
	}

	if (texcoords) {
		const size_t numTexcoordsBytes = texcoords->buffer.size_bytes();
		geometry.texCoords = vector<float>(vertices->count * 2);
		std::memcpy(geometry.texCoords.data(), texcoords->buffer.get(), numTexcoordsBytes);
	}

	if (colors) {
		const size_t numColorsBytes = colors->buffer.size_bytes();
		geometry.colors = vector < uint8_t >(colors->count * 3);
		std::memcpy(geometry.colors.data(), colors->buffer.get(), numColorsBytes);
	}

	if (faces) {
		const size_t numFacesBytes = faces->buffer.size_bytes();
		geometry.indices = vector < int32_t >(faces->count * 3);
		std::memcpy(geometry.indices.data(), faces->buffer.get(), numFacesBytes);
	}

	geometry.drawOption = DrawOption::Element;

	return geometry;
}