#include <opencv2/opencv.hpp>

#include "../3rd-party/glfw/glfw3.h"
#include "../TextureBlender/TextureBlender.h"
#include "../Common/Common.h"
#include "../Common/Type.h"
#include "Geometry.h"

using namespace std;

bool ReadIntrinsic(const string &filename, Intrinsic &intrin)
{
	cv::FileStorage file(filename, cv::FileStorage::READ);
	cv::Mat M;

	if (!file.isOpened()) {
		return false;
	}

	file["M"] >> M;
	std::memcpy(intrin.intrinsic, M.ptr<float>(), 9 * sizeof(float));
	
	return true;
}

bool ReadExtrinsic(const string &filename, Extrinsic &extrin)
{
	cv::FileStorage file(filename, cv::FileStorage::READ);
	cv::Mat R, T;
	cv::Mat RT4(4, 4, CV_32FC1);

	if (!file.isOpened()) {
		return false;
	}

	file["R"] >> R;
	file["T"] >> T;

	R.convertTo(R, CV_32FC1);
	T.convertTo(T, CV_32FC1);
	R.copyTo(RT4(cv::Range(0, 3), cv::Range(0, 3)));
	T.copyTo(RT4(cv::Range(0, 3), cv::Range(3, 4)));
	RT4.at<float>(3, 0) = 0.f;
	RT4.at<float>(3, 1) = 0.f;
	RT4.at<float>(3, 2) = 0.f;
	RT4.at<float>(3, 3) = 1.f;

	std::memcpy(extrin.world2cam, RT4.ptr<float>(), 4*4*sizeof(float));
	
	return true;
}

bool ReadImage(const string &filename, cv::Mat &image)
{
	image = cv::imread(filename);
	
	return !image.empty();
}

bool ReadMesh(const string &filename, float **verts, size_t *szV)
{
	Geometry mesh = Geometry::FromPly(filename);

	if (!mesh.HasVertex()) {
		return false;
	}
	if (!verts || !szV) {
		return false;
	}

	*szV = mesh.vertices.size() * sizeof(float);
	*verts = reinterpret_cast<float*>(new char[*szV]);
	std::memcpy(*verts, mesh.vertices.data(), *szV);
	return true;
}

int main()
{
	const size_t N_CAMS = 6;
	const size_t WIDTH = 1280;
	const size_t HEIGHT = 960;
	const string DATA_FOLDER = "./TestData";

	TextureBlender blender(N_CAMS);

	try {
		/* Read intrinsics and world2cam */
		vector<Intrinsic> intrins(N_CAMS);
		vector<Extrinsic> wld2cams(N_CAMS);

		for (size_t i = 0; i < N_CAMS; ++i) {
			string fdr = DATA_FOLDER + "/parameters/" + std::to_string(i + 1);
			string intrinFile = fdr + "/color_intrinsic.xml";
			string extrinFile = fdr + "/world2color.xml";

			ASSERT(ReadExtrinsic(extrinFile, wld2cams[i]));
			ASSERT(ReadIntrinsic(intrinFile, intrins[i]));
		}

		/* Configure texture blender */
		for (size_t i = 0; i < N_CAMS; ++i) {
			ASSERT(blender.SetCamera(i, WIDTH, HEIGHT, intrins[i].intrinsic,
				wld2cams[i].world2cam));
		}

		ASSERT(blender.HaveSetCameras());
		ASSERT(blender.IsConfigured());

		/* Feed tex images */
		blender.WaitFree();

		for (size_t i = 0; i < N_CAMS; ++i) {
			string file = DATA_FOLDER + "/rgb116_" + to_string(i) + ".jpg";
			cv::Mat image;

			ASSERT(ReadImage(file, image));
			ASSERT(blender.UpdateImage(i, image.ptr<uint8_t>(), image.rows*image.cols * 3));
		}

		/* Feed mesh */
		float *verts = nullptr;
		size_t szV = 0;
		string meshFile = DATA_FOLDER + "/mesh116.ply";

		ASSERT(ReadMesh(meshFile, &verts, &szV));
		ASSERT(blender.UpdateMesh(verts, szV, false));

		/* Run */
		blender.Run();
	}
	catch (std::exception &e) {
		cerr << "Caught exception: " << e.what() << endl;
	}

	while (true) {
		glfwPollEvents();
	}
}